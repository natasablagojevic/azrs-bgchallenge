FROM ubuntu:latest 

COPY ./bgchallenge /bgchallenge 

WORKDIR /bgchallenge 

RUN apt-get update -y 

RUN apt-get install -y cmake make git g++ qt6-base-dev qt6-multimedia-dev libglx-dev libgl1-mesa-dev xauth xvfb libxcb-xinerama0 x11-apps

RUN mkdir /build 

RUN cd /build 

RUN cmake ..

RUN make 

CMD ["./bgchallenge"]
