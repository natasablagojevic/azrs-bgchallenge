# BgChallenge

## :memo: Opis igre
Dobrodošao u BgChallenge, aplikaciju koja je stvorena kako bi ti pružila nezaboravno iskustvo upoznavanja Beograda iz jedinstvene perspektive! Naš cilj je da ti omogućimo da istražiš ovaj grad na način koji će te oduševiti i ostaviti te bez daha. 

BgChallenge nije samo obična aplikacija – ona je tvoj vodič kroz lavirinte istorije, geografije i intrigantnih mozgalica, sve to uz nezaboravnu avanturu u srcu Beograda. Ukoliko si strastveni istraživač istorijskih činjenica ovog grada, zaljubljenik u tajne njegovih ulica i želiš da testiraš svoje znanje, ovo je pravo mesto za tebe! 

Svaki nivo aplikacije je osmišljen kako bi predstavio mnoštvo izazova, svaki sa svojim jedinstvenim šarmom i intrigantnim zadacima. U igri su četiri nivoa, a svaki nivo je prava mala riznica ne samo znanja o Beogradu, već i veština koje ćeš razvijati tokom igre. 

Cilj? Osvojiti svaki izazov u što kraćem vremenu! Samo najbrži i najpametniji će se uzdići do titule najboljeg poznavatelja Beograda i osvojiti priznanje kao pravi majstor grada. 

Ali nemoj brinuti ako zapneš! Naš tim je tu da ti pruži podršku u vidu korisnih hintova kako bi te gurnuo napred ka rešenju problema. Bez obzira na prepreku, neka ti ova aplikacija pruži uzbudljivo iskustvo upoznavanja Beograda i njegovih tajni na potpuno nov način. 

Uđi u svet BgChallenge-a i doživi Beograd kao nikada pre! Dozvoli da te ova aplikacija vodi kroz vekove, ulice i priče grada koji čeka da bude otkriven. Spakuj se za avanturu – očekuje te nezaboravan doživljaj!

## :movie_camera: Demo snimak projekta 
- link: [BgChallenge](https://drive.google.com/file/d/19HTiikgm0VNtJNx-u6hGvigMJCbiUX3h/view)  <br><br>

## Okruženje
- [![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-olivia)](https://www.qt.io/download) <br><br>


## Programski jezik
- [![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-red)](https://www.cplusplus.com/)  *C++20*  <br>
- [![qt5](https://img.shields.io/badge/Framework-Qt5-blue)](https://doc.qt.io/qt-6/) *ili >* <br><br>

## :hammer: Instalacija:
- Preuzeti i instalirati [*Qt* i *Qt Creator*](https://www.qt.io/download).
- Ako je neophodno, nadograditi verziju C++ na C++20<br><br>

## Članovi :woman: :man: :
 - <a href="https://gitlab.com/AnjaCvetkovic25">Anja Cvetković 127/2020</a>
 - <a href="https://gitlab.com/natasablagojevic">Nataša Blagojević 159/2020</a>
 - <a href="https://gitlab.com/skiiszn">Anđelija Vasiljević 222/2020</a>
 - <a href="https://gitlab.com/pepi151101">Petra Ignjatović 63/2020</a>
 - <a href="https://gitlab.com/laza_mi20062">Lazar Lazović 62/2020</a>
