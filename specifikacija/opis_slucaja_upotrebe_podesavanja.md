# Slučaj upotrebe: Podešavanje
### Kratak opis:
Igrac bira opciju Podešavanje iz glavnog menija aplikacije. Izborom te opcije
otvara se prozor u kome se mogu podesiti zvuk I ton klika.


### Akteri:
- Igrač
### Preduslovi:
- Aplikacija je pokrenuta I prikazan je glavni meni.

### Osnovni tok:
    1. Igrac bira dugme "Podešavanje" iz glavnog menija.
    2. Aplikacija prikazuje prozor sa opcijama koje se mogu izmeniti.
    3. Ako je igrač izabrao opciju podešavanje jačine zvuka
       3.1. Ako je izabrao odredjenu opciju
          3.1.1. Izabrana opcija se primenjuje do kraja igre
       3.2. Inače se primenjuju podrazumevana podešavanja za jačinu zvuka
    4. Ako je igrač izabrao opciju podešavanje tona klika
       4.1. Ako je izabrao odredjenu opciju
          4.1.1. Izabrana opcija se primenjuje do kraja igre
       4.2. Inače se primenjuju podrazumevana podesavanja za ton klika
    5. Igrac bira dugme "Nazad" za povratak u glavni meni

### Alternativni tokovi:
-A1: Neočekivani izlaz iz aplikacije. Ako prilikom podešavanja igre,
korisnik iskljuci aplikaciju, sve izabrane opcije podešavanja se brisu I
aplikacija završava rad. Slučaj upotrebe se zavrsava.

### Podtokovi: /
### Specijalni zahtevi: /
### Dodatne informacije:/
