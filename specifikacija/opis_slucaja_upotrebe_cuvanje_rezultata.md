# Slučaj upotrebe: Čuvanje rezultata
**Kratak opis**: Prilikom kompletiranja igrice ili uspešno savladanog _Challenge_-a, čuvaju se informacije o rezultatima igrača.

**Akteri**: /

**Preduslovi**: Igrač je uspešno savladao sve nivoe igrice, ili je uspešno završio jedan _Challenge_.

**Postuslovi**: Podaci o trenutnom stanju igre za datog igrača su trajno sačuvani.

**Osnovni tok**:
```
1. Aplikacija serijalizatoru podataka šalje informacije o igraču i trenutnom stanju igrice
2. Podaci se serijalizuju
3. Serijalizovani podaci se skladište u fajl sistemu
4. Nastavlja se tok Igranje igre
```
**Aletarnativni tokovi**:
```
A1: Neočekivani zlaz iz aplikacije. Ukoliko u bilo kojem koraku dođe do prekida rada aplikacije, zapamćeni rezultati neće biti sačuvani. Slučaj upotrebe se završava.
A2: Igrač je kompletirao igricu. Ako je igrač savladao sve nivoe, u koraku 4. se prikazuje Tabela sa rezultatima. Slučaj upotrbe se završava.
```
**Podtokovi**: /

**Specijalni zahtevi**: /

**Dodatne informacije**: Tokom igranja drugog nivoa, aplikacija pamti rezultat koji je igrač postigao.

