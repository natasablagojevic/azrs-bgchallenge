cmake_minimum_required(VERSION 3.5)

project(bgchallenge VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --coverage")

set(CMAKE_CXX_FLAGS_DEBUG_INIT "-g -std=c++20 -Wall -Wextra")
set(CMAKE_CXX_FLAGS_RELEASE_INIT "-O3")

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Widgets Test Multimedia)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Widgets Test Multimedia)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS UiTools)

set(PROJECT_SOURCES
    source/main.cpp
    source/mainwindow.cpp
    include/mainwindow.h
    forms/mainwindow.ui
)

if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(bgchallenge
        MANUAL_FINALIZATION
        ${PROJECT_SOURCES}
        source/igrac.cpp
        source/rezultat.cpp
        source/mapa.cpp
        source/Challenge_test.cpp

        include/igrac.h
        include/rezultat.h
        include/mapa.h
        include/Challenge_test.h

        include/mapaUI.h
        source/mapaUI.cpp
        forms/mapaUI.ui

        include/timer.h
        source/timer.cpp

        include/challengeMorzeovaAzbukaUI.h
        source/challengeMorzeovaAzbukaUI.cpp
        forms/morzeovaAzbuka.ui

        resources.qrc

        include/lostinmigration.h
        source/lostinmigration.cpp
        forms/lostinmig.ui
        forms/kalemegdanUI.ui
        include/challengeKalemegdan.h
        source/challengeKalemegdan.cpp

        include/zeleznickastanica.h
        source/zeleznickastanica.cpp
        forms/zeleznicka.ui

        include/memorija.h
        source/memorija.cpp
        forms/memorija.ui

        include/permutacije.h
        source/permutacije.cpp
        forms/permutacije.ui

        include/skriveni.h
        source/skriveni.cpp
        forms/skriveni2.ui

        include/sveci.h
        source/sveci.cpp
        forms/sveci.ui

        include/hanojskakula.h
        source//hanojskakula.cpp
        forms/hanojskakula.ui

	include/vesalice.h
	source/vesalice.cpp
	forms/vesalice.ui

	include/kljucevi.h
	source/kljucevi.cpp
	forms/kljucevi.ui

        include/kviz.h
        source/kviz.cpp
        forms/kvizUI.ui

	include/matf.h
	source/matf.cpp
        forms/matf.ui

        include/pronadjiuljeza.h
        source/pronadjiuljeza.cpp
        forms/pronadjiuljeza.ui


	include/puzla.h
        source/puzla.cpp
        forms/puzla.ui


        include/note.h
        source/note.cpp
        forms/note.ui

    )
# Define target properties for Android with Qt 6 as:
#    set_property(TARGET bgchallenge APPEND PROPERTY QT_ANDROID_PACKAGE_SOURCE_DIR
#                 ${CMAKE_CURRENT_SOURCE_DIR}/android)
# For more information, see https://doc.qt.io/qt-6/qt-add-executable.html#target-creation
else()
    if(ANDROID)
        add_library(bgchallenge SHARED
            ${PROJECT_SOURCES}
        )
# Define properties for Android with Qt 5 after find_package() calls as:
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
    else()
        add_executable(bgchallenge
            ${PROJECT_SOURCES}
        )
    endif()
endif()

target_link_libraries(bgchallenge PRIVATE Qt${QT_VERSION_MAJOR}::Widgets Qt${QT_VERSION_MAJOR}::UiTools Qt${QT_VERSION_MAJOR}::Multimedia Qt${QT_VERSION_MAJOR}::Test)

# Qt for iOS sets MACOSX_BUNDLE_GUI_IDENTIFIER automatically since Qt 6.1.
# If you are developing for iOS or macOS you should consider setting an
# explicit, fixed bundle identifier manually though.
if(${QT_VERSION} VERSION_LESS 6.1.0)
  set(BUNDLE_ID_OPTION MACOSX_BUNDLE_GUI_IDENTIFIER com.example.bgchallenge)
endif()
set_target_properties(bgchallenge PROPERTIES
    ${BUNDLE_ID_OPTION}
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

include(GNUInstallDirs)
install(TARGETS bgchallenge
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

if(QT_VERSION_MAJOR EQUAL 6)
    qt_finalize_executable(bgchallenge)
endif()
