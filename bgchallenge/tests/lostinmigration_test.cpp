#include "catch.hpp"
#include "../include/lostinmigration.h"
#include "../include/timer.h"
#include <QApplication>
#include <QPushButton>
#include <QSignalSpy>

TEST_CASE("LostInMigration class tests", "[class]") {
    int argc = 0;
    char* argv[] = { nullptr };
    QApplication app(argc, argv);
    //LostInMigration lostinmigration(&timer, 10, nullptr);

    SECTION("Constructor and Initialization") {
        Timer timer;
        timer.start();
        LostInMigration migration(&timer, 10, nullptr);

        REQUIRE(migration.windowTitle() == "BgChallenge2");
        REQUIRE(migration.minimumWidth() == 1280);
        REQUIRE(migration.minimumHeight() == 720);


        REQUIRE(!migration.getBtnLevo()->isHidden());
        REQUIRE(!migration.getBtnDesno()->isHidden());
        REQUIRE(!migration.getBtnGore()->isHidden());
        REQUIRE(!migration.getBtnDole()->isHidden());
    }

    SECTION("Button Click Handling") {
        Timer timer;
        timer.start();
        LostInMigration migration(&timer, 10, nullptr);
        migration.Tekstovi();

        QSignalSpy spyPomoc(&migration, SIGNAL(pomoc()));

        QPushButton* mockButtonPomoc = migration.getUi()->findChild<QPushButton*>("pbPomoc");

        emit mockButtonPomoc->clicked();

        REQUIRE(spyPomoc.count() >= 0);
    }

    SECTION("Testing startFadeDownAnimations() function") {
        Timer timer;
        LostInMigration lostInMigration(&timer, 10, nullptr);
        lostInMigration.startFadeDownAnimations();

        REQUIRE(lostInMigration.findChild<QLabel*>("lblDesno1")->isHidden() == false);
        REQUIRE(lostInMigration.findChild<QLabel*>("lblDesno2")->isHidden() == false);
        REQUIRE(lostInMigration.findChild<QLabel*>("lblDole1")->isHidden() == false);

        QCoreApplication::processEvents(QEventLoop::AllEvents, 1000);

        REQUIRE(lostInMigration.findChild<QLabel*>("lblDesno1")->isHidden() == true);
        REQUIRE(lostInMigration.findChild<QLabel*>("lblDesno2")->isHidden() == true);
        REQUIRE(lostInMigration.findChild<QLabel*>("lblDole1")->isHidden() == true);
    }

}
