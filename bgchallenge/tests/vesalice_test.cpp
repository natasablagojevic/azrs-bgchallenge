#include "catch.hpp"
#include "../include/vesalice.h"
#include "../include/timer.h"
#include "../forms/ui_vesalice.h"
#include <QApplication>
#include <QTextEdit>
#include <QDebug>
#include <QtMath>

TEST_CASE("vesalice", "[vesalice]") {

    int argc = 0;
    char* argv[] = { nullptr };
    QApplication app(argc, argv);


    Timer* testTimer = new Timer();
    vesalice* v = new vesalice(testTimer, 0, nullptr);


    SECTION("Provera konstruktora i inicijalizacije") {
        testTimer->start();
        REQUIRE(v->minimumWidth() == 1280);
        REQUIRE(v->minimumHeight() == 720);
    }

    SECTION("Slovo A se pravilno obrađuje kada je prisutno u reči") {
        v->setRec( "JUGOSLAVIJA");
        v->slovoA();

        REQUIRE(v->getBrojSlova() > 1);

        QTextEdit* prviTextEdit = qobject_cast<QTextEdit *>(v->ui->horizontalLayout->itemAt(0)->widget());
        REQUIRE(prviTextEdit->toPlainText() == "A");

        // Dodatne provere prema potrebi
    }

    SECTION("Slovo A se pravilno obrađuje kada nije prisutno u reči") {
        v->setRec ("JUGOSLAVIJA");
        v->slovoA();

        REQUIRE(v->getBrojGreske() > 1);

        // Dodatne provere prema potrebi
    }

    delete v;
    delete testTimer;
}
