#include "catch.hpp"
#include "../include/skriveni.h"
#include "../include/timer.h"
#include <QSignalSpy>
#include <QApplication>
#include <QTest>  // Include the header for QTest

TEST_CASE("Skriveni class tests", "[class]") {
    int argc = 0;
    char* argv[] = { nullptr };
    QApplication app(argc, argv);

    SECTION("Button Click Handling") {
        Timer timer;
        timer.start();
        Skriveni skriveni(&timer, 10, nullptr);
        skriveni.Tekstovi();

        QSignalSpy spyBtnVucicClicked(&skriveni, SIGNAL(btnVucic_clicked()));
        QSignalSpy spyBtnMetaClicked(&skriveni, SIGNAL(btnMeta_clicked()));
        QSignalSpy spyBtnMegafonClicked(&skriveni, SIGNAL(btnMegafon_clicked()));

        skriveni.btnVucic_clicked();
        skriveni.btnMeta_clicked();
        skriveni.btnMegafon_clicked();

        REQUIRE(spyBtnVucicClicked.count() >= 0);
        REQUIRE(spyBtnMetaClicked.count() >= 0);
        REQUIRE(spyBtnMegafonClicked.count() >= 0);

        QTest::qWait(1000);

    }

    SECTION("Edukativni Tekst Display") {
        Timer timer;
        timer.start();
        Skriveni skriveni(&timer, 10, nullptr);
        skriveni.Tekstovi();

        skriveni.show();

        QTextBrowser* tbOpis = skriveni.findChild<QTextBrowser*>("tbOpis");
        REQUIRE(tbOpis != nullptr);
        QTest::qWait(1000);

    }

}
