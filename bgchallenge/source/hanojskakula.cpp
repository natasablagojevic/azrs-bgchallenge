#include "../include/hanojskakula.h"
#include <QDebug>
#include <QGraphicsProxyWidget>
#include <QGraphicsSceneDragDropEvent>
#include <QGridLayout>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>

Hanojskakula::Hanojskakula(Timer* vreme, int pocetnoVreme, QWidget* parent)
    : ChallengeTest(1, 1, vreme, pocetnoVreme, parent)
    , m_ui(new Ui::Hanojskakula)
{
    m_ui->setupUi(this);

    m_ui->stub1->setParent(nullptr);
    m_ui->stub2->setParent(nullptr);
    m_ui->stub3->setParent(nullptr);

    setImagePath(":/new/pozadine/resources/geneksBg.jpg");

    // inicijalizacija QGraphicsView i QGraphicsScene
    m_viewHanojskeKule = m_ui->viewHanojskaKula;
    m_sceneHanojskeKule = new QGraphicsScene(this);
    m_viewHanojskeKule->setScene(m_sceneHanojskeKule);
    m_sceneHanojskeKule->setSceneRect(0, 0, 400, 227);

    tekstovi();
    connect(m_vreme, &Timer::azurirajVreme, this, [=](int vreme) {
        m_pocetnoVreme++;
        QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
        m_ui->lcdNumber->display(time.toString("h:mm:ss"));
        update();
    });

    QPixmap stuboviSlika(":/resources/hanojska_kula/stub.png");
    m_ui->stub1->setPixmap(stuboviSlika);
    m_ui->stub2->setPixmap(stuboviSlika);
    m_ui->stub3->setPixmap(stuboviSlika);

    // dodavanje stubica
    m_proxyStub1 = new QGraphicsProxyWidget();
    m_proxyStub2 = new QGraphicsProxyWidget();
    m_proxyStub3 = new QGraphicsProxyWidget();
    m_proxyStub1->setWidget(m_ui->stub1);
    m_proxyStub2->setWidget(m_ui->stub2);
    m_proxyStub3->setWidget(m_ui->stub3);
    m_proxyStub1->setAcceptDrops(true);
    m_proxyStub2->setAcceptDrops(true);
    m_proxyStub3->setAcceptDrops(true);
    m_sceneHanojskeKule->addItem(m_proxyStub1);
    m_sceneHanojskeKule->addItem(m_proxyStub2);
    m_sceneHanojskeKule->addItem(m_proxyStub3);
    // postavi stubice na scenu
    m_proxyStub1->setPos(-100, 0);
    m_proxyStub2->setPos(100, 0);
    m_proxyStub3->setPos(300, 0);

    // ucitavanje diskova
    QPixmap diskoviSlika(":/resources/hanojska_kula/diskovi.png");
    if (diskoviSlika.isNull()) {
        qDebug() << "Slika nije uspešno učitana!";
        return;
    }

    // izdvajanje delova slike za svaki disk
    QPixmap disk1Slika = diskoviSlika.copy(77, 0, 155, 51);
    QPixmap disk2Slika = diskoviSlika.copy(35, 52, 235, 51);
    QPixmap disk3Slika = diskoviSlika.copy(0, 52 + 51, 320, 51);
    m_disk1Item = new QGraphicsPixmapItem(disk1Slika);
    m_disk2Item = new QGraphicsPixmapItem(disk2Slika);
    m_disk3Item = new QGraphicsPixmapItem(disk3Slika);

    postaviPocetnoStanje();

    m_disk1Item->setScale(0.4);
    m_disk2Item->setScale(0.4);
    m_disk3Item->setScale(0.4);
    m_sceneHanojskeKule->addItem(m_disk1Item);
    m_sceneHanojskeKule->addItem(m_disk2Item);
    m_sceneHanojskeKule->addItem(m_disk3Item);

    m_viewHanojskeKule->setDragMode(QGraphicsView::ScrollHandDrag);
    m_sceneHanojskeKule->installEventFilter(this);
    m_viewHanojskeKule->show();

    connect(m_ui->retry, &QPushButton::clicked, this, &Hanojskakula::onRetryClicked);
    connect(m_ui->pbNazad, &QPushButton::clicked, this, &Hanojskakula::zatvori);
    connect(m_ui->pbPomoc, &QPushButton::clicked, this, &Hanojskakula::pomoc);
}

void Hanojskakula::pomoc()
{
    QDialog dijalog;
    dijalog.setFixedHeight(300);
    dijalog.setFixedWidth(400);
    dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
                          "rgb(200,200,200); margin: 10px;}");

    QLabel* text = new QLabel("Kao što je Geneks kula simbol socijalističkog Beograda, "
                              "tako su hanojske "
                              "kule jedan simboličan programerski zadatak. Da li mozete "
                              "da prebacite sve "
                              "diskove na treću kulu, od najvećeg do najmanjeg? Srećno!",
        &dijalog);
    text->setAlignment(Qt::AlignCenter);
    text->setWordWrap(true);
    text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
    QPushButton* ok = new QPushButton("OK", &dijalog);
    ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
                      "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
                      "padding: 10px; border-radius: 21px; font: bold 14px;}"
                      "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
                      "#bcbcbc; }"
                      "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
                      "#a0a0a0; }");
    QVBoxLayout* layout = new QVBoxLayout(&dijalog);

    layout->addWidget(text);

    layout->addWidget(ok);
    connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
    dijalog.exec();
    m_pomocExecuted = true;
}

bool Hanojskakula::eventFilter(QObject* watched, QEvent* event)
{
    if (watched == m_sceneHanojskeKule) {
        if (event->type() == QEvent::GraphicsSceneMousePress) {
            QGraphicsSceneMouseEvent* mouseEvent = static_cast<QGraphicsSceneMouseEvent*>(event);
            QGraphicsItem* item = m_sceneHanojskeKule->itemAt(mouseEvent->scenePos(), m_viewHanojskeKule->transform());
            // Ako je kliknuto na disk
            if (item && item->type() == QGraphicsPixmapItem::Type && (item->flags() & QGraphicsItem::ItemIsMovable)) {
                m_izabranDisk = dynamic_cast<QGraphicsPixmapItem*>(item);
                m_inicijalnaPozicijaDiska = m_izabranDisk->pos();
                m_sceneHanojskeKule->setFocusItem(m_izabranDisk);

                QList<QGraphicsItem*> itemNaPoz = m_sceneHanojskeKule->items(item->pos());
                // pronadji prvi QGraphicsProxyWidget u listi
                for (QGraphicsItem* item : itemNaPoz) {
                    if (item->type() == QGraphicsProxyWidget::Type) {
                        m_proxyInicijalniStub = dynamic_cast<QGraphicsProxyWidget*>(item);
                        break;
                    }
                }
                // qDebug() << "Proxy stub s" << proxyInicijalniStub;
                m_sceneHanojskeKule->setFocusItem(m_izabranDisk);

                QStack<unsigned char>& stek = vratiOdgovarajuciStek(m_proxyInicijalniStub);
                smanjiStek(stek);
            }
        } else if (event->type() == QEvent::GraphicsSceneMouseRelease && m_izabranDisk) {
            QGraphicsSceneMouseEvent* mouseEvent = static_cast<QGraphicsSceneMouseEvent*>(event);
            QGraphicsItem* dropItem = m_sceneHanojskeKule->itemAt(mouseEvent->scenePos(), m_viewHanojskeKule->transform());

            QList<QGraphicsItem*> itemNaPoz = m_sceneHanojskeKule->items(dropItem->pos());
            QGraphicsProxyWidget* proxyCiljniStub = nullptr;
            // pronadji prvi QGraphicsProxyWidget u listi
            for (QGraphicsItem* item : itemNaPoz) {
                if (item->type() == QGraphicsProxyWidget::Type) {
                    proxyCiljniStub = dynamic_cast<QGraphicsProxyWidget*>(item);
                    break;
                }
            }
            m_sceneHanojskeKule->setFocusItem(m_izabranDisk);
            if (proxyCiljniStub && m_proxyInicijalniStub) {
                obradiDropDiska(*m_proxyInicijalniStub, *proxyCiljniStub, m_inicijalnaPozicijaDiska);
            }
            // resetujemo povuceni disk i inicijalni stub
            m_izabranDisk = nullptr;
            m_proxyInicijalniStub = nullptr;
            m_sceneHanojskeKule->setFocusItem(nullptr);
        }
    }
    return QWidget::eventFilter(watched, event);
}

void Hanojskakula::obradiDropDiska(QGraphicsProxyWidget& proxyIzvorniStub, QGraphicsProxyWidget& proxyCiljniStub, QPointF inicijalnaPozicijaDiska)
{

    QLabel* ciljniStub = dynamic_cast<QLabel*>(proxyCiljniStub.widget());
    QStack<unsigned char>& ciljniStek = vratiOdgovarajuciStek(&proxyCiljniStub);
    QStack<unsigned char>& izvorniStek = vratiOdgovarajuciStek(&proxyIzvorniStub);

    // ako je ciljni stub prazan
    if (ciljniStek.empty()) {
        postaviDiskNaStub(m_izabranDisk, ciljniStek, ciljniStub, 0);
    } else if (dozvoljenPotez(m_izabranDisk, ciljniStek)) {
        if (ciljniStek.size() == 1 || ciljniStek.size() == 2) {
            promeniDozvolu(ciljniStek, false);
            int y = (ciljniStek.size() == 1) ? -20 : -40;
            postaviDiskNaStub(m_izabranDisk, ciljniStek, ciljniStub, y);

            if (m_stub3.size() == 3) {
                prikaziPorukuPobede();
            }
        }
    } else {
        promeniDozvolu(izvorniStek, false);
        if (m_izabranDisk == m_disk2Item) { // zeleni
            izvorniStek.push(2);
        } else { // crveni;
            izvorniStek.push(3);
        }
        m_izabranDisk->setPos(inicijalnaPozicijaDiska);
    }
}

void Hanojskakula::postaviDiskNaStub(QGraphicsPixmapItem* disk, QStack<unsigned char>& stek, QLabel* ciljniStub, int yOffset)
{
    int x = 0;
    int y = 0;

    // postavljanje odgovarajucih vrednosti za x i y u zavisnosti od diska
    if (disk == m_disk1Item) { // zuti
        x = 70;
        y = 178 + yOffset;
        stek.push(1);
    } else if (disk == m_disk2Item) { // zeleni
        x = 54;
        y = 179 + yOffset;
        stek.push(2);
    } else { // crveni
        x = 40;
        y = 179 + yOffset;
        stek.push(3);
    }

    qreal diskX = ciljniStub->pos().x() + x;
    qreal diskY = ciljniStub->pos().y() + y;
    disk->setPos(diskX, diskY);
}

void Hanojskakula::postaviPocetnoStanje()
{
    if (m_disk1Item && m_disk2Item && m_disk3Item) {
        m_disk3Item->setPos(-100 + 40, 179); // crveni
        m_disk2Item->setPos(-100 + 54, 179 - 20); // zeleni
        m_disk1Item->setPos(-100 + 70, 179 - 40); // zuti
        m_disk1Item->setFlag(QGraphicsItem::ItemIsMovable);
        m_disk2Item->setFlag(QGraphicsItem::ItemIsMovable, false);
        m_disk3Item->setFlag(QGraphicsItem::ItemIsMovable, false);
        m_stub1.clear();
        m_stub2.clear();
        m_stub3.clear();
        m_stub1.push(3);
        m_stub1.push(2);
        m_stub1.push(1);
    }
}

QStack<unsigned char>& Hanojskakula::vratiOdgovarajuciStek(QGraphicsProxyWidget* proxyCiljniStub)
{

    if (proxyCiljniStub == m_proxyStub1) {
        return m_stub1;
    } else if (proxyCiljniStub == m_proxyStub2) {
        return m_stub2;
    }
    return m_stub3;
}
bool Hanojskakula::dozvoljenPotez(QGraphicsPixmapItem* izabranDisk, QStack<unsigned char> stub)
{
    unsigned char velicinaDraggedDiska = 0;
    if (izabranDisk == m_disk1Item) { // zuti
        velicinaDraggedDiska = 1;
    } else if (izabranDisk == m_disk2Item) { // zeleni
        velicinaDraggedDiska = 2;
    } else { // crveni
        velicinaDraggedDiska = 3;
    }

    // da bi potez bio dozvoljen mora da disk na vrhu bude veci od velicine diska
    // koji se spusta
    return stub.top() > velicinaDraggedDiska ? true : false;
}

void Hanojskakula::smanjiStek(QStack<unsigned char>& stub)
{
    if (!stub.isEmpty()) {
        stub.pop();
    }
    promeniDozvolu(stub, true);
}

void Hanojskakula::promeniDozvolu(QStack<unsigned char> stub, bool dozvola)
{
    if (!stub.isEmpty()) {
        if (stub.top() == 2) {
            m_disk2Item->setFlag(QGraphicsItem::ItemIsMovable, dozvola);
        } else {
            m_disk3Item->setFlag(QGraphicsItem::ItemIsMovable, dozvola);
        }
    }
}

void Hanojskakula::prikaziPorukuPobede()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle(" ");
    msgBox.setText("Bravo!");

    QAbstractButton* igrajOpetButton = msgBox.addButton("Igraj opet", QMessageBox::ActionRole);
    QAbstractButton* daljeButton = msgBox.addButton("Dalje", QMessageBox::ActionRole);

    msgBox.setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);
    QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
                               "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
                               "padding: 10px; border-radius: 21px; font: bold 14px;}"
                               "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
                               "#bcbcbc; }"
                               "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
                               "#a0a0a0; }";

    QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";
    msgBox.setStyleSheet(msgBoxStylesheet);
    igrajOpetButton->setStyleSheet(buttonStylesheet);
    daljeButton->setStyleSheet(buttonStylesheet);
    msgBox.exec();

    if (msgBox.clickedButton() == igrajOpetButton) {
        postaviPocetnoStanje();
    } else if (msgBox.clickedButton() == daljeButton) {

        if (this->layout() != nullptr) {
            QLayoutItem* item;
            while ((item = this->layout()->takeAt(0)) != nullptr) {
                qDebug() << "brisem" << item->widget()->objectName() << item->widget()->parent()->objectName();

                delete item->widget();
                delete item;
            }
            delete this->layout();
        }
        QObjectList children = this->children();
        for (QObject* child : children) {
            if (QWidget* widget = qobject_cast<QWidget*>(child)) {
                widget->setVisible(false);
            }
        }
        QLayout* postojeciLayout = this->layout();
        if (postojeciLayout) {
            delete postojeciLayout;
        }
        this->prikaziEdukativniTekst();
    }
}

void Hanojskakula::tekstovi()
{

    m_edukativniTekst = R"(
Beograd ima dve urbanističke kapije. Jedna je neuspela. To je Istočna – zvana Rudo. Čine je tri stepenasta solitera, međusobno okrenuta leđima, kao da ne govore.
Nasuprot Istočnoj, Zapadna kapija je do te mere uspelo delo da ju je njujorški muzej MoMa uvrstio u izložbu jugoslovenske arhitekture!

U narodu je Zapadna kapija poznatija pod imenom Geneksove kule. U kuli bližoj auto-putu najveća spoljnotrgovinska firma Jugoslacije Generaleksport, skraćeno Geneks, našla je sebi odgovarajuće sedište. Poslovanje po rubnim kontinentima i milijarde dolara prometa tražili su upravo takvu poslovnu zgradu: moderno monumentalnu. Prvi đakuzi koji je uvezen u Jugoslaviju ugrađen je u kabinu generalnog direktora.

Geneks je izrastao u državu u državi. Osnovali su ga 1952. najsposobniji islednici Uprave državne bezbednosti sa zadatkom da se razbije trgovinska blokada Sovjetskog saveza i razviju ekonomske veze sa kapitalističkim svetom. Šezdest predstavništava u svetu trgovalo je sa obe strane gvozdene zavese. Prihvatali su se I poslovi u kriznim područjima. U Africi i na Bliskom istoku je važilo pravilo: Ako si rešio da počneš rat, prvo pozovi Geneks!

Velika firma je bučno propala u opštem raspadu Jugoslavije. U njenim prostorijama sada rade neke daleko manje kompanije koje nikada neće obnoviti sjaj Titove doktrine trećeg puta u međunarodnoj trgovini.
Za stanare Geneksa, veliki trag u srcu ostavljaju polukružne narandžaste stepenice, gde se klinci igraju žmurke I fudbala, a tinejdžeri piju pivo. Tu se stvaraju prve uspomene, prve ljubavi i ostale čari detinjstva.)";

    m_asocijacija = "Prva stanica tramvaja 7 nakon nakon Ekonomskog fakulteta";

    m_hint = "Iskoristi Moovit, slobodno! Obrati pažnju na smer!";

    m_resenjeAsocijacije.append("staro sajmiste");
    m_resenjeAsocijacije.append("staro sajmište");

    m_naslovEdukativnogTeksta = "O Geneks kuli";
}

Hanojskakula::~Hanojskakula() { delete m_ui; }

void Hanojskakula::zapocniChallenge() { }
void Hanojskakula::zavrsiChallenge() { }

void Hanojskakula::onRetryClicked() { postaviPocetnoStanje(); }
