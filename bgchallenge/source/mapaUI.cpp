#include "../include/mapaUI.h"
#include "../include/igrac.h"
#include "../include/mapa.h"
#include <QFile>
#include <QIcon>
#include <QPainter>
#include <QPixmap>
#include <QPushButton>
#include <QUiLoader>

#include "../include/challengeKalemegdan.h"
#include "../include/challengeMorzeovaAzbukaUI.h"
#include "../include/hanojskakula.h"
#include "../include/kljucevi.h"
#include "../include/kviz.h"
#include "../include/lostinmigration.h"
#include "../include/matf.h"
#include "../include/memorija.h"
#include "../include/note.h"
#include "../include/permutacije.h"
#include "../include/pronadjiuljeza.h"
#include "../include/puzla.h"
#include "../include/skriveni.h"
#include "../include/sveci.h"
#include "../include/vesalice.h"
#include "../include/zeleznickastanica.h"

MapaUI::MapaUI(Igrac igrac, Rezultat* rez, QWidget* parent)
    : QWidget(parent)
{
    // UI setup

    qDebug() << igrac.getIme() << "iz mape ui";

    m_mapa = new Mapa(igrac, rez, this);

    this->setWindowTitle("BgChallenge");
    this->setMinimumWidth(1280);
    this->setMinimumHeight(720);
    this->setFixedSize(this->width(), this->height());
    updateBackground(":/new/resources/resources/prvi.png");
    this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

    // ucitavanje ui
    QFile file(":/new/resources/forms/mapaUI.ui");

    if (!file.open(QFile::ReadOnly)) {
        qDebug() << "Error opening UI file iz mapeui:" << file.errorString();
        return;
    }

    // Kreirajte QUiLoader i učitajte sučelje iz .ui datoteke
    QUiLoader loader;
    QWidget* ui = loader.load(&file, this);
    ui->setFixedSize(this->width(), this->height());
    // Zatvorite datoteku
    file.close();

    QPushButton* pbNazad = ui->findChild<QPushButton*>("pbNazadNaGlavniMeni");

    m_izazovi.append(ui->findChild<QPushButton*>("pbPrviChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbDrugiChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbTreciChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbCetvrtiChallenge"));

    m_izazovi.append(ui->findChild<QPushButton*>("pbPetiChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbSestiChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbSedmiChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbOsmiChallenge"));

    m_izazovi.append(ui->findChild<QPushButton*>("pbDevetiChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbDesetiChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbJedanaestiChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbDvanaestiChallenge"));

    m_izazovi.append(ui->findChild<QPushButton*>("pbTrinaestiChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbCetrnaestiChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbPetnaestiChallenge"));
    m_izazovi.append(ui->findChild<QPushButton*>("pbSesnaestiChallenge"));

    for (int i = 1; i < m_izazovi.length(); i++)
        m_izazovi[i]->setVisible(false);

    if (igrac.getPoslednjiOtkljucaniChallenge() > 0) {
        ucitajMapu(igrac.getPoslednjiOtkljucaniChallenge(), igrac.getNivo());
    }

    m_vreme = ui->findChild<QLCDNumber*>("lcdVreme");
    m_vreme->setStyleSheet("QLCDNumber { background-color: #2c3e50; color: "
                           "#ecf0f1; border: 2px solid "
                           "#3498db; border-radius: 5px; }"
                           "QLCDNumber::segment { background-color: #e74c3c; }"
                           "QLCDNumber::decimals { color: #e74c3c; }");

    azurirajVreme(igrac.getVreme());

    connect(pbNazad, &QPushButton::clicked, this, &MapaUI::nazadNaGlavniMeni);
    connect(m_mapa, &Mapa::azurirajLCDNumber, this, &MapaUI::azurirajVreme);
    connect(m_mapa, &Mapa::otkljucajNaredniIzazov, this, &MapaUI::otkljucajNaredniIzazov);
    connect(m_mapa, &Mapa::cestitajIgracu, this, &MapaUI::zavrsenaIgrica);

    connect(m_izazovi[0], &QPushButton::clicked, this,
        &MapaUI::prikaziPrviChallenge); // genex - hanojske kule
    connect(m_izazovi[1], &QPushButton::clicked, this,
        &MapaUI::prikaziDrugiChallenge); // staro sajmiste - kviz
    connect(m_izazovi[2], &QPushButton::clicked, this,
        &MapaUI::prikaziTreciChallenge); // hotel jugoslavija - morzeova azbuka
    connect(m_izazovi[3], &QPushButton::clicked, this,
        &MapaUI::prikaziCetvrtiChallenge); // gardos kula - puzle

    connect(m_izazovi[4], &QPushButton::clicked, this,
        &MapaUI::prikaziPetiChallenge); // zeleznicka - povezivanje
    connect(m_izazovi[5], &QPushButton::clicked, this,
        &MapaUI::prikaziSestiChallenge); // teslin muzej - lost in migration
    connect(m_izazovi[6], &QPushButton::clicked, this,
        &MapaUI::prikaziSedmiChallenge); // hram - memorija
    connect(m_izazovi[7], &QPushButton::clicked, this,
        &MapaUI::prikaziOsmiChallenge); // muzej jugoslavija - vesalice

    connect(m_izazovi[8], &QPushButton::clicked, this,
        &MapaUI::prikaziDevetiChallenge); // hotel moskva - kljucevi
    connect(m_izazovi[9], &QPushButton::clicked, this,
        &MapaUI::prikaziDesetiChallenge); // skupstina - skriveni objekti
    connect(m_izazovi[10], &QPushButton::clicked, this,
        &MapaUI::prikaziJedanaestiChallenge); // tasmajdan - muzicke note
    connect(m_izazovi[11], &QPushButton::clicked, this,
        &MapaUI::prikaziDvanaestiChallenge); // crkva sv marka - sveci

    connect(m_izazovi[12], &QPushButton::clicked, this,
        &MapaUI::prikaziTrinaestiChallenge); // skadarlija - uljezi
    connect(m_izazovi[13], &QPushButton::clicked, this,
        &MapaUI::prikaziCetrnaestiChallenge); // trg republike - permutacije
    connect(m_izazovi[14], &QPushButton::clicked, this,
        &MapaUI::prikaziPetnaestiChallenge); // kalemegdan ?
    connect(m_izazovi[15], &QPushButton::clicked, this,
        &MapaUI::prikaziSesnaestiChallenge); // MATF ?
}

MapaUI::~MapaUI()
{
    delete m_mapa;
    m_mapa = nullptr;
    delete m_vreme;
    m_vreme = nullptr;
}

void MapaUI::prikaziPrviChallenge()
{
    Hanojskakula* challenge = new Hanojskakula(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(challenge, &Hanojskakula::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(challenge, &Hanojskakula::closed, this, [=]() {
        if (challenge != nullptr) {
            delete challenge;
            qDebug() << "izbrisana genex kula";
        }
    });
    challenge->show();
}

void MapaUI::prikaziDrugiChallenge()
{
    Kviz* challenge = new Kviz(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(challenge, &Kviz::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(challenge, &Kviz::closed, this, [=]() {
        if (challenge != nullptr) {
            delete challenge;
            qDebug() << "izbrisano staro sajmiste";
        }
    });
    challenge->show();
}

void MapaUI::prikaziTreciChallenge()
{
    ChallengeMorzeovaAzbukaUI* challenge = new ChallengeMorzeovaAzbukaUI(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(challenge, &ChallengeMorzeovaAzbukaUI::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(challenge, &ChallengeMorzeovaAzbukaUI::closed, this, [=]() {
        if (challenge != nullptr) {
            delete challenge;
            qDebug() << "izbrisan hotel jugoslavija";
        }
    });
    challenge->show();
}

void MapaUI::prikaziCetvrtiChallenge()
{
    Puzla* challenge = new Puzla(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(challenge, &Puzla::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(challenge, &Puzla::closed, this, [=]() {
        if (challenge != nullptr) {
            delete challenge;
            qDebug() << "izbrisan gardos";
        }
    });
    challenge->show();
}

void MapaUI::prikaziPetiChallenge()
{
    ZeleznickaStanica* vozovi = new ZeleznickaStanica(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(vozovi, &ZeleznickaStanica::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(vozovi, &ZeleznickaStanica::closed, this, [=]() {
        if (vozovi != nullptr) {
            delete vozovi;
            qDebug() << "izbrisana zeleznicka";
        }
    });
    vozovi->show();
}

void MapaUI::prikaziSestiChallenge()
{
    LostInMigration* lostinmig = new LostInMigration(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(lostinmig, &LostInMigration::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(lostinmig, &LostInMigration::closed, this, [=]() {
        if (lostinmig != nullptr) {
            delete lostinmig;
            qDebug() << "izbrisan muzej nikole tesle";
        }
    });
    lostinmig->show();
}

void MapaUI::prikaziSedmiChallenge()
{
    Memorija* memorija = new Memorija(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(memorija, &Memorija::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(memorija, &Memorija::closed, this, [=]() {
        if (memorija != nullptr) {
            delete memorija;
            qDebug() << "izbrisan hram sv save";
        }
    });
    memorija->show();
}

void MapaUI::prikaziOsmiChallenge()
{
    Vesalice* challenge = new Vesalice(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(challenge, &Vesalice::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(challenge, &Vesalice::closed, this, [=]() {
        if (challenge != nullptr) {
            delete challenge;
            qDebug() << "izbrisan muzej jugoslavije";
        }
    });
    challenge->show();
}

void MapaUI::prikaziDevetiChallenge()
{
    Kljucevi* challenge = new Kljucevi(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(challenge, &Kljucevi::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(challenge, &Kljucevi::closed, this, [=]() {
        if (challenge != nullptr) {
            delete challenge;
            qDebug() << "izbrisan hotel moskva";
        }
    });
    challenge->show();
}

void MapaUI::prikaziDesetiChallenge()
{
    Skriveni* skriveni = new Skriveni(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(skriveni, &Skriveni::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(skriveni, &Skriveni::closed, this, [=]() {
        if (skriveni != nullptr) {
            delete skriveni;
            qDebug() << "izbrisana skupstina";
        }
    });
    skriveni->show();
}

void MapaUI::prikaziJedanaestiChallenge()
{
    Sveci* challenge = new Sveci(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(challenge, &Sveci::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(challenge, &Sveci::closed, this, [=]() {
        if (challenge != nullptr) {
            delete challenge;
            qDebug() << "izbrisana crkva sv marka";
        }
    });
    challenge->show();
}
void MapaUI::prikaziDvanaestiChallenge()
{
    Note* note = new Note(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(note, &Note::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(note, &Note::closed, this, [=]() {
        if (note != nullptr) {
            delete note;
            qDebug() << "izbrisane note";
        }
    });
    note->show();
}

void MapaUI::prikaziTrinaestiChallenge()
{
    PronadjiUljeza* challenge = new PronadjiUljeza(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(challenge, &PronadjiUljeza::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(challenge, &PronadjiUljeza::closed, this, [=]() {
        if (challenge != nullptr) {
            delete challenge;
            qDebug() << "izbrisana skadarlija";
        }
    });
    challenge->show();
}

void MapaUI::prikaziCetrnaestiChallenge()
{
    Permutacije* permutacije = new Permutacije(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(permutacije, &Permutacije::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(permutacije, &Permutacije::closed, this, [=]() {
        if (permutacije != nullptr) {
            delete permutacije;
            qDebug() << "izbrisan trg";
        }
    });
    permutacije->show();
}

void MapaUI::prikaziPetnaestiChallenge()
{
    ChallengeKalemegdan* challenge = new ChallengeKalemegdan(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(challenge, &ChallengeKalemegdan::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(challenge, &ChallengeKalemegdan::closed, this, [=]() {
        if (challenge != nullptr) {
            delete challenge;
            qDebug() << "izbrisan kalemegdan";
        }
    });
    challenge->show();
}

void MapaUI::prikaziSesnaestiChallenge()
{
    MATF* challenge = new MATF(m_mapa->m_timer, m_mapa->getGameTime(), this);
    connect(challenge, &MATF::resenIzazov, m_mapa, &Mapa::resenIzazov);
    connect(challenge, &MATF::closed, this, [=]() {
        if (challenge != nullptr) {
            delete challenge;
            qDebug() << "izbrisan matf";
        }
    });
    challenge->show();
}

void MapaUI::ucitajMapu(int poslednjiIzazov, int nivo)
{
    if (nivo == 2) {
        updateBackground(":/new/resources/resources/drugi.png");
    } else if (nivo == 3) {
        updateBackground(":/new/resources/resources/treci.png");
    } else if (nivo == 4) {
        updateBackground(":/new/resources/resources/cetvrti.png");
    }
    // int id = poslednjiIzazov;
    // if(id>4 && id<=8)
    // {
    //     updateBackground(":/new/resources/resources/drugi.png");
    // }
    // else if(id>8 && id<=12)
    // {
    //     updateBackground(":/new/resources/resources/treci.png");
    // }
    // else if(id>12)
    // {
    //     updateBackground(":/new/resources/resources/cetvrti.png");
    // }

    if (poslednjiIzazov < 16) {
        for (int i = 0; i <= poslednjiIzazov; i++) {
            m_izazovi[i]->setVisible(true);
        }
    } else {
        // ucitani igrac je presao sve nivoe
        for (int i = 0; i < 16; i++) {
            m_izazovi[i]->setVisible(true);
        }
    }
}

void MapaUI::azurirajVreme(qint64 vreme)
{
    // qDebug()<< vreme << "iz mape ui";
    QTime time = QTime(0, 0).addSecs(vreme);
    m_vreme->display(time.toString("h:mm:ss"));
    update();
}

void MapaUI::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event);

    QPainter painter(this);

    QPixmap backgroundImage(m_bgImagePath);

    painter.drawPixmap(rect(), backgroundImage, backgroundImage.rect());
}

void MapaUI::updateBackground(const QString& newPath)
{
    m_bgImagePath = newPath;
    update();
}

void MapaUI::nazadNaGlavniMeni()
{
    m_mapa->zavrsiIgru();
    this->close();
}

void MapaUI::otkljucajNaredniIzazov(qint64 id)
{
    m_izazovi[id]->setVisible(true);
    if (id >= 4 && id < 8) {
        updateBackground(":/new/resources/resources/drugi.png");
    } else if (id >= 8 && id < 12) {
        updateBackground(":/new/resources/resources/treci.png");
    } else if (id >= 12) {
        updateBackground(":/new/resources/resources/cetvrti.png");
    }
}

void MapaUI::zavrsenaIgrica()
{
    emit cestitajIgracu();
    this->close();
}
