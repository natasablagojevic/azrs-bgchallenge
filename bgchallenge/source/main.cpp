#include "../include/mainwindow.h"
#include <QApplication>

int main(int argc, char* argv[])
{
    // Postavite Qt::AA_ShareOpenGLContexts pre nego što se stvori
    // QCoreApplication ili QApplication
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QApplication a(argc, argv);
    mainwindow w; // Pretpostavljajući da je vaš prozor nazvan MainWindow,
                  // prilagodite prema stvarnom imenu
    w.show();
    return a.exec();
}
