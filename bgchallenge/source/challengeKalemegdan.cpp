#include "../include/challengeKalemegdan.h"
#include <QDebug>
#include <QFile>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPixmap>
#include <QRandomGenerator>
#include <QSpacerItem>
#include <QUiLoader>

ChallengeKalemegdan::ChallengeKalemegdan(Timer* timer, int pocetnoVreme, QWidget* parent)
    : m_trenutnaPozicija(0)
    , m_pozicijaPocetnogSlova(0)
    , ChallengeTest(15, 4, timer, pocetnoVreme, parent)
{
    this->setWindowTitle("BgChallenge");
    this->setMinimumWidth(1280);
    this->setMinimumHeight(720);
    this->setFixedSize(this->width(), this->height());

    this->setImagePath(":/new/kalemegdan/resources/kalemegdanBG.jpg");
    this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

    QFile file(":/new/kalemegdan/forms/kalemegdanUI.ui");

    if (!file.open(QFile::ReadOnly)) {
        qDebug() << "Error opening UI file:" << file.errorString();
        return;
    }

    QUiLoader loader;
    m_ui = loader.load(&file, this);
    m_ui->setFixedSize(this->width(), this->height());

    file.close();

    QPushButton* nazadBtn = m_ui->findChild<QPushButton*>("pbNazad");
    connect(nazadBtn, &QPushButton::clicked, this, &ChallengeKalemegdan::zatvori);
    QLCDNumber* lcdNumber = m_ui->findChild<QLCDNumber*>("lcdNumber");

    connect(m_vreme, &Timer::azurirajVreme, this, [=](int vreme) {
        m_pocetnoVreme++;
        QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
        lcdNumber->display(time.toString("h:mm:ss"));
        update();
    });

    connect(this, &ChallengeKalemegdan::pogodjenaRec, this, &ChallengeKalemegdan::prikaziEdukativniTekst);

    tekstovi();
    m_rec = generisiRec();
    qDebug() << m_rec;

    // djedo ako si umro pomagaj
    QString id;
    for (int i = 0; i < 30; i++) {
        id = "label_" + QString::number(i + 1);
        m_polja.append(m_ui->findChild<QLabel*>(id));
        // qDebug() << id;
    }
    // koja je karta!!!!!

    QPushButton* pogodiBtn = m_ui->findChild<QPushButton*>("pbPogodi");
    connect(pogodiBtn, &QPushButton::clicked, this, &ChallengeKalemegdan::pogodi);

    QPushButton* izbrisiBtn = m_ui->findChild<QPushButton*>("pbIzbrisi");
    connect(izbrisiBtn, &QPushButton::clicked, this, &ChallengeKalemegdan::izbrisi);

    connect(m_ui->findChild<QPushButton*>("pbA"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("A");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbB"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("B");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbC"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("C");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbCh"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("Č");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbChh"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("Ć");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbD"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("D");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbDz"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("DŽ");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbDj"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("Đ");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbE"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("E");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbF"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("F");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbG"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("G");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbH"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("H");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbI"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("I");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbJ"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("J");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbK"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("K");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbL"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("L");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbLJ"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("LJ");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbM"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("M");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbN"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("N");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbNj"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("NJ");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbO"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("O");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbP"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("P");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbR"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("R");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbS"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("S");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbSh"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("Š");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbT"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("T");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbU"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("U");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbV"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("V");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbZ"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("Z");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbZh"), &QPushButton::clicked, this, [=]() {
        if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) <= 4) {
            m_polja[m_trenutnaPozicija]->setText("Ž");
            // qDebug() << m_trenutnaPozicija << m_pozicijaPocetnogSlova;
            if ((m_trenutnaPozicija - m_pozicijaPocetnogSlova) < 4)
                m_trenutnaPozicija++;
        }
    });

    connect(m_ui->findChild<QPushButton*>("pbReset"), &QPushButton::clicked, this, &ChallengeKalemegdan::zapocniChallenge);
}

ChallengeKalemegdan::~ChallengeKalemegdan() { delete m_ui; }

void ChallengeKalemegdan::zapocniChallenge()
{
    m_trenutnaPozicija = 0;
    m_pozicijaPocetnogSlova = 0;
    m_rec = generisiRec();
    for (int i = 0; i < 30; i++) {
        m_polja[i]->setText("");
        m_polja[i]->setStyleSheet("QLabel{ border: 3px solid rgb(100,100,100); max-width: 50px; "
                                  "max-height: 50px; background-color: #f0f0f0; font: bold 26px; color: "
                                  "#333;}");
    }
}

void ChallengeKalemegdan::zavrsiChallenge() { }

void ChallengeKalemegdan::tekstovi()
{
    m_naslovEdukativnogTeksta = "O Kalemegdanu";
    m_hint = "Saberi se - Oduzmi se";
    m_edukativniTekst =
        R"(Da li Vam je nekim slučajem poznato da je današnji najlepši beogradski park-tvrđava Kalemegdan sve do 1945.godine većim delom bio vojna baza puna skladišta, magacina i kasarni?
Teško je to reći dok šetamo stazama i divimo se pogledu sa njegovih zidina, da je jednom, ne tako davno, pre svega sedam-osam decenija, dakle, u jednom ljudskom veku, kao gradski park-šetalište na Kalemegdanu
služio tek manji deo tvrđave.

Da bi postao ono što danas imamo, Kalemegdan je morao da doživi mnogo rušenja ali i zidanja, koja su sva zajedno formirala njegov sadašnji lik.
Rušen je veliki severni bedem od 1922. do 1926. da bi se napravile današnje Velike stepenice i plato ispod njih. Rušiili su se ostaci kasarni posle njihovog temeljnog oštećenja u Prvom svetskom ratu.

Početkom tridesetih godina na blatnjavoj obali ispod samih bedema započeta je izgradnja velike hale u sklopu beogradske luke, one koju danas znamo kao “Beton halu”. Masivni privredni objekat, sa sve dodatim tunelom
za železničku prugu koja je tuda prolazila, nije se baš uklapao u vizuru Kalemegdana i bio je gotovo slepljen sa tvrđavom.

Ipak, to za užurbani razvoj Beograda početkom te epohe nije predstavljao nikakav problem: u zemlji koja je jurila napred u obnovu posle teškog ratnog stradanja, malo se brinulo o vizurama a mnogo više o privređivanju i poslu.

Ideja koja danas svim ljubiteljima Kalemegdana izaziva najviše užasa lansirana je krajem te dekade od strane nemačkih nacista iz Berlina i njihovih obožavatelja iz redova tadašnje srpske i jugoslovenske politike:
da se na Kalemegdanu grade olimpijski stadioni i razni drugi “prateći sadržaji”.

Samo je izbijanje Drugog svetskog rata i njegov strahoviti dolazak u Beograd u vidu 6. aprila i surove okupacije zauvek odložio takve mahnite ideje, i sačuvao Kalemegdan u ovakvom obliku koji imamo danas.
Ali, nacisti nisu mirovali, fascinirani položajem i drevnom istorijom beogradske tvrđave: nakon što su u bombardovanju 6. aprila 1941. spalili i porušili većinu vojnih kasarni i magacina na Donjem gradu,
tokom okupacije u Drugom svetskom ratu porušili su sve spaljene ruševine, ostavivši samo Karlovu kapiju i kulu Nebojša, i tu 1942. godine napravili park kakav danas poznajemo.
Najveća obnova i uređenje Kalemegdana kako bi dobio puni sjaj najinteresantnijeg mesta u Beogradu, i postao simbol za ponos, izvedeni su tek od 2019. godine, obnovama Velikog i Malog stepeništa,
Pobednika, Spomenika zahvalnosti Francuskoj, vraćanjem dela kompozicije sa spomenika Karađorđu, slepog gusara, dok na red sada najpre čeka obnova Paviljona Cvijeta Zuzorić. )";
    m_asocijacija = "U toku su mini izmene...";
    m_resenjeAsocijacije.append("matematicki fakultet");
    m_resenjeAsocijacije.append("matematički fakultet");
    m_resenjeAsocijacije.append("matf");
}

QStringList ChallengeKalemegdan::generisiRec()
{
    QStringList reci1 = { "g", "o", "l", "u", "b" };
    QStringList reci2 = { "p", "r", "a", "s", "e" };
    QStringList reci3 = { "č", "a", "p", "lj", "a" };
    QStringList reci4 = { "l", "e", "m", "u", "r" };
    QStringList reci5 = { "t", "i", "g", "a", "r" };
    QStringList reci6 = { "k", "a", "v", "e", "z" };
    QStringList reci7 = { "p", "t", "i", "c", "a" };
    QStringList reci8 = { "t", "u", "k", "a", "n" };
    QStringList reci9 = { "v", "i", "d", "r", "a" };

    QList<QStringList> listaReci;
    listaReci.append(reci1);
    listaReci.append(reci2);
    listaReci.append(reci3);
    listaReci.append(reci4);
    listaReci.append(reci5);
    listaReci.append(reci6);
    listaReci.append(reci7);
    listaReci.append(reci8);
    listaReci.append(reci9);
    return listaReci[QRandomGenerator::global()->bounded(100) % listaReci.size()];
}

void ChallengeKalemegdan::izbrisi()
{
    if (m_trenutnaPozicija >= m_pozicijaPocetnogSlova) {
        m_polja[m_trenutnaPozicija]->setText("");
        if (m_trenutnaPozicija > m_pozicijaPocetnogSlova)
            m_trenutnaPozicija--;
    }
}

void ChallengeKalemegdan::setTrenutnaPozicija(int poz) { m_trenutnaPozicija = poz; }
void ChallengeKalemegdan::setPozicijaPocetnogSlova(int poz)
{
    if (poz % 5 == 0)
        m_pozicijaPocetnogSlova = poz;
    else
        m_pozicijaPocetnogSlova = m_trenutnaPozicija % 5;
}

int ChallengeKalemegdan::getTrenutnaPozicija() { return m_trenutnaPozicija; }
int ChallengeKalemegdan::getPozicijaPocetnogSlova() { return m_pozicijaPocetnogSlova; }

void ChallengeKalemegdan::pogodi()
{
    // izbaciti posle!!

    QVector<int> zelenePozicije;
    QVector<int> zutePozicije;
    QStringList rec;
    QStringList trazenaRec = m_rec;
    if (m_trenutnaPozicija - m_pozicijaPocetnogSlova == 4) {
        for (int i = m_pozicijaPocetnogSlova; i <= m_trenutnaPozicija; i++) {
            rec.append((m_polja[i]->text()).toLower());
            // qDebug() << (m_polja[i]->text()).toLower();
        }
        if (m_rec == rec) {
            // ui->setVisible(false);
            QObjectList children = this->children();
            for (QObject* child : children) {
                if (QWidget* widget = qobject_cast<QWidget*>(child)) {
                    widget->setVisible(false);
                }
            }
            QLayout* postojeciLayout = this->layout();
            if (postojeciLayout) {
                delete postojeciLayout;
            }
            emit pogodjenaRec();
        } else {
            qDebug() << trazenaRec;
            // oboj slova
            int j = 0;
            for (int i = m_pozicijaPocetnogSlova; i < m_pozicijaPocetnogSlova + 5 && j < 5; i++, j++) {
                if (rec[j] == trazenaRec[j]) {
                    // qDebug()<< "Pogodjeno slovo na poz "<< j;
                    m_polja[i]->setStyleSheet("QLabel{ text-align: center; max-width: 50px; max-height: 50px; "
                                              "background-color: #4fb946; font: bold 26px; color: #fff;}");
                    m_polja[i]->setAlignment(Qt::AlignCenter);

                    trazenaRec[j] = "0";
                    zelenePozicije.append(i);
                }
            }
            qDebug() << trazenaRec;
            j = 0;
            for (int i = m_pozicijaPocetnogSlova; i < m_pozicijaPocetnogSlova + 5 && j < 5; i++, j++) {
                qDebug() << trazenaRec;

                if (trazenaRec.contains(rec[j])) {
                    // qDebug()<< "Slovo se nalazi u reci "<< j;
                    m_polja[i]->setStyleSheet("QLabel{ text-align: center; max-width: 50px; max-height: 50px; "
                                              "background-color: #fec21c; font: bold 26px; color: #fff;}");
                    m_polja[i]->setAlignment(Qt::AlignCenter);
                    int pozicija = trazenaRec.indexOf(rec[j]);
                    trazenaRec[pozicija] = "0";
                    zutePozicije.append(i);
                } else if (!zelenePozicije.contains(i)) {
                    // qDebug() << "Slovo se ne nalazi u reci "<< j;
                    m_polja[i]->setStyleSheet("QLabel{text-align: center; max-width: 50px; max-height: 50px; "
                                              "background-color: #a0b0c7; font: bold 26px; color: #fff;}");
                    m_polja[i]->setAlignment(Qt::AlignCenter);
                }
            }

            m_pozicijaPocetnogSlova += 5;
            m_trenutnaPozicija++;
            if (m_trenutnaPozicija == 30) {
                // neuspeh u resavanju
                // resetuje se
                zapocniChallenge();
            }
            // qDebug() << m_pozicijaPocetnogSlova << m_trenutnaPozicija << "iz
            // pogodi";
        }
    }
}

QStringList ChallengeKalemegdan::getTrenutnaRec() { return m_rec; }
void ChallengeKalemegdan::setTrenutnaRec(const QStringList& rec)
{
    if (rec.size() == 5)
        m_rec = rec;
    else
        m_rec = generisiRec();
}

QVector<QLabel*>& ChallengeKalemegdan::getPolja() { return m_polja; }
