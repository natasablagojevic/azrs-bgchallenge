
#include "../include/mapa.h"
#include "../include/rezultat.h"
#include <QDebug>
// konstruktor za novog igraca
Mapa::Mapa(Igrac& trenutniIgrac, Rezultat* rez, QObject* parent)
    : m_trenutniIgrac(trenutniIgrac)
    , m_rez(rez)
    , QObject(parent)
{
    qDebug() << m_trenutniIgrac.getIme() << m_trenutniIgrac.getNivo() << m_trenutniIgrac.getPoslednjiOtkljucaniChallenge() << "iz mape";
    m_gameTime = m_trenutniIgrac.getVreme();
    qDebug() << m_gameTime << "iz mape.cpp";
    m_timer = new Timer(this);
    connect(m_timer, &Timer::azurirajVreme, this, &Mapa::azurirajGameTime);
}

Mapa::~Mapa()
{
    delete m_timer;
    m_timer = nullptr;
}
void Mapa::zapocniIgru() { }

void Mapa::zavrsiIgru() { sacuvajRezultat(m_trenutniIgrac.getPoslednjiOtkljucaniChallenge()); }

int Mapa::getGameTime() { return m_gameTime; }

void Mapa::sacuvajRezultat(qint64 id)
{

    qDebug() << id;
    m_trenutniIgrac.setPoslednjiOtkljucaniChallenge(id); // izazov sa presledjenim identifikator je uspesno kompletiran!
    int nivo = 1;
    if (id >= 4)
        nivo = 2;
    if (id >= 8)
        nivo = 3;
    if (id >= 12)
        nivo = 4;
    m_trenutniIgrac.setNivo(nivo);
    m_rez->azurirajIgracaJson("../bgchallenge/resources/savefile.json", m_trenutniIgrac.getIme(), m_trenutniIgrac);
}

void Mapa::resenIzazov(qint64 id)
{
    if (id < 16)
        emit otkljucajNaredniIzazov(id);
    if (id > m_trenutniIgrac.getPoslednjiOtkljucaniChallenge())
        sacuvajRezultat(id); // cuva prvi pokusaj
    if (id == 16) {
        emit cestitajIgracu();
    }
}

void Mapa::azurirajGameTime(qint64 vreme)
{
    m_gameTime++;
    m_trenutniIgrac.setVreme(m_gameTime);

    emit azurirajLCDNumber(m_gameTime);
}
