#include "../include/sveci.h"
#include "../forms/ui_sveci.h"
#include <QMessageBox>
#include <QPainter>
#include <QTime>

Sveci::Sveci(Timer* vreme, int pocetnoVreme, QWidget* parent)
    : ChallengeTest(11, 3, vreme, pocetnoVreme, parent)
    , m_ui(new Ui::Sveci)
{
    m_ui->setupUi(this);

    // pozadina
    this->setImagePath(":/new/sveci/resources/pozadinaa.png");

    // connect(&m_timer, &QTimer::timeout, this, &Sveci::timerExpired);

    /*QPixmap bkgnd(":/new/sveci/resources/pozadinaa.png");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio,
    Qt::SmoothTransformation); QPalette palette;
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);*/
    tekstovi();

    connect(m_vreme, &Timer::azurirajVreme, this, [=](int vreme) {
        m_pocetnoVreme++;
        QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
        m_ui->lcdNumber->display(time.toString("h:mm:ss"));
        update();
    });

    // Kreiranje dugmadi
    QString skriveniBtnStylesheet = "QPushButton { background-color: rgba(0, 0, 0, 0.5); color: white; "
                                    "border: "
                                    "1px solid white; border-radius: 5px; padding: 5px 10px; min-width: "
                                    "100px; "
                                    "max-width:100px; min-height: 100px; max-height:100px;}"
                                    "QPushButton:hover { background-color: rgba(255, 255, 255, 0.2);}"
                                    "QPushButton:pressed { background-color: rgba(255, 255, 255, 0.4);}";

    m_button1 = new QPushButton("F", this);
    m_button1->setVisible(false);
    m_button1->setStyleSheet(skriveniBtnStylesheet);

    m_button2 = new QPushButton("R", this);
    m_button2->setVisible(false);
    m_button2->setStyleSheet(skriveniBtnStylesheet);

    m_button3 = new QPushButton("E", this);
    m_button3->setVisible(false);
    m_button3->setStyleSheet(skriveniBtnStylesheet);

    m_button4 = new QPushButton("S", this);
    m_button4->setVisible(false);
    m_button4->setStyleSheet(skriveniBtnStylesheet);

    m_button5 = new QPushButton("K", this);
    m_button5->setVisible(false);
    m_button5->setStyleSheet(skriveniBtnStylesheet);

    m_button6 = new QPushButton("A", this);
    m_button6->setVisible(false);
    m_button6->setStyleSheet(skriveniBtnStylesheet);

    m_labela = new QLabel();
    m_labela->setText("");

    m_labela->setFont(QFont("ARIAL", 70));
    m_labela->setStyleSheet("border: 2px solid black;");
    m_labela->setStyleSheet("text-decoration: underline;");
    m_labela->setAlignment(Qt::AlignCenter);
    m_ui->horizontalLayout_2->addWidget(m_labela);
    m_ui->horizontalLayout_2->setAlignment(Qt::AlignCenter);

    connect(m_ui->pushButton, SIGNAL(clicked()), this, SLOT(proveri()));
    connect(m_ui->pushButton_2, SIGNAL(clicked()), this, SLOT(proveri2()));
    connect(m_ui->pushButton_3, SIGNAL(clicked()), this, SLOT(proveri3()));
    connect(m_ui->pushButton_4, SIGNAL(clicked()), this, SLOT(proveri4()));
    connect(m_ui->pushButton_5, SIGNAL(clicked()), this, SLOT(proveri5()));
    connect(m_ui->pushButton_6, SIGNAL(clicked()), this, SLOT(proveri6()));

    connect(m_button1, SIGNAL(clicked()), this, SLOT(f()));
    connect(m_button2, SIGNAL(clicked()), this, SLOT(r()));
    connect(m_button3, SIGNAL(clicked()), this, SLOT(e()));
    connect(m_button4, SIGNAL(clicked()), this, SLOT(s()));
    connect(m_button5, SIGNAL(clicked()), this, SLOT(k()));
    connect(m_button6, SIGNAL(clicked()), this, SLOT(a()));

    connect(m_ui->btnNazadNaLokaciju, &QPushButton::clicked, this, &Sveci::zatvori);
}

Sveci::~Sveci() { delete m_ui; }

void Sveci::proveri()
{
    if (m_ui->lineEdit->text().compare("SAVA", Qt::CaseInsensitive) == 0 || m_ui->lineEdit->text().compare("SV SAVA", Qt::CaseInsensitive) == 0
        || m_ui->lineEdit->text().compare("SVETI SAVA", Qt::CaseInsensitive) == 0) {
        // QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

        // Dodavanje dugmadi u horizontalni raspored
        m_ui->horizontalLayout->addWidget(m_button5);
        m_button5->setVisible(true);
    } else {
        // QMessageBox::warning(this,"Greska", "Molim vas pokusajte opet!!!");
        m_ui->lineEdit->clear();
    }
}

void Sveci::proveri2()
{
    if (m_ui->lineEdit_2->text().compare("JOVAN", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_2->text().compare("SV JOVAN", Qt::CaseInsensitive) == 0
        || m_ui->lineEdit_2->text().compare("SVETI JOVAN", Qt::CaseInsensitive) == 0) {
        // QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

        // Dodavanje dugmadi u horizontalni raspored
        m_ui->horizontalLayout->addWidget(m_button4);
        m_button4->setVisible(true);
    } else {
        // QMessageBox::warning(this,"Greska", "Molim vas pokusajte opet!!!");
        m_ui->lineEdit_2->clear();
    }
}
void Sveci::proveri3()
{
    if (m_ui->lineEdit_3->text().compare("PETKA", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_3->text().compare("PARASKEVA", Qt::CaseInsensitive) == 0
        || m_ui->lineEdit_3->text().compare("SV PETKA", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_3->text().compare("SVETA PETKA", Qt::CaseInsensitive) == 0) {
        // QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

        // Dodavanje dugmadi u horizontalni raspored
        m_ui->horizontalLayout->addWidget(m_button6);
        m_button6->setVisible(true);
    } else {
        QMessageBox::warning(this, "Greska", "Molim vas pokusajte opet!!!");
        m_ui->lineEdit_3->clear();
    }
}
void Sveci::proveri4()
{
    if (m_ui->lineEdit_4->text().compare("VASILIJE", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_4->text().compare("SV VASILIJE", Qt::CaseInsensitive) == 0
        || m_ui->lineEdit_4->text().compare("SVETI VASILIJE", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_4->text().compare("VASILIJE OSTROŠKI", Qt::CaseInsensitive) == 0
        || m_ui->lineEdit_4->text().compare("SV VASILIJE OSTROŠKI", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_4->text().compare("SVETI VASILIJE OSTROŠKI", Qt::CaseInsensitive) == 0) {
        // QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

        // Dodavanje dugmadi u horizontalni raspored
        m_ui->horizontalLayout->addWidget(m_button3);
        m_button3->setVisible(true);
    } else {
        // QMessageBox::warning(this,"Greska", "Molim vas pokusajte opet!!!");
        m_ui->lineEdit_4->clear();
    }
}
void Sveci::proveri5()
{
    if (m_ui->lineEdit_5->text().compare("GEORGIJE", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_5->text().compare("SV GEORGIJE", Qt::CaseInsensitive) == 0
        || m_ui->lineEdit_5->text().compare("SVETI GEORGIJE", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_5->text().compare("ĐORĐE", Qt::CaseInsensitive) == 0
        || m_ui->lineEdit_5->text().compare("SV ĐORĐE", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_5->text().compare("SVETI ĐORĐE", Qt::CaseInsensitive) == 0
        || m_ui->lineEdit_5->text().compare("DJORDJE", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_5->text().compare("SV DJORDJE", Qt::CaseInsensitive) == 0
        || m_ui->lineEdit_5->text().compare("SVETI DJORDJE", Qt::CaseInsensitive) == 0) {
        // QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

        // Dodavanje dugmadi u horizontalni raspored
        m_ui->horizontalLayout->addWidget(m_button2);
        m_button2->setVisible(true);
    } else {
        // QMessageBox::warning(this,"Greska", "Molim vas pokusajte opet!!!");
        m_ui->lineEdit_5->clear();
    }
}
void Sveci::proveri6()
{
    if (m_ui->lineEdit_6->text().compare("BELI ANDJEO", Qt::CaseInsensitive) == 0 || m_ui->lineEdit_6->text().compare("BELI ANĐEO", Qt::CaseInsensitive) == 0) {
        // QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

        // Dodavanje dugmadi u horizontalni raspored
        m_ui->horizontalLayout->addWidget(m_button1);
        m_button1->setVisible(true);
    } else {
        // QMessageBox::warning(this,"Greska", "Molim vas pokusajte opet!!!");
        m_ui->lineEdit_6->clear();
    }
}

void Sveci::f()
{
    // QLabel *labela = new QLabel();
    m_labela->setText(m_labela->text() + "F");
    // ui->horizontalLayout_2->addWidget(labela);
    m_button1->setEnabled(false);

    if (m_labela->text() == "FRESKA") {
        QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
        m_timer.stop();
        // ui->setVisible(false);

        QObjectList children = this->children();
        for (QObject* child : children) {
            if (QWidget* widget = qobject_cast<QWidget*>(child)) {
                widget->setVisible(false);
            }
        }
        QLayout* postojeciLayout = this->layout();
        if (postojeciLayout) {
            delete postojeciLayout;
        }
        this->prikaziEdukativniTekst();
        // zavrsiChallenge();
    } else if (m_labela->text().length() == 6) {
        QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
        m_labela->setText("");
        m_button1->setEnabled(true);
        m_button2->setEnabled(true);
        m_button3->setEnabled(true);
        m_button4->setEnabled(true);
        m_button5->setEnabled(true);
        m_button6->setEnabled(true);
    }
}
void Sveci::r()
{
    // QLabel *labela = new QLabel();
    m_labela->setText(m_labela->text() + "R");
    // ui->horizontalLayout_2->addWidget(labela);
    m_button2->setEnabled(false);

    if (m_labela->text() == "FRESKA") {
        QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
        zavrsiChallenge();
    } else if (m_labela->text().length() == 6) {
        QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
        m_labela->setText("");
        m_button1->setEnabled(true);
        m_button2->setEnabled(true);
        m_button3->setEnabled(true);
        m_button4->setEnabled(true);
        m_button5->setEnabled(true);
        m_button6->setEnabled(true);
    }
}

void Sveci::e()
{
    // QLabel *labela = new QLabel();
    m_labela->setText(m_labela->text() + "E");
    // ui->horizontalLayout_2->addWidget(labela);
    m_button3->setEnabled(false);

    if (m_labela->text() == "FRESKA") {
        QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
        zavrsiChallenge();
    } else if (m_labela->text().length() == 6) {
        QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
        m_labela->setText("");
        m_button1->setEnabled(true);
        m_button2->setEnabled(true);
        m_button3->setEnabled(true);
        m_button4->setEnabled(true);
        m_button5->setEnabled(true);
        m_button6->setEnabled(true);
    }
}
void Sveci::s()
{
    // QLabel *labela = new QLabel();
    m_labela->setText(m_labela->text() + "S");
    // ui->horizontalLayout_2->addWidget(labela);
    m_button4->setEnabled(false);

    if (m_labela->text() == "FRESKA") {
        QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
        zavrsiChallenge();
    } else if (m_labela->text().length() == 6) {
        QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
        m_labela->setText("");
        m_button1->setEnabled(true);
        m_button2->setEnabled(true);
        m_button3->setEnabled(true);
        m_button4->setEnabled(true);
        m_button5->setEnabled(true);
        m_button6->setEnabled(true);
    }
}
void Sveci::k()
{
    // QLabel *labela = new QLabel();
    m_labela->setText(m_labela->text() + "K");
    // ui->horizontalLayout_2->addWidget(labela);
    m_button5->setEnabled(false);

    if (m_labela->text() == "FRESKA") {
        QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
        zavrsiChallenge();
    } else if (m_labela->text().length() == 6) {
        QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
        m_labela->setText("");
        m_button1->setEnabled(true);
        m_button2->setEnabled(true);
        m_button3->setEnabled(true);
        m_button4->setEnabled(true);
        m_button5->setEnabled(true);
        m_button6->setEnabled(true);
    }
}
void Sveci::a()
{
    // QLabel *labela = new QLabel();
    m_labela->setText(m_labela->text() + "A");
    // ui->horizontalLayout_2->addWidget(labela);
    m_button6->setEnabled(false);

    if (m_labela->text() == "FRESKA") {
        QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
        m_ui->btnNazadNaLokaciju->setVisible(false);
        m_ui->pushButton->setVisible(false);
        m_ui->pushButton_2->setVisible(false);
        m_ui->pushButton_3->setVisible(false);
        m_ui->pushButton_4->setVisible(false);
        m_ui->pushButton_5->setVisible(false);
        m_ui->pushButton_6->setVisible(false);

        m_ui->btnNazadNaLokaciju->setVisible(false);
        m_ui->label->setVisible(false);
        m_ui->label_2->setVisible(false);
        m_ui->label_3->setVisible(false);
        m_ui->label_4->setVisible(false);
        m_ui->label_5->setVisible(false);
        m_ui->label_6->setVisible(false);
        m_ui->label_7->setVisible(false);
        m_ui->label_8->setVisible(false);
        m_ui->label_9->setVisible(false);
        m_ui->label_10->setVisible(false);
        m_ui->label_11->setVisible(false);
        m_ui->label_12->setVisible(false);
        m_labela->setVisible(false);

        m_ui->lineEdit->setVisible(false);
        m_ui->lineEdit_2->setVisible(false);
        m_ui->lineEdit_3->setVisible(false);
        m_ui->lineEdit_4->setVisible(false);
        m_ui->lineEdit_5->setVisible(false);
        m_ui->lineEdit_6->setVisible(false);

        m_button1->setVisible(false);
        m_button2->setVisible(false);
        m_button3->setVisible(false);
        m_button4->setVisible(false);
        m_button5->setVisible(false);
        m_button6->setVisible(false);
        m_timer.stop();
        // ui->setVisible(false);

        QObjectList children = this->children();
        for (QObject* child : children) {
            if (QWidget* widget = qobject_cast<QWidget*>(child)) {
                widget->setVisible(false);
            }
        }
        QLayout* postojeciLayout = this->layout();
        if (postojeciLayout) {
            delete postojeciLayout;
        }

        prikaziEdukativniTekst();
        zavrsiChallenge();
    } else if (m_labela->text().length() == 6) {
        QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
        m_labela->setText("");
        m_button1->setEnabled(true);
        m_button2->setEnabled(true);
        m_button3->setEnabled(true);
        m_button4->setEnabled(true);
        m_button5->setEnabled(true);
        m_button6->setEnabled(true);
    }
}

void Sveci::tekstovi()
{

    m_edukativniTekst = R"(
Crkva Svetog Marka je izgrađena između 1931. i 1940. godine, ali je po izbijanju Drugog Svetskog rata prekinuta izgradnja. Tada u tom periodu su izvršeni samo građevinski radovi, ali su uprkos tome bogosluženja bila vršena i tokom i posle rata, sve do 14. novembra 1948.

Izgrađena je u srpsko-vizantijskom stilu, po modelu manastira Gračanica. Spoljna fasada urađena je polihromno, tačnije u više boja, a sama veličina zadnja je impozantna i veličanstvena. Iznad ulaznih vrata na spoljnoj fasadi nalazi se mozaička ikona Svetog Apostola i Evangelista Marka, iz 1961. godine, delo Veljka Stojanovića. Prostire se preko 1150 kvadratnih metara, a njen naos može da ugosti i do 2000 vernika, a horska galerija preko 150 pevača.

Ikonostas je rađen po projektu Zorana Petrovića, a napravljen je od mermera dok su ikone koje ga krase rađene tehnikom mozaika i delo su Đure Radlovića, akademskog slikara. Pored ikonostasa u mermeru rađena je i časna trpeza, a ukrašena je manjim mozaicima na prednjoj strani.

Sa desne strane glavnog oltara nalazi se znatno manji oltar Svetom despotu Stefanu Lazareviću, dok se sa leve strane nalazi oltar posvećen prazniku Preobraženja Gospodnjeg. Grobnica cara Stefana Dušana se nalazi uz južni zid, rađena po projektu Dragomira Tadića, u mermeru, gde su položene mošti cara Stefana Dušana donesene iz manastira Svetih Arhandjela. Nasuprot grobnice cara Stefana Dušana izgrađena je grobnica patrijarha Germana.

Na sredini se nalazi bakarni polijelej, rađen po projektu Dragomira Tadića, a napravio ga je Dragutin Petrović.

Kripta se nalazi ispod crkve, a urađena je 2007. godine, u njoj se nalaze mošti Mitropolita Teodosija, Episkopa niškog Viktora, Episkopa šabačkog Gavrila i Episkopa timočkog Mojsija, takođe se nalaze i posmrtni ostaci Kralja Aleksandra Obrenovića i Kraljice Drage, Ane Jovane Obrenović, Knjaza Milana Obrenovića i Kneževića Sergija Obrenovića, kao i grob ktitora starog hrama Lazara Pančea)";
    m_asocijacija = "Tamo se održavaju dobri koncerti (na otvorenom)";

    m_resenjeAsocijacije.append("tasmajdan");
    m_resenjeAsocijacije.append("tašmajdan");

    m_hint = "Možeš i da treniraš, i da plivaš na bazenima, i da šetaš kučiće, i "
             "da ideš na koncerte, sve to pored Bulevara ";
    m_naslovEdukativnogTeksta = "O Crkvi Svetog Marka";
}
