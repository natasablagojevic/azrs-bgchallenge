#include "../include/kljucevi.h"
#include "../forms/ui_kljucevi.h"
#include <QGraphicsOpacityEffect>
#include <QGraphicsSceneMouseEvent>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMovie>
#include <QPropertyAnimation>
#include <QPushButton>
#include <QRandomGenerator>
#include <QString>
#include <QTimer>
#include <iostream>

Kljucevi::Kljucevi(Timer* vreme, int pocetnoVreme, QWidget* parent)
    : ChallengeTest(9, 3, vreme, pocetnoVreme, parent)
    , m_ui(new Ui::Kljucevi)
    , m_izabranaSlika(nullptr)
    , m_brojKljuceva(4)
{
    m_ui->setupUi(this);
    // this->setStyleSheet("background-color: #333333;");
    m_player = new QMediaPlayer();
    m_audioOutput = new QAudioOutput();
    m_player->setAudioOutput(m_audioOutput);
    m_player->setSource(QUrl("qrc:/new/kljucevi/resources/dorm-door-opening-6038.mp3"));
    m_audioOutput->setVolume(10);

    setImagePath(":/new/pozadine/resources/moskva_bg.png");
    m_scene = new QGraphicsScene(this);
    m_scene->setSceneRect(0, 0, 400, 400);

    tekstovi();
    connect(m_vreme, &Timer::azurirajVreme, this, [=](int vreme) {
        m_pocetnoVreme++;
        QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
        m_ui->lcdNumber->display(time.toString("h:mm:ss"));
        update();
    });

    // Dodavanje fiksne slike(vrata)
    m_fiksnaSlika1 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/vrataZ.png"));
    m_fiksnaSlika1->setPos(400, 2);
    m_fiksnaSlika1->setScale(1.5);

    m_fiksnaSlika2 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/vrataZ.png"));
    m_fiksnaSlika2->setPos(150, 2);
    m_fiksnaSlika2->setScale(1.5);

    m_fiksnaSlika3 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/vrataZ.png"));
    m_fiksnaSlika3->setPos(-100, 2);
    m_fiksnaSlika3->setScale(1.5);

    m_fiksnaSlika4 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/vrataZ.png"));
    m_fiksnaSlika4->setPos(-350, 2);
    m_fiksnaSlika4->setScale(1.5);

    // Dodavanje pokretne slike(kljuc)
    m_pokretnaSlika1 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/kljucc.png"));
    m_pokretnaSlika1->setFlag(QGraphicsItem::ItemIsMovable);
    m_pokretnaSlika1->setPos(400, 350);

    m_pokretnaSlika2 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/kljucc.png"));
    m_pokretnaSlika2->setFlag(QGraphicsItem::ItemIsMovable);
    m_pokretnaSlika2->setPos(150, 350);

    m_pokretnaSlika3 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/kljucc.png"));
    m_pokretnaSlika3->setFlag(QGraphicsItem::ItemIsMovable);
    m_pokretnaSlika3->setPos(-100, 350);

    m_pokretnaSlika4 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/kljucc.png"));
    m_pokretnaSlika4->setFlag(QGraphicsItem::ItemIsMovable);
    m_pokretnaSlika4->setPos(-350, 350);

    // Dodavanje na scenu
    m_scene->addItem(m_fiksnaSlika1);
    m_scene->addItem(m_fiksnaSlika2);
    m_scene->addItem(m_fiksnaSlika3);
    m_scene->addItem(m_fiksnaSlika4);
    m_scene->addItem(m_pokretnaSlika1);
    m_scene->addItem(m_pokretnaSlika2);
    m_scene->addItem(m_pokretnaSlika3);
    m_scene->addItem(m_pokretnaSlika4);

    m_ui->graphicsView->setScene(m_scene);
    m_ui->graphicsView->installEventFilter(this);
    m_ui->graphicsView->scene()->installEventFilter(this);

    connect(m_ui->btnNazadNaLokaciju, &QPushButton::clicked, this, &Kljucevi::zatvori);
}

Kljucevi::~Kljucevi()
{
    if (m_player != nullptr) {
        qDebug() << "obrisan player";
        delete m_player;
        m_player = nullptr;
    };

    if (m_audioOutput != nullptr) {
        qDebug() << "obrisan audioOutput";
        delete m_audioOutput;
        m_audioOutput = nullptr;
    };
    delete m_ui;
}

bool Kljucevi::eventFilter(QObject* watched, QEvent* event)
{
    if (watched == m_ui->graphicsView->scene()) {

        if (event->type() == QEvent::GraphicsSceneMousePress) {
            QGraphicsSceneMouseEvent* mouseEvent = dynamic_cast<QGraphicsSceneMouseEvent*>(event);
            QGraphicsItem* item = m_ui->graphicsView->scene()->itemAt(mouseEvent->scenePos(), m_ui->graphicsView->transform());

            if (item && item->type() == QGraphicsPixmapItem::Type && (item->flags() & QGraphicsItem::ItemIsMovable)) {

                m_izabranaSlika = qgraphicsitem_cast<QGraphicsPixmapItem*>(item);
                m_inicijalnaPozicijaSlike = m_izabranaSlika->pos();
                m_ui->graphicsView->scene()->setFocusItem(m_izabranaSlika);
            }
        } else if (event->type() == QEvent::GraphicsSceneMouseRelease && m_izabranaSlika) {
            QGraphicsSceneMouseEvent* mouseEvent = dynamic_cast<QGraphicsSceneMouseEvent*>(event);
            QGraphicsItem* dropItem = m_ui->graphicsView->scene()->itemAt(mouseEvent->scenePos(), m_ui->graphicsView->transform());

            if (dropItem && dropItem->type() == QGraphicsPixmapItem::Type && (dropItem->flags() & QGraphicsItem::ItemIsMovable)) {

                m_izabranaSlika->setPos(dropItem->pos());
                // Dodaj dodatnu proveru da li je pravi kljuc postavljen iznad pravih
                // vrata
                if (m_izabranaSlika == m_pokretnaSlika1 && m_izabranaSlika->collidesWithItem(m_fiksnaSlika1)) {
                    // QMessageBox::information(this, "Bravo", "Čestitamo! Otvorili ste
                    // vrata!");
                    m_player->play();
                    m_fiksnaSlika1->setPixmap(QPixmap(":/new/resources/resources/vrataO.png"));
                    m_izabranaSlika->setVisible(false);
                    m_brojKljuceva--;
                } else if (m_izabranaSlika == m_pokretnaSlika2 && m_izabranaSlika->collidesWithItem(m_fiksnaSlika2)) {
                    m_player->play();
                    m_fiksnaSlika2->setPixmap(QPixmap(":/new/resources/resources/vrataO.png"));
                    m_izabranaSlika->setVisible(false);
                    m_brojKljuceva--;
                } else if (m_izabranaSlika == m_pokretnaSlika3 && m_izabranaSlika->collidesWithItem(m_fiksnaSlika3)) {
                    m_player->play();
                    m_fiksnaSlika3->setPixmap(QPixmap(":/new/resources/resources/vrataO.png"));
                    m_izabranaSlika->setVisible(false);
                    m_brojKljuceva--;
                } else if (m_izabranaSlika == m_pokretnaSlika4 && m_izabranaSlika->collidesWithItem(m_fiksnaSlika4)) {
                    m_fiksnaSlika4->setPixmap(QPixmap(":/new/resources/resources/vrataO.png"));
                    m_player->play();
                    m_izabranaSlika->setVisible(false);
                    m_brojKljuceva--;
                } else {

                    m_izabranaSlika->setPos(m_inicijalnaPozicijaSlike);
                }
            }
            // Otvorena sva vrata
            if (m_brojKljuceva == 0) {
                QMessageBox::information(this, "Bravo", "Čestitamo! Otvorili ste vrata!");
                m_timer.stop();
                // ui->setVisible(false);
                QObjectList children = this->children();
                for (QObject* child : children) {
                    if (QWidget* widget = qobject_cast<QWidget*>(child)) {
                        widget->setVisible(false);
                    }
                }
                QLayout* postojeciLayout = this->layout();
                if (postojeciLayout) {
                    delete postojeciLayout;
                }
                this->prikaziEdukativniTekst();
                // zavrsiChallenge();
            }

            m_izabranaSlika = nullptr;
            m_ui->graphicsView->scene()->setFocusItem(nullptr);
        } else if (event->type() == QEvent::GraphicsSceneMouseMove && m_izabranaSlika) {
            QGraphicsSceneMouseEvent* mouseEvent = dynamic_cast<QGraphicsSceneMouseEvent*>(event);
            m_izabranaSlika->setPos(mouseEvent->scenePos() - m_izabranaSlika->boundingRect().center());
        }
    }

    return QWidget::eventFilter(watched, event);
}

void Kljucevi::tekstovi()
{

    m_edukativniTekst = R"(
Hotel Moskva predstavlja jedno od najznačajnijih arhitektonskih dragulja srpske prestonice, izgrađenog u stilu ruske secesije, koji je pod zaštitom države od druge polovine prošlog veka. Tradicija postojanja i poslovanja duža je od jednog veka, učinila je da ovaj hotel poseti preko 45 miliona ljudi.

Pored njene fascinantne istorije, najpoznatija je i po tome što je od trenutka kada je otvorila svoja vrata privukla i oduševila na stotine slavnih ličnosti.
Odmah po otvaranju, 1908. kroz vrata su ušetali izumitelji teorije relativiteta Albert Ajnštajn i njegova žena, naučnica srpskog porekla Mileva Marić. Upravo zbog njihove posete ovom hotelu, predsednički apartman nosi Ajnštajnovo ime. Neki od najpoznatijih svetskih vokala poput Luciana Pavarottija i Reja Carlsa odabrali su baš nju, hotel Moskvu. Džek Nikolson, Robert De Niro, Kirk i Majkl Daglas pridružili su se čitavom nizu glumačkih legendi koje su uživale u raskoši ovog hotela. Takođe, i 37. predsednik Sjedinjenih Američkih Država Ričard Nikson boravio je u hotelu Moskva.
Ipak, najznačajniji od svih slavnih ličnosti jesu književnici koji su hotel Moskvu iz decenije u deceniju činili kulturnim i Književnim centrom. Neka od velikih spisateljskih imena koji krase ovu listu su Jean-Paul Sartre, Orson Welles i Maksim Gorki.

Jedan od prvih gostiju hotela Moskva bio je Branislav Nušić, slavni srpski pisac i redovni gost beogradskih kafana. Stevan Sremac je takođe bio redovan gost ove ustanove. Ovaj slavni srpski akademik, pisac realizma, je navodno prilagodio svoj raspored predavanja u jednoj od beogradskih gimnazija kako bi mogao duže da spava jer je do kasno ostajao u Velikoj Srbiji.
Interesantno je da je hotel Moskva jedini hotel u Beogradu koji nema sobu, a ni apartman pod brojem 13. U toku Drugog svetskog rata, Hotel Moskva je bio glavni štab Gestapoa.

Istoričari koji poznaju probleme srpske duše kažu da su Srbi zavoleli hotel Moskvu jer ih fasada hotela i naziv podsećaju na Rusiju, a unutra je komforno kao u Beču.)";

    m_asocijacija = "Usvaja i menja Ustav";

    m_resenjeAsocijacije.append("skupstina");
    m_resenjeAsocijacije.append("skupština");
    m_resenjeAsocijacije.append("narodna skupstina");
    m_resenjeAsocijacije.append("narodna skupština");
    m_resenjeAsocijacije.append("nardona skupština");

    m_naslovEdukativnogTeksta = "O Hotelu Moskva";

    m_hint = "Čini je 250 narodnih poslanika";
}
