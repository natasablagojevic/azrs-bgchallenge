#include "../include/vesalice.h"
#include "../forms/ui_vesalice.h"
#include <QFont>
#include <QInputDialog>
#include <QMessageBox>
#include <QTextEdit>
#include <QUiLoader>

Vesalice::Vesalice(Timer* vreme, int pocetnoVreme, QWidget* parent)
    : m_ui(new Ui::vesalice)
    , ChallengeTest(8, 2, vreme, pocetnoVreme, parent)
    , m_rec("JUGOSLAVIJA")
    , m_brojgreske(0)
    , m_brojSlova(0)
    , m_igraZavrsena(false)
{
    m_ui->setupUi(this);

    this->setImagePath(":/new/resources/resources/muzej(1).jpg");
    // QPixmap bkgnd(":/resources/jugoslavija.jpg");
    // bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio,
    // Qt::SmoothTransformation);
    tekstovi();
    m_ui->lineEdit->setVisible(true);
    m_ui->pbProveri->setVisible(true);

    connect(m_vreme, &Timer::azurirajVreme, this, [=](int vreme) {
        m_pocetnoVreme++;
        QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
        m_ui->lcdNumber->display(time.toString("h:mm:ss"));
        update();
    });

    connect(m_ui->pbA, SIGNAL(clicked()), this, SLOT(slovoA()));
    connect(m_ui->pbB, SIGNAL(clicked()), this, SLOT(slovoB()));
    connect(m_ui->pbV, SIGNAL(clicked()), this, SLOT(slovoV()));
    connect(m_ui->pbG, SIGNAL(clicked()), this, SLOT(slovoG()));
    connect(m_ui->pbD, SIGNAL(clicked()), this, SLOT(slovoD()));
    connect(m_ui->pbDj, SIGNAL(clicked()), this, SLOT(slovoDj()));
    connect(m_ui->pbE, SIGNAL(clicked()), this, SLOT(slovoE()));
    connect(m_ui->pbZz, SIGNAL(clicked()), this, SLOT(slovoZz()));
    connect(m_ui->pbZ, SIGNAL(clicked()), this, SLOT(slovoZ()));
    connect(m_ui->pbI, SIGNAL(clicked()), this, SLOT(slovoI()));
    connect(m_ui->pbJ, SIGNAL(clicked()), this, SLOT(slovoJ()));
    connect(m_ui->pbK, SIGNAL(clicked()), this, SLOT(slovoK()));
    connect(m_ui->pbL, SIGNAL(clicked()), this, SLOT(slovoL()));
    connect(m_ui->pbLj, SIGNAL(clicked()), this, SLOT(slovoLj()));
    connect(m_ui->pbM, SIGNAL(clicked()), this, SLOT(slovoM()));
    connect(m_ui->pbN, SIGNAL(clicked()), this, SLOT(slovoN()));
    connect(m_ui->pbNj, SIGNAL(clicked()), this, SLOT(slovoNj()));
    connect(m_ui->pbO, SIGNAL(clicked()), this, SLOT(slovoO()));
    connect(m_ui->pbP, SIGNAL(clicked()), this, SLOT(slovoP()));
    connect(m_ui->pbR, SIGNAL(clicked()), this, SLOT(slovoR()));
    connect(m_ui->pbS, SIGNAL(clicked()), this, SLOT(slovoS()));
    connect(m_ui->pbT, SIGNAL(clicked()), this, SLOT(slovoT()));
    connect(m_ui->pbCc, SIGNAL(clicked()), this, SLOT(slovoCc()));
    connect(m_ui->pbU, SIGNAL(clicked()), this, SLOT(slovoU()));
    connect(m_ui->pbF, SIGNAL(clicked()), this, SLOT(slovoF()));
    connect(m_ui->pbH, SIGNAL(clicked()), this, SLOT(slovoH()));
    connect(m_ui->pbC, SIGNAL(clicked()), this, SLOT(slovoC()));
    connect(m_ui->pbCcc, SIGNAL(clicked()), this, SLOT(slovoCcc())); //č
    connect(m_ui->pbDz, SIGNAL(clicked()), this, SLOT(slovoDz()));
    connect(m_ui->pb, SIGNAL(clicked()), this, SLOT(slovoS2()));
    connect(m_ui->pbProveri, SIGNAL(clicked()), this, SLOT(proveri()));

    connect(m_ui->bthNazadNaLokaciju, &QPushButton::clicked, this, &Vesalice::zatvori);
}

Vesalice::~Vesalice() { delete m_ui; }

void Vesalice::paintEvent(QPaintEvent* event)
{

    // Q_UNUSED(event);

    // QPainter painter(this);

    ChallengeTest::paintEvent(event);

    /*QPixmap backgroundImage(imagePath);
    painter.drawPixmap(rect(), backgroundImage, backgroundImage.rect());*/
    if (!m_igraZavrsena) {
        QPainter painterVesalice(this);
        painterVesalice.setRenderHint(QPainter::Antialiasing);
        drawGallows(painterVesalice);
        drawHangman(painterVesalice, m_brojgreske);
        // QPainter painter(this);
    }
}

void Vesalice::drawHead(QPainter& painter)
{
    painter.setPen(QPen(QColor(233, 96, 69), 5));
    // painter.setBrush(Qt::white);
    painter.drawEllipse(50, 50, 100, 100); // Glava
}

void Vesalice::drawBody(QPainter& painter)
{
    painter.setPen(QPen(QColor(233, 96, 69), 5));
    painter.drawLine(100, 150, 100, 250);
}
void Vesalice::drawLeftArm(QPainter& painter) { painter.drawLine(100, 175, 50, 200); }
void Vesalice::drawRightArm(QPainter& painter) { painter.drawLine(100, 175, 150, 200); }
void Vesalice::drawLeftLeg(QPainter& painter)
{
    // Crtanje leve noge
    painter.drawLine(100, 250, 50, 300);
}
void Vesalice::drawRightLeg(QPainter& painter)
{
    // Crtanje desne noge
    painter.drawLine(100, 250, 150, 300);
}
void Vesalice::drawLeftEye(QPainter& painter)
{
    painter.setBrush(Qt::black);
    painter.drawEllipse(75, 75, 10, 10); // Levo oko
}
void Vesalice::drawRightEye(QPainter& painter)
{
    painter.setBrush(Qt::black);
    painter.drawEllipse(115, 75, 10, 10); // Desno oko
}

void Vesalice::drawMouth(QPainter& painter) { painter.drawArc(75, 120, 50, 30, 30 * 16, 120 * 16); }

void Vesalice::drawHair(QPainter& painter)
{
    int xOffset = 100; // Horizontalna pozicija sredine glave
    int yOffset = 100; // Vertikalna pozicija sredine glave
    int headRadius = 50; // Radijus glave

    // Broj linija kose
    int numLines = 20; // Broj linija kose

    // Inicijalizacija generatora slučajnih brojeva
    srand(time(nullptr));

    // Kreiranje putanje kose kao QPainterPath
    QPainterPath hairPath;

    for (int i = 0; i < numLines; ++i) {
        // Određivanje koordinata početne tačke linije (na obodu glave)
        double angle = (i + numLines) * (M_PI / numLines); // Ugao u radijanima (drugi polukrug)

        int x1 = xOffset + headRadius * qCos(angle);
        int y1 = yOffset + headRadius * qSin(angle);

        // Generisanje slučajne dužine za svaku liniju
        int hairLength = 30; // Linije su u rasponu od 20 do 49 piksela

        // Određivanje koordinata krajnje tačke linije
        int x2 = x1 + hairLength * qCos(angle);
        int y2 = y1 + hairLength * qSin(angle);

        // Dodavanje segmenta putanje
        hairPath.moveTo(x1, y1);

        // Dodavanje sinusne krivulje
        for (int j = 1; j <= hairLength; ++j) {
            double t = static_cast<double>(j) / hairLength; // Parametar na osnovu dužine linije
            int x = x1 + t * (x2 - x1);
            int y = y1 + t * (y2 - y1) + 3 * qSin(4 * M_PI * t); // 3 * qSin(4 * M_PI * t) dodaje krivudavost
            hairPath.lineTo(x, y);
        }
    }

    // Postavljanje stila linije
    // QPen pen(Qt::black);
    QPen pen(QColor(233, 96, 69));
    pen.setWidth(1); // Promenjena debljina linije na 1

    // Stvaranje konture putanje
    QPainterPathStroker stroker;
    stroker.setWidth(0.0001); // Promenjena debljina konture na 1
    stroker.setCapStyle(Qt::RoundCap);
    stroker.setJoinStyle(Qt::RoundJoin);
    QPainterPath hairOutline = stroker.createStroke(hairPath);

    // Crtanje konture kose
    painter.setPen(pen);
    painter.drawPath(hairOutline);
}

void Vesalice::drawEars(QPainter& painter)
{
    int xOffset = 100; // Horizontalna pozicija sredine glave
    int yOffset = 100; // Vertikalna pozicija sredine glave
    int headRadius = 50; // Radijus glave

    // Crtanje leve uši
    painter.setBrush(Qt::black);
    painter.drawEllipse(xOffset + headRadius - 10, yOffset - 25, 20, 30);

    // Crtanje desne uši
    painter.drawEllipse(xOffset - headRadius - 10, yOffset - 25, 20, 30);
}

void Vesalice::drawTear(QPainter& painter, int x, int y)
{
    // Kreiranje putanje za kapljicu
    QPainterPath tearPath;

    // Postavljanje položaja kapljice
    QPointF top(x, y); // Vrh kapljice
    QPointF left(x - 5, y + 15); // Leva tačka na dnu kapljice
    QPointF right(x + 5, y + 15); // Desna tačka na dnu kapljice
    QPointF control(x, y + 20); // Kontrolna tačka za oblik kapljice

    // Dodavanje oblika kapljice
    tearPath.moveTo(top);
    tearPath.quadTo(left, control);
    tearPath.quadTo(right, top);

    // Postavljanje boje i popunjavanje kapljice
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::blue); // Boja kapljice

    // Crtanje kapljice
    painter.drawPath(tearPath);
}
void Vesalice::drawGallows(QPainter& painter)
{
    // Postavljanje boje i debljine linije vešala
    painter.setPen(QPen(Qt::black, 3));

    // Crtanje horizontalne grede vešala
    painter.drawLine(15, 10, 100, 10);

    // Crtanje vertikalne grede vešala
    painter.drawLine(15, 10, 15, 350);

    // Crtanje vertikalne šipke za vešanje
    painter.drawLine(100, 10, 100, 20);
}

void Vesalice::slovoA()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'A') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("A");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbA->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbA->setEnabled(false);
}

void Vesalice::slovoB()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'B') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("B");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbB->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbB->setEnabled(false);
}

void Vesalice::slovoV()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'V') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("V");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbV->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbV->setEnabled(false);
}

void Vesalice::slovoG()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'G') {
                m_brojSlova++;
                pronadjen = 1;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("G");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbG->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbG->setEnabled(false);
}

void Vesalice::slovoD()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'D') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("D");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbD->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbD->setEnabled(false);
}

void Vesalice::slovoDj()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == QChar(0x110)) {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("Đ");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbDj->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbDj->setEnabled(false);
}

void Vesalice::slovoE()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'E') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("E");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbE->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbE->setEnabled(false);
}

void Vesalice::slovoZz()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == QChar(0x17D)) {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("Ž");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbZz->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbZz->setEnabled(false);
}

void Vesalice::slovoZ()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'E') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("E");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbZ->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbZ->setEnabled(false);
}

void Vesalice::slovoI()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'I') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("I");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbI->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbI->setEnabled(false);
}

void Vesalice::slovoJ()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'J') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("J");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbJ->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbJ->setEnabled(false);
}

void Vesalice::slovoK()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'K') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("K");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbK->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbK->setEnabled(false);
}

void Vesalice::slovoL()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'L') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("L");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbL->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbL->setEnabled(false);
}

void Vesalice::slovoLj()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec.mid(i, 2) == "Lj") {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("Lj");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbLj->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbLj->setEnabled(false);
}

void Vesalice::slovoM()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'M') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("M");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();
    m_ui->pbM->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbM->setEnabled(false);
}

void Vesalice::slovoN()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'N') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("N");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbN->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbN->setEnabled(false);
}

void Vesalice::slovoNj()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec.mid(i, 2) == "Nj") {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("Nj");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbNj->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbNj->setEnabled(false);
}

void Vesalice::slovoO()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'O') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("O");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbO->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbO->setEnabled(false);
}

void Vesalice::slovoP()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'P') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("P");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbP->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbP->setEnabled(false);
}

void Vesalice::slovoR()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'R') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("R");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();
    m_ui->pbR->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbR->setEnabled(false);
}

void Vesalice::slovoS()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'S') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("S");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbS->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbS->setEnabled(false);
}

void Vesalice::slovoT()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'T') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("T");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbT->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbT->setEnabled(false);
}

void Vesalice::slovoCc()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == QChar(0x106)) {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("Ć");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbCc->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbCc->setEnabled(false);
}

void Vesalice::slovoU()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'U') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("U");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbU->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbU->setEnabled(false);
}

void Vesalice::slovoF()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'F') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("F");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbF->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbF->setEnabled(false);
}

void Vesalice::slovoH()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'H') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("H");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbH->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbH->setEnabled(false);
}

void Vesalice::slovoC()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == 'C') {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("C");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbC->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbC->setEnabled(false);
}

void Vesalice::slovoCcc()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == QChar(0x160)) {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("Č");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbCcc->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbCcc->setEnabled(false);
}

void Vesalice::slovoDz()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == QChar(0x1C4)) {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("Dž");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pbDz->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pbDz->setEnabled(false);
}

void Vesalice::slovoS2()
{
    int pronadjen = 0;

    if (m_brojgreske < 11) {
        for (int i = 0; i < m_rec.length(); i++) {
            if (m_rec[i] == QChar(0x160)) {
                pronadjen = 1;
                m_brojSlova++;
                QTextEdit* textEdit = qobject_cast<QTextEdit*>(m_ui->horizontalLayout->itemAt(i)->widget());
                textEdit->setPlainText("Š");
                // Postavljanje većeg fonta
                QFont font = textEdit->font();
                font.setPointSize(36); // Promenite veličinu fonta prema potrebi

                textEdit->setFont(font);

                // Centriranje teksta
                textEdit->setAlignment(Qt::AlignCenter);
            }
        }
        if (pronadjen == 0) {
            m_brojgreske++;
            update();
        }
    } else {
        QMessageBox::information(this, "Nema više slova", "Nemate više pravo na pogađanje slova! Probajte da pogodite celu reč.");
        m_ui->lineEdit->setVisible(true);
        m_ui->pbProveri->setVisible(true);
    }
    proveri2();

    m_ui->pb->setStyleSheet("background-color: #D2B48C;"); // Promenite boju po želji
    m_ui->pb->setEnabled(false);
}
void Vesalice::proveri2()
{
    if (m_brojSlova == m_rec.length()) {
        m_igraZavrsena = true;
        QMessageBox::information(this, "Pogodak!", "Bravo pogodili ste reč!!!");
        m_timer.stop();
        // ui->setVisible(false);

        QObjectList children = this->children();
        for (QObject* child : children) {
            if (QWidget* widget = qobject_cast<QWidget*>(child)) {
                widget->setVisible(false);
            }
        }

        if (this->layout() != nullptr) {
            QLayoutItem* item;
            while ((item = this->layout()->takeAt(0)) != nullptr) {
                qDebug() << "brisem" << item->widget()->objectName() << item->widget()->parent()->objectName();

                delete item->widget();
                delete item;
            }
            delete this->layout();
        }

        this->prikaziEdukativniTekst();

        // zavrsiChallenge();
    }
}

void Vesalice::proveri()
{

    if (m_ui->lineEdit->text().compare("JUGOSLAVIJA", Qt::CaseInsensitive) == 0) {
        m_igraZavrsena = true;
        QMessageBox::information(this, "Pogodak!", "Bravo pogodili ste reč!!!");
        m_timer.stop();
        foreach (QObject* child, this->children()) {
            if (child->isWidgetType()) {
                QWidget* childWidget = qobject_cast<QWidget*>(child);
                if (childWidget) {
                    qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                    childWidget->hide();
                }
            }
        }

        if (this->layout() != nullptr) {
            QLayoutItem* item;
            while ((item = this->layout()->takeAt(0)) != nullptr) {
                qDebug() << "brisem" << item->widget()->objectName() << item->widget()->parent()->objectName();

                item->widget()->setVisible(false);
                delete item;
            }
            delete this->layout();
        }

        prikaziEdukativniTekst();

        // zavrsiChallenge();

    } else {
        QMessageBox::information(this, "Greska", "Probajte opet!");
    }
}

void Vesalice::drawHangman(QPainter& painter, int brojgreske)
{
    if (m_igraZavrsena) {
        return;
    }
    switch (brojgreske) {
    case 1:
        drawHead(painter);
        break;
    case 2:
        drawHead(painter);
        drawBody(painter);
        break;
    case 3:
        drawHead(painter);
        drawBody(painter);
        drawLeftArm(painter);
        break;
    case 4:
        drawHead(painter);
        drawBody(painter);
        drawLeftArm(painter);
        drawRightArm(painter);
        break;
    case 5:
        drawHead(painter);
        drawBody(painter);
        drawLeftArm(painter);
        drawRightArm(painter);
        drawLeftLeg(painter);
        break;
    case 6:
        drawHead(painter);
        drawBody(painter);
        drawLeftArm(painter);
        drawRightArm(painter);
        drawLeftLeg(painter);
        drawRightLeg(painter);
        break;
    case 7:
        drawHead(painter);
        drawBody(painter);
        drawLeftArm(painter);
        drawRightArm(painter);
        drawLeftLeg(painter);
        drawRightLeg(painter);
        drawLeftEye(painter);
        break;
    case 8:
        drawHead(painter);
        drawBody(painter);
        drawLeftArm(painter);
        drawRightArm(painter);
        drawLeftLeg(painter);
        drawRightLeg(painter);
        drawLeftEye(painter);
        drawRightEye(painter);
        break;
    case 9:
        drawHead(painter);
        drawBody(painter);
        drawLeftArm(painter);
        drawRightArm(painter);
        drawLeftLeg(painter);
        drawRightLeg(painter);
        drawLeftEye(painter);
        drawRightEye(painter);
        drawMouth(painter);
        break;
    case 10:
        drawHead(painter);
        drawBody(painter);
        drawLeftArm(painter);
        drawRightArm(painter);
        drawLeftLeg(painter);
        drawRightLeg(painter);
        drawLeftEye(painter);
        drawRightEye(painter);
        drawMouth(painter);
        drawTear(painter, 120, 85);
        break;
    case 11:
        drawHead(painter);
        drawBody(painter);
        drawLeftArm(painter);
        drawRightArm(painter);
        drawLeftLeg(painter);
        drawRightLeg(painter);
        drawLeftEye(painter);
        drawRightEye(painter);
        drawMouth(painter);
        drawTear(painter, 120, 85);
        drawHair(painter);
        break;
    case 12:
        drawHead(painter);
        drawBody(painter);
        drawLeftArm(painter);
        drawRightArm(painter);
        drawLeftLeg(painter);
        drawRightLeg(painter);
        drawLeftEye(painter);
        drawRightEye(painter);
        drawMouth(painter);
        drawTear(painter, 120, 85);
        drawHair(painter);
        break;
    default:

        break;
    }
}

void Vesalice::tekstovi()
{

    m_edukativniTekst = R"(
Smešten u raskošnom parku sa kaskadnim kompleksom staza, stepeništa i platoa, na mestu sa koga se na Beograd pruža prelepa panorama, Muzej Jugoslavije govori o periodu i vremenu koga se mnogi sa setom prisećaju i koje retki žele ...

Muzej istorije Jugoslavije, nastao je spajanjem Memorijalnog centra “Josip Broz Tito” i Muzeja Revolucije 1996. cini tri zgrade: Kuca cveca, Stari muzej i Muzej 25. Maj.

Kuca cveca, sagradjena 1975. predstavlja mesto gde su sahranjeni Joisp Broz Tito i njegova supruga Jovanka. Ova zgrada je bila Titova zimsak basta i sada je spomen soba koju posetioci mogu da obidju. Tokom Titovog zivota, Kuca cveca bila je njegovo omiljeno mesto, a nakon smrti je preuredjen za njegov vecni pocinak, sahranjen je 8. maja 1980.
Samo jezgro Muzeja Jugoslavije proizilazi iz Muzeja 25. maj, osnovanog 1962. godine, kada su količina, materijalna i emotivna vrednost poklona koje je Josip Broz Tito dobijao, kako od svojih sunarodnika, tako i od mnogih svetskih državnika, zahtevali poseban prostor za smeštaj i kategorizaciju.

Neki od eksponata u riznici poklona su materijalno vredni, a neki su zanimljivi samo zbog darodavca. Među njima su opšte poznati fragment kamena sa Meseca, ali i manje poznati pokloni kao što je maketa za spomenik Titovom konju.

U kompleksu Muzeja Jugoslavije, pored posete Titovom grobu, uvek se mogu videti i tri stalne postavke: Štafete u Kući cveća, Spomen-soba u Kući cveća i stalna postavka u Starom muzeju. Štafeta mladosti je palica koja je išla kroz celu SFR Jugoslaviju i svakog 25. maja dodeljivana je jugoslovenskom predsedniku Josipu Brozu Titu. Taj datum proglašen je za njegov službeni datum rođenja, iako je Tito bio rođen 7. maja. Takođe, ovaj praznik se zvao „Dan mladosti“.
Ovaj novi praznik proslavljao se na stadionu JNA i podrazumevao je slet u obliku masovnih scenskih performanasa i ceremonijalnu predaju štafete Titu. Umesto više glavnih štafeta, Tito sada prima samo jednu, saveznu štafetu, koju od ovog perioda izrađuju jugoslovenski umetnici, i koja postaje simbol svih ostalih, lokalnih štafeta.
)";

    m_asocijacija = "Я тебя люблю";

    m_resenjeAsocijacije.append("hotel moskva");
    m_resenjeAsocijacije.append("moskva");

    m_hint = "Jeste trenutno najezda Rusa u Beogradu, ali ova zelena građevina "
             "je ovde već jako dugo";
}

QString Vesalice::getRec() { return m_rec; }
void Vesalice::setRec(QString nova) { m_rec = nova; }
int Vesalice::getBrojGreske() { return m_brojgreske; }

int Vesalice::getBrojSlova() { return m_brojSlova; }
