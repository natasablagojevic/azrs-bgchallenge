// zeleznickastanica.cpp

#include "../include/zeleznickastanica.h"
#include <QDebug>
#include <QPainter>

#include <QBrush>
#include <QDialog>
#include <QFile>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPalette>
#include <QPixmap>
#include <QPushButton>
#include <QString>
#include <QUiLoader>

ZeleznickaStanica::ZeleznickaStanica(Timer* vreme, int pocetnoVreme, QWidget* parent)
    : ChallengeTest(5, 2, vreme, pocetnoVreme, parent)
{

    this->setWindowTitle("BgChallenge");
    this->setMinimumWidth(1280);
    this->setMinimumHeight(720);
    this->setFixedSize(this->width(), this->height());

    this->setImagePath(":/new/pozadine/resources/vozovi(1).png");
    this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

    QFile file(":/new/resources/forms/zeleznicka.ui");

    if (!file.open(QFile::ReadOnly)) {
        qDebug() << "Error opening UI file zeleznicka.ui:" << file.errorString();
        return;
    }

    QUiLoader loader;
    m_ui = loader.load(&file, this);
    m_ui->setFixedSize(this->width(), this->height());
    file.close();

    QLCDNumber* lcdNumber = m_ui->findChild<QLCDNumber*>("lcdNumber");

    connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
        m_pocetnoVreme++;
        QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
        lcdNumber->display(time.toString("h:mm:ss"));
        update();
    });

    connect(m_ui->findChild<QPushButton*>("pbNazad"), &QPushButton::clicked, this, &ZeleznickaStanica::zatvori);
    connect(m_ui->findChild<QPushButton*>("pbPomoc"), &QPushButton::clicked, this, &ZeleznickaStanica::pomoc);

    tekstovi();

    // Deklaracija dugmadi
    QPushButton* btnRomantika = m_ui->findChild<QPushButton*>("btnRomantika");
    QPushButton* btnCrveni = m_ui->findChild<QPushButton*>("btnCrveni");
    QPushButton* btnNostalgija = m_ui->findChild<QPushButton*>("btnNostalgija");
    QPushButton* btnPlavi = m_ui->findChild<QPushButton*>("btnPlavi");

    QPushButton* btnProveri = m_ui->findChild<QPushButton*>("btnProveri");

    QPushButton* levoLjubljana = m_ui->findChild<QPushButton*>("levoLjubljana");
    QPushButton* desnoLjubljana = m_ui->findChild<QPushButton*>("desnoLjubljana");

    QPushButton* levoZagreb = m_ui->findChild<QPushButton*>("levoZagreb");
    QPushButton* desnoZagreb = m_ui->findChild<QPushButton*>("desnoZagreb");

    QPushButton* levoJugoslavija = m_ui->findChild<QPushButton*>("levoJugoslavija");
    QPushButton* desnoJugoslavija = m_ui->findChild<QPushButton*>("desnoJugoslavija");

    QPushButton* levoMokraGora = m_ui->findChild<QPushButton*>("levoMokraGora");
    QPushButton* desnoMokraGora = m_ui->findChild<QPushButton*>("desnoMokraGora");

    connect(levoLjubljana, &QPushButton::clicked, this, &ZeleznickaStanica::levoLjubljanaClicked);
    connect(desnoLjubljana, &QPushButton::clicked, this, &ZeleznickaStanica::desnoLjubljanaClicked);

    connect(levoMokraGora, &QPushButton::clicked, this, &ZeleznickaStanica::levoMokraGoraClicked);
    connect(desnoMokraGora, &QPushButton::clicked, this, &ZeleznickaStanica::desnoMokraGoraClicked);

    connect(levoJugoslavija, &QPushButton::clicked, this, &ZeleznickaStanica::levoTitoClicked);
    connect(desnoJugoslavija, &QPushButton::clicked, this, &ZeleznickaStanica::desnoTitoClicked);

    connect(levoZagreb, &QPushButton::clicked, this, &ZeleznickaStanica::levoZagrebClicked);
    connect(desnoZagreb, &QPushButton::clicked, this, &ZeleznickaStanica::desnoZagrebClicked);

    connect(btnProveri, &QPushButton::clicked, this, &ZeleznickaStanica::btnProveriClicked);

    btnRomantika->setEnabled(false);
    btnCrveni->setEnabled(false);
    btnNostalgija->setEnabled(false);
    btnPlavi->setEnabled(false);

    m_odgovori = QList<QString>();
    m_odgovori.push_back("Cela Jugoslavija");
    m_odgovori.push_back("Ljubljana");
    m_odgovori.push_back("Mokra Gora");
    m_odgovori.push_back("Zagreb");

    m_tito = m_ui->findChild<QPushButton*>("tbJugoslavija");

    m_mokraGora = m_ui->findChild<QPushButton*>("tbMokraGora");

    m_zagreb = m_ui->findChild<QPushButton*>("tbZagreb");

    m_ljubljana = m_ui->findChild<QPushButton*>("tbLjubljana");


    m_tito->setEnabled(false);
    m_mokraGora->setEnabled(false);
    m_zagreb->setEnabled(false);
    m_ljubljana->setEnabled(false);

    m_brojacTito = new int(-1);
    m_brojacLjubljana = new int(-1);
    m_brojacMokraGora = new int(-1);
    m_brojacZagreb = new int(-1);
}

void ZeleznickaStanica::pomoc()
{

    QDialog dijalog;
    dijalog.setFixedHeight(300);
    dijalog.setFixedWidth(400);
    dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
                          "rgb(200,200,200); margin: 10px;}");

    QLabel* text = new QLabel("Srce koje je nekada povezivalo krajeve bivše Jugoslavije bila je "
                              "Železnička stanica, pre nego što se preselila na Prokop. Mesto koje "
                              "sada "
                              "služi za filmske setove zbog svog enterijera poput vremenske kapsule, "
                              "nekada je bilo žarište putovanja. Tada su postojali ikonski vozovi čije "
                              "je ime ostalo samo u sećanju. Da li možete da pretpostavite koji voz je "
                              "išao kuda? Znate li koji voz je bio čuveni Titov voz koji ga je "
                              "razvozio "
                              "gde god je trebalo poći?",
        &dijalog);
    text->setAlignment(Qt::AlignCenter);
    text->setWordWrap(true);
    text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
    QPushButton* ok = new QPushButton("OK", &dijalog);
    ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
                      "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
                      "padding: 10px; border-radius: 21px; font: bold 14px;}"
                      "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
                      "#bcbcbc; }"
                      "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
                      "#a0a0a0; }");
    QVBoxLayout* layout = new QVBoxLayout(&dijalog);

    layout->addWidget(text);

    layout->addWidget(ok);
    connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
    dijalog.exec();
}

void ZeleznickaStanica::levoLjubljanaClicked()
{
    // ljubljana->setStyleSheet("");

    int pomeriLevo = *m_brojacLjubljana - 1;

    if (pomeriLevo < 0) {
        pomeriLevo = (pomeriLevo + 4) % 4;
    }

    m_ljubljana->setText(m_odgovori[pomeriLevo]);

    *m_brojacLjubljana = pomeriLevo;
}

void ZeleznickaStanica::desnoLjubljanaClicked()
{
    // ljubljana->setStyleSheet("");
    int pomeriDesno = *m_brojacLjubljana + 1;

    if (pomeriDesno > 3) {
        pomeriDesno = (pomeriDesno + 4) % 4;
    }

    m_ljubljana->setText(m_odgovori[pomeriDesno]);

    *m_brojacLjubljana = pomeriDesno;
}

void ZeleznickaStanica::levoMokraGoraClicked()
{
    // mokraGora->setStyleSheet("");
    int pomeriLevo = *m_brojacMokraGora - 1;

    if (pomeriLevo < 0) {
        pomeriLevo = (pomeriLevo + 4) % 4;
    }

    m_mokraGora->setText(m_odgovori[pomeriLevo]);

    *m_brojacMokraGora = pomeriLevo;
}

void ZeleznickaStanica::desnoMokraGoraClicked()
{
    // mokraGora->setStyleSheet("");
    int pomeriDesno = *m_brojacMokraGora + 1;

    if (pomeriDesno > 3) {
        pomeriDesno = (pomeriDesno + 4) % 4;
    }

    m_mokraGora->setText(m_odgovori[pomeriDesno]);

    *m_brojacMokraGora = pomeriDesno;
}

void ZeleznickaStanica::levoTitoClicked()
{
    // tito->setStyleSheet("");
    int pomeriLevo = *m_brojacTito - 1;

    if (pomeriLevo < 0) {
        pomeriLevo = (pomeriLevo + 4) % 4;
    }

    m_tito->setText(m_odgovori[pomeriLevo]);

    *m_brojacTito = pomeriLevo;
}

void ZeleznickaStanica::desnoTitoClicked()
{
    // tito->setStyleSheet("");
    int pomeriDesno = *m_brojacTito + 1;

    if (pomeriDesno > 3) {
        pomeriDesno = (pomeriDesno + 4) % 4;
    }

    m_tito->setText(m_odgovori[pomeriDesno]);

    *m_brojacTito = pomeriDesno;
}

void ZeleznickaStanica::levoZagrebClicked()
{
    // zagreb->setStyleSheet("");
    int pomeriLevo = *m_brojacZagreb - 1;

    if (pomeriLevo < 0) {
        pomeriLevo = (pomeriLevo + 4) % 4;
    }

    m_zagreb->setText(m_odgovori[pomeriLevo]);

    *m_brojacZagreb = pomeriLevo;
}

void ZeleznickaStanica::desnoZagrebClicked()
{
    // zagreb->setStyleSheet("");

    int pomeriDesno = *m_brojacZagreb + 1;

    if (pomeriDesno > 3) {
        pomeriDesno = (pomeriDesno + 4) % 4;
    }

    m_zagreb->setText(m_odgovori[pomeriDesno]);

    *m_brojacZagreb = pomeriDesno;
}

void ZeleznickaStanica::btnProveriClicked()
{

    if (m_ljubljana->text().compare("Ljubljana") == 0) {
        m_ljubljana->setStyleSheet("QPushButton { background-color: rgba(46,194,126,80%); border: 1px "
                                 "solid "
                                 "#ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
    } else {
        m_ljubljana->setStyleSheet("QPushButton { background-color: rgba(237,51,59,80%); border: 1px "
                                 "solid "
                                 "#ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
    }

    if (m_zagreb->text().compare("Zagreb") == 0) {
        m_zagreb->setStyleSheet("QPushButton { background-color: rgba(46,194,126,80%); border: 1px "
                              "solid "
                              "#ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
    } else {
        m_zagreb->setStyleSheet("QPushButton { background-color: rgba(237,51,59,80%); border: 1px "
                              "solid "
                              "#ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
    }

    if (m_mokraGora->text().compare("Mokra Gora") == 0) {
        m_mokraGora->setStyleSheet("QPushButton { background-color: rgba(46,194,126,80%); border: 1px "
                                 "solid "
                                 "#ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
    } else {
        m_mokraGora->setStyleSheet("QPushButton { background-color: rgba(237,51,59,80%); border: 1px "
                                 "solid "
                                 "#ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
    }

    if (m_tito->text().compare("Cela Jugoslavija") == 0) {
        m_tito->setStyleSheet("QPushButton { background-color: rgba(46,194,126,80%); border: 1px "
                            "solid "
                            "#ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
    } else {
        m_tito->setStyleSheet("QPushButton { background-color: rgba(237,51,59,80%); border: 1px "
                            "solid "
                            "#ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
    }

    if ((m_ljubljana->text().compare("Ljubljana") == 0) && (m_zagreb->text().compare("Zagreb") == 0) && (m_mokraGora->text().compare("Mokra Gora") == 0) && (m_tito->text().compare("Cela Jugoslavija") == 0)) {
        foreach (QObject* child, m_ui->children()) {
            if (child->isWidgetType()) {
                QWidget* childWidget = qobject_cast<QWidget*>(child);
                if (childWidget) {
                    qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                    childWidget->hide();
                }
            }
        }

        if (this->layout() != nullptr) {
            QLayoutItem* item;
            while ((item = this->layout()->takeAt(0)) != nullptr) {
                qDebug() << "brisem" << item->widget()->objectName() << item->widget()->parent()->objectName();

                item->widget()->setVisible(false);
                delete item;
            }
            delete this->layout();
        }

        QLayout* postojeciLayout = this->layout();
        if (postojeciLayout) {
            delete postojeciLayout;
        }

        this->prikaziEdukativniTekst();
    }
}

ZeleznickaStanica::~ZeleznickaStanica()
{
    delete m_brojacTito;
    delete m_brojacMokraGora;
    delete m_brojacZagreb;
    delete m_brojacLjubljana;
    delete m_ui;
}

void ZeleznickaStanica::tekstovi()
{

    m_edukativniTekst = R"(
Zgrada je izdradjena po uzoru na zeleznicke stanice velikih evropskih zemalja i stoji kao monumetalno zdanje. Trebalo je odrediti mesto novoj stanici.
Francuzi su predlagali Prokop. Srpski članovi komisije su taj predlog odbacili jer bi onda stanica bila suviše udaljena od grada. Oni su hteli da to važno i veliko zdanje bude urbanistički svetionik (francuski predlog će biti usvojen skoro vek kasnije). Ideja da železnička stanica bude ukras gradu obilato je prihvaćena u javnosti.
Ulaz stanice odrediće pravac Nemanjinog bulevara: u vreme kad je stanica građena ne samo što tu nije bilo ulice, nego ni ledine. Bara Venecija je bio šaljivi naziv za močvaru koja se kilometrima pružala uz Savu, pa sve uz breg do Topčiderskog puta (ulice Kneza Miloša).

1941. iz nemačkih aviona u ponirućem letu i uz pištanje sirena na stanicu su izbačeni tovari bombi. Taman je obnovljena, da bi u aprilu 1944. na nju, sa 5000m visine, poletele polutonske bombe iz američkih i engleskih bombardera.

U Titovoj Jugoslaviji stanica je zguljeno obnovljena. Kule na peronskom ulazu nisu ponovo podignute, pa je narušen opšti utisak stanice. Međutim, danas možemo zateći, po nekim mišljenjima grotesknu, statuu Stefana Nemanje.

Sredinom 2018. železnička stanica je zatvorena i napuštena. Postala je žrtva svoje skupe lokacije namenjene izgradnji kvarta solitera pored Save. Vozovi su užurbano izmešteni u nedovršenu stanicu Prokop.

Čast da poslednji pođe sa Glavne stanice pripala je međunarodnom vozu koji je krenuo 30. juna u 21.40h. Kao da mu je određena simbolična putanja, išao je za Budimpeštu, kuda i prvi, koji je sa iste stanice krenuo pre 134 godine. Prvi voz za Evropu je ispraćen marševima Orkestra Kraljevske garde, a poslednji je ispraćen pesmom A sad adio.
Od kultnih vozova, koji se danas mogu posetiti kao muzeji, tu su Romantika (čija je poslednja destinacija bila Ljubljana), Nostalgija (redovno prevozila do Mokre Gore), Crveni voz koji je išao do Zagreba, i najpoznatiji, Titov Plavi voz, koji ga je vozio gde god je trebalo da se krene, širom Jugoslavije i Evrope. )";

    m_asocijacija = "100 dinara";

    m_hint = "Ako ne znaš ko se nalazi na novčanici od 100 dinara, otvori "
             "novčanik. Ukoliko imaš išta keša, studentska sirotinjo. I imaj na "
             "umu da tražimo lokaciju - zgradu - od tebe.";

    m_resenjeAsocijacije.append("muzej nikole tesle");

    m_naslovEdukativnogTeksta = "O Glavnoj železničkoj stanici";
}
