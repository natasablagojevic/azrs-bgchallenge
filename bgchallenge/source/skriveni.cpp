#include "../include/skriveni.h"

#include <QDebug>
#include <QPainter>

#include <QBrush>
#include <QDialog>
#include <QFile>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPalette>
#include <QPixmap>
#include <QPropertyAnimation>
#include <QPushButton>
#include <QString>
#include <QTextBrowser>
#include <QUiLoader>

Skriveni::Skriveni(Timer* vreme, int pocetnoVreme, QWidget* parent)
    : ChallengeTest(10, 3, vreme, pocetnoVreme, parent)
{

    this->setWindowTitle("BgChallenge");
    this->setMinimumWidth(1280);
    this->setMinimumHeight(720);
    this->setFixedSize(this->width(), this->height());

    this->setImagePath(":/new/pozadine/resources/skupstina.png");
    this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

    QFile file(":/new/resources/forms/skriveni2.ui");

    if (!file.open(QFile::ReadOnly)) {
        qDebug() << "Error opening UI file Skriveni.ui:" << file.errorString();
        return;
    }

    QUiLoader loader;
    m_ui = loader.load(&file, this);
    m_ui->setFixedSize(this->width(), this->height());
    file.close();

    QLCDNumber* lcdNumber = m_ui->findChild<QLCDNumber*>("lcdNumber");

    connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
        m_pocetnoVreme++;
        QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
        lcdNumber->display(time.toString("h:mm:ss"));
        update();
    });

    connect(m_ui->findChild<QPushButton*>("pbNazad"), &QPushButton::clicked, this, &Skriveni::zatvori);
    connect(m_ui->findChild<QPushButton*>("pbPomoc"), &QPushButton::clicked, this, &Skriveni::pomoc);

    tekstovi();

    // deklaracija dugmadi
    m_tbOpis = m_ui->findChild<QTextBrowser*>("tbOpis");
    qDebug() << m_tbOpis->objectName() << m_tbOpis->parent()->objectName();
    m_btnNasaoMegafon = m_ui->findChild<QPushButton*>("btnNasaoMegafon");
    m_btnNasaoMetu = m_ui->findChild<QPushButton*>("btnNasaoMetu");
    m_btnNasaoSuzavac = m_ui->findChild<QPushButton*>("btnNasaoSuzavac");
    m_btnNasaoTransparent = m_ui->findChild<QPushButton*>("btnNasaoTransparent");
    m_btnNasaoVucica = m_ui->findChild<QPushButton*>("btnNasaoVucica");
    m_btnNasaoZastavu = m_ui->findChild<QPushButton*>("btnNasaoZastavu");

    m_btnMegafon = m_ui->findChild<QPushButton*>("btnMegafon");
    m_btnMeta = m_ui->findChild<QPushButton*>("btnMeta");
    m_btnSuzavac = m_ui->findChild<QPushButton*>("btnSuzavac");
    m_btnTransparent = m_ui->findChild<QPushButton*>("btnTransparent");
    m_btnVucic = m_ui->findChild<QPushButton*>("btnVucic");
    m_btnFerari = m_ui->findChild<QPushButton*>("btnFerari");

    m_tbOpis->setVisible(false);

    QList<QPushButton*> nadjeniObjekti;
    nadjeniObjekti.append(m_btnNasaoMegafon);
    nadjeniObjekti.append(m_btnNasaoMetu);
    nadjeniObjekti.append(m_btnNasaoSuzavac);
    nadjeniObjekti.append(m_btnNasaoTransparent);
    nadjeniObjekti.append(m_btnNasaoVucica);
    nadjeniObjekti.append(m_btnNasaoZastavu);

    // for (QPushButton* btn : nadjeniObjekti) {

    //     btn->setStyleSheet("QPushButton { border: 3px solid black;
    //     border-radius: 10px; background-color: rgba(255, 255, 255, 0.4); }");
    // }

    QList<QPushButton*> skriveniObjekti;
    skriveniObjekti.append(m_btnMegafon);
    skriveniObjekti.append(m_btnMeta);
    skriveniObjekti.append(m_btnSuzavac);
    skriveniObjekti.append(m_btnTransparent);
    skriveniObjekti.append(m_btnVucic);
    skriveniObjekti.append(m_btnFerari);

    connect(m_btnNasaoMegafon, &QPushButton::clicked, this, &Skriveni::btnNasaoMegafonClicked);
    connect(m_btnNasaoMetu, &QPushButton::clicked, this, &Skriveni::btnNasaoMetuClicked);
    connect(m_btnNasaoSuzavac, &QPushButton::clicked, this, &Skriveni::btnNasaoSuzavacClicked);
    connect(m_btnNasaoTransparent, &QPushButton::clicked, this, &Skriveni::btnNasaoTransparentClicked);
    connect(m_btnNasaoVucica, &QPushButton::clicked, this, &Skriveni::btnNasaoVucicaClicked);
    connect(m_btnNasaoZastavu, &QPushButton::clicked, this, &Skriveni::btnNasaoZastavuClicked);

    connect(m_btnMegafon, &QPushButton::clicked, this, &Skriveni::btnMegafonClicked);
    connect(m_btnMeta, &QPushButton::clicked, this, &Skriveni::btnMetaClicked);
    connect(m_btnSuzavac, &QPushButton::clicked, this, &Skriveni::btnSuzavacClicked);
    connect(m_btnTransparent, &QPushButton::clicked, this, &Skriveni::btnTransparentClicked);
    connect(m_btnVucic, &QPushButton::clicked, this, &Skriveni::btnVucicClicked);
    connect(m_btnFerari, &QPushButton::clicked, this, &Skriveni::btnFerariClicked);

    m_brojac = new int(0);
}

Skriveni::~Skriveni()
{
    delete m_brojac;
    delete m_ui;
}

void Skriveni::tekstovi()
{

    m_edukativniTekst = R"(
Prvo je bio put. Rimljani su u prvom veku naše ere trasom današnjeg Bulevara kralja Aleksandra utvrdili glatke kamene ploče u zemljanu podlogu. Carski put je vodio za Atinu.
Po običaju o sahranjivanju, groblje je raslo uz drum kojim se ulazi u grad. Kada je Osmanlijsko carstvo zauzelo Beograd, na mestu rimskog groblja izgradili su Ejnehan-begovu džamiju.
Ni čitavu deceniju kasnije, na brdašcetu pored rimskog druma spaljene su mošti Svetog Save. Plamen sa lomače srpskog svetitelja obasjavao je polumesec na vrhu džamije. Pod njenim minaretom se s vremenom raširilo tursko groblje.
U nekolikim ratovima nalazila se prva na udaru naletnika. Često rušena i sporo obnavljana, prozvana je Batal-džamija - batal na turskom znači napuštena.

Nakon odlaska Turaka Osmanlija, sravnjena je sa zemljom a rusenje su za 230 dukata obavili Cincari.

Prvi projekat skupstine izradio je Konstantin Jovanovic 1891. Ali, zbog politickih dogadjaja i ekonomskih uslova gradnja je odlozena za nekoliko godina i poverena je arhitekti Jovanu Ilkicu.
Istorija je pokazala da je lakše sagraditi palatu Skupštine, nego karakter poslanika. No, da nije sve tako crno, potvrđuje i priča o Stevanu Ćiriću. On je bio novosadski političar, svojevremeno predsednik Skupštine.
I on je smatran zaslužnim za dvorištenje zgrade Skupštine. Da on nije pritiskao da se gradnja ubrza, ko zna šta bi bilo sa sedištem demokratije. Od izrade plana 1892. do useljenja 1936. protekle su 44 godine!
Za to vreme Skupština je zasedala u pivnici u Admirala Geprata, Starom dvoru, u pozorištu u Manješkom parku. Kralj Aleksandar nije mario za Skupštinu, pa ni knez namesnik. Skele bi možda dočekale Drugi svetski rat, a time bi zadugo bilo odloženo dovršenje. Bila bi to batal-skupština.

Zvanican pocetak gradnje palate oznacen je polaganjem kamena temeljca 27. avgusta 1907. u prisustvu kralja Petra I Karadjordjevica i prestolonaslednika Dordja, narodnih poslanika i diplomatskog kora.)";
    m_asocijacija = "Ima li negde u blizini da se zapali sveća?";
    m_hint = "Ako ne znaš ovo, prekrsti se";
    m_resenjeAsocijacije.append("crkva svetog marka");
    m_resenjeAsocijacije.append("crkva sv marka");

    m_naslovEdukativnogTeksta = "O Narodnoj skupštini";
}

void Skriveni::pomoc()
{
    QDialog dijalog;
    dijalog.setFixedHeight(300);
    dijalog.setFixedWidth(400);
    dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
                          "rgb(200,200,200); margin: 10px;}");

    QLabel* text = new QLabel("Skupština je oduvek bilo mesto velikih dešavanja, a naročito velikih -i "
                              "malih- pokušaja da se uradi nešto povodom političkog stanja u našoj "
                              "državi. Svaki protest, bilo da se održavao 1996. ili 2023. godine, imao "
                              "je nešto karakteristično, nešto što opisuje Srbe kao narod, da li to "
                              "bila "
                              "naša tvrdoglavost, inat, ili možda ludost. Na slici pred tobom, "
                              "sakrivena "
                              "su 6 simbola protesta u prethodnih dvadesetak godina. Možeš li sve da "
                              "ih "
                              "pronađeš?",
        &dijalog);
    text->setAlignment(Qt::AlignCenter);
    text->setWordWrap(true);
    text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
    QPushButton* ok = new QPushButton("OK", &dijalog);
    ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
                      "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
                      "padding: 10px; border-radius: 21px; font: bold 14px;}"
                      "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
                      "#bcbcbc; }"
                      "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
                      "#a0a0a0; }");
    QVBoxLayout* layout = new QVBoxLayout(&dijalog);

    layout->addWidget(text);

    layout->addWidget(ok);
    connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
    dijalog.exec();
}

void Skriveni::btnVucicClicked()
{
    QPropertyAnimation* buttonAnimation = new QPropertyAnimation(m_btnVucic, "geometry", this);
    buttonAnimation->setDuration(200);
    buttonAnimation->setStartValue(m_btnVucic->geometry());
    buttonAnimation->setEndValue(m_btnVucic->geometry().adjusted(-8, -8, 8, 8));
    buttonAnimation->start();

    m_btnNasaoVucica->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
                                  "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
                                  "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
    disconnect(m_btnVucic, SIGNAL(clicked()), this, SLOT(btnVucic_clicked()));

    m_nasaoVucica = true;
    (*m_brojac) += 1;
    if (*m_brojac == 6) {
        QTimer::singleShot(1000, this, [=]() {
            foreach (QObject* child, m_ui->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            // if (this->layout() != nullptr) {
            //     QLayoutItem *item;
            //     while ((item = this->layout()->takeAt(0)) != nullptr) {
            //         qDebug() << "brisem" << item->widget()->objectName() <<
            //         item->widget()->parent()->objectName();

            //         item->widget()->setVisible(false);
            //         delete item;
            //     }
            //     delete this->layout();
            // }

            foreach (QObject* child, this->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            QLayout* postojeciLayout = this->layout();
            if (postojeciLayout) {
                delete postojeciLayout;
            }

            prikaziEdukativniTekst();
        });
    }
}

void Skriveni::btnMetaClicked()
{
    QPropertyAnimation* buttonAnimation = new QPropertyAnimation(m_btnMeta, "geometry", this);
    buttonAnimation->setDuration(200);
    buttonAnimation->setStartValue(m_btnMeta->geometry());
    buttonAnimation->setEndValue(m_btnMeta->geometry().adjusted(-8, -8, 8, 8));
    buttonAnimation->start();

    m_btnNasaoMetu->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
                                "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
                                "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
    disconnect(m_btnMeta, SIGNAL(clicked()), this, SLOT(btnMeta_clicked()));

    m_nasaoMetu = true;

    (*m_brojac) += 1;
    if (*m_brojac == 6) {
        QTimer::singleShot(1000, this, [=]() {
            foreach (QObject* child, m_ui->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            // if (this->layout() != nullptr) {
            //     QLayoutItem *item;
            //     while ((item = this->layout()->takeAt(0)) != nullptr) {
            //         qDebug() << "brisem" << item->widget()->objectName() <<
            //         item->widget()->parent()->objectName();

            //         item->widget()->setVisible(false);
            //         delete item;
            //     }
            //     delete this->layout();
            // }

            foreach (QObject* child, this->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            QLayout* postojeciLayout = this->layout();
            if (postojeciLayout) {
                delete postojeciLayout;
            }
            prikaziEdukativniTekst();
        });
    }
}

void Skriveni::btnMegafonClicked()
{
    QPropertyAnimation* buttonAnimation = new QPropertyAnimation(m_btnMegafon, "geometry", this);
    buttonAnimation->setDuration(200);
    buttonAnimation->setStartValue(m_btnMegafon->geometry());
    buttonAnimation->setEndValue(m_btnMegafon->geometry().adjusted(-8, -8, 8, 8));
    buttonAnimation->start();

    m_btnNasaoMegafon->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
                                   "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
                                   "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
    disconnect(m_btnMegafon, SIGNAL(clicked()), this, SLOT(btnMegafon_clicked()));

    m_nasaoMegafon = true;

    (*m_brojac) += 1;
    if (*m_brojac == 6) {
        QTimer::singleShot(1000, this, [=]() {
            foreach (QObject* child, m_ui->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            // if (this->layout() != nullptr) {
            //     QLayoutItem *item;
            //     while ((item = this->layout()->takeAt(0)) != nullptr) {
            //         qDebug() << "brisem" << item->widget()->objectName() <<
            //         item->widget()->parent()->objectName();

            //         item->widget()->setVisible(false);
            //         delete item;
            //     }
            //     delete this->layout();
            // }

            foreach (QObject* child, this->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            QLayout* postojeciLayout = this->layout();
            if (postojeciLayout) {
                delete postojeciLayout;
            }
            prikaziEdukativniTekst();
        });
    }
}

void Skriveni::btnTransparentClicked()
{
    QPropertyAnimation* buttonAnimation = new QPropertyAnimation(m_btnTransparent, "geometry", this);
    buttonAnimation->setDuration(200);
    buttonAnimation->setStartValue(m_btnTransparent->geometry());
    buttonAnimation->setEndValue(m_btnTransparent->geometry().adjusted(-8, -8, 8, 8));
    buttonAnimation->start();

    m_btnNasaoTransparent->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
                                       "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
                                       "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
    disconnect(m_btnTransparent, SIGNAL(clicked()), this, SLOT(btnTransparent_clicked()));

    m_nasaoTransparent = true;

    (*m_brojac) += 1;
    if (*m_brojac == 6) {
        QTimer::singleShot(1000, this, [=]() {
            foreach (QObject* child, m_ui->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            // if (this->layout() != nullptr) {
            //     QLayoutItem *item;
            //     while ((item = this->layout()->takeAt(0)) != nullptr) {
            //         qDebug() << "brisem" << item->widget()->objectName() <<
            //         item->widget()->parent()->objectName();

            //         item->widget()->setVisible(false);
            //         delete item;
            //     }
            //     delete this->layout();
            // }

            foreach (QObject* child, this->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            QLayout* postojeciLayout = this->layout();
            if (postojeciLayout) {
                delete postojeciLayout;
            }
            prikaziEdukativniTekst();
        });
    }
}

void Skriveni::btnSuzavacClicked()
{
    QPropertyAnimation* buttonAnimation = new QPropertyAnimation(m_btnSuzavac, "geometry", this);
    buttonAnimation->setDuration(200);
    buttonAnimation->setStartValue(m_btnSuzavac->geometry());
    buttonAnimation->setEndValue(m_btnSuzavac->geometry().adjusted(-8, -8, 8, 8));
    buttonAnimation->start();

    m_btnNasaoSuzavac->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
                                   "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
                                   "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
    disconnect(m_btnSuzavac, SIGNAL(clicked()), this, SLOT(btnSuzavac_clicked()));

    m_nasaoSuzavac = true;

    (*m_brojac) += 1;
    if (*m_brojac == 6) {
        QTimer::singleShot(1000, this, [=]() {
            foreach (QObject* child, m_ui->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            // if (this->layout() != nullptr) {
            //     QLayoutItem *item;
            //     while ((item = this->layout()->takeAt(0)) != nullptr) {
            //         qDebug() << "brisem" << item->widget()->objectName() <<
            //         item->widget()->parent()->objectName();

            //         item->widget()->setVisible(false);
            //         delete item;
            //     }
            //     delete this->layout();
            // }

            foreach (QObject* child, this->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            QLayout* postojeciLayout = this->layout();
            if (postojeciLayout) {
                delete postojeciLayout;
            }
            prikaziEdukativniTekst();
        });
    }
}

void Skriveni::btnFerariClicked()
{
    QPropertyAnimation* buttonAnimation = new QPropertyAnimation(m_btnFerari, "geometry", this);
    buttonAnimation->setDuration(200);
    buttonAnimation->setStartValue(m_btnFerari->geometry());
    buttonAnimation->setEndValue(m_btnFerari->geometry().adjusted(-8, -8, 8, 8));
    buttonAnimation->start();

    m_btnNasaoZastavu->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
                                   "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
                                   "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
    disconnect(m_btnFerari, SIGNAL(clicked()), this, SLOT(btnFerari_clicked()));

    m_nasaoZastavu = true;

    (*m_brojac) += 1;
    if (*m_brojac == 6) {
        QTimer::singleShot(1000, this, [=]() {
            foreach (QObject* child, m_ui->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            // if (this->layout() != nullptr) {
            //     QLayoutItem *item;
            //     while ((item = this->layout()->takeAt(0)) != nullptr) {
            //         qDebug() << "brisem" << item->widget()->objectName() <<
            //         item->widget()->parent()->objectName();

            //         item->widget()->setVisible(false);
            //         delete item;
            //     }
            //     delete this->layout();
            // }

            foreach (QObject* child, this->children()) {
                if (child->isWidgetType()) {
                    QWidget* childWidget = qobject_cast<QWidget*>(child);
                    if (childWidget) {
                        qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                        childWidget->hide();
                    }
                }
            }

            QLayout* postojeciLayout = this->layout();
            if (postojeciLayout) {
                delete postojeciLayout;
            }
            prikaziEdukativniTekst();
        });
    }
}

void Skriveni::btnNasaoVucicaClicked()
{

    if (m_nasaoVucica) {

        if (!m_tbOpis->isVisible()) {
            m_tbOpis->setVisible(true);
        }
        m_tbOpis->setText("'Vucicu, odlazi!' je simbol sa poslednje odrzanih protesta, Srbija "
                        "protiv Nasilja, kada nam je svima samo prekipilo.........");
    }
}

void Skriveni::btnNasaoMetuClicked()
{

    if (m_nasaoMetu) {

        if (!m_tbOpis->isVisible()) {
            m_tbOpis->setVisible(true);
        }
        m_tbOpis->setText("Tog bombardovanja, kada je prikazan onaj pravi srpski "
                        "karakter, kada su "
                        "ljudi izlazili na ulice noseci mete i pevali Klintonu..");
    }
}

void Skriveni::btnNasaoZastavuClicked()
{

    if (m_nasaoZastavu) {
        if (!m_tbOpis->isVisible()) {
            m_tbOpis->setVisible(true);
        }
        m_tbOpis->setText("A protesta 1996, zabranili su zastave koje simbolizuju bilo kakva "
                        "politicka obelezja. Ferari zastava nije politicko obelezje, zar ne?");
    }
}

void Skriveni::btnNasaoTransparentClicked()
{

    if (m_nasaoTransparent) {
        if (!m_tbOpis->isVisible()) {
            m_tbOpis->setVisible(true);
        }
        m_tbOpis->setText("Samo jedan transparent sa protesta Srbija protiv nasilja...");
    }
}

void Skriveni::btnNasaoSuzavacClicked()
{
    if (m_nasaoSuzavac) {
        if (!m_tbOpis->isVisible()) {
            m_tbOpis->setVisible(true);
        }
        m_tbOpis->setText("Protesti 2016. Da li se secate kako je policija suzavcima "
                        "i konjima rasterivala gradjane?");
    }
}

void Skriveni::btnNasaoMegafonClicked()
{
    if (m_nasaoMegafon) {
        if (!m_tbOpis->isVisible()) {
            m_tbOpis->setVisible(true);
        }
        m_tbOpis->setText("Kako protesti da prodju bez megafona? STUDENTI NE CUTE!");
    }
}
