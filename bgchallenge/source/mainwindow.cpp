#include "../include/mainwindow.h"
#include "../forms/ui_mainwindow.h"
#include "../include/challengeMorzeovaAzbukaUI.h"
#include "../include/igrac.h"
#include "../include/mapa.h"
#include "../include/rezultat.h"
#include <QDebug>
#include <QFile>
#include <QInputDialog>
#include <QLineEdit>
#include <QMessageBox>
#include <QPalette>
#include <QTabWidget>
#include <QTextStream>
#include <QTime>
#include <QUiLoader>
#include <QVector>

mainwindow::mainwindow(QWidget* parent)
    : QWidget(parent)
    , m_ui(new Ui::mainwindow)
{
    m_ui->setupUi(this);
    this->setWindowTitle("BgChallenge");
    this->setFixedSize(this->width(), this->height());
    this->setUpBackground();

    m_rez = new Rezultat();
    m_rez->ucitajIgraceJson(":/new/resources/resources/savefile.json");

    this->showLoadGame();
    // loadScores();
    this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));
    m_ui->stackedWidget->setCurrentIndex(0);

    qApp->installEventFilter(this);

    connect(m_ui->pbNapustiIgricu, SIGNAL(clicked()), this, SLOT(napustiIgricuClicked()));
    connect(m_ui->pbZapocniIgricu, SIGNAL(clicked()), this, SLOT(zapocniIgricuClicked()));
    connect(m_ui->pbNazad_2, SIGNAL(clicked()), this, SLOT(natragNaGlavniMeniClicked()));
    connect(m_ui->pbPregledRezultata, &QPushButton::clicked, this, &mainwindow::pregledRezultataClicked);
    connect(m_ui->pbNazadNaMeni, &QPushButton::clicked, this, &mainwindow::natragNaGlavniMeniClicked);
    connect(m_ui->pbNazad, &QPushButton::clicked, this, &mainwindow::natragNaGlavniMeniClicked);
    connect(m_ui->pbPodesavanja, &QPushButton::clicked, this, &mainwindow::podesavanjaClicked);
    setUpTabWidges();
    connect(m_ui->pbNastaviIgru, SIGNAL(clicked()), this, SLOT(nastaviIgricuClicked()));
    connect(m_ui->pbKakoSeIgra, &QPushButton::clicked, this, &mainwindow::prikaziUputstvo);

    connect(m_ui->pbKreni, &QPushButton::clicked, this, &mainwindow::kreni);

    QObject::connect(this, &mainwindow::clicked, this, &mainwindow::klikMelodija);
    connect(m_ui->RB_ukljucen, &QRadioButton::clicked, this, &mainwindow::klikMelodija);
    connect(m_ui->HS_jacina_tona, &QSlider::valueChanged, this, &mainwindow::jacinaPozadinseMelodije);

    m_player = new QMediaPlayer();
    m_audioOutput = new QAudioOutput();
    m_player->setAudioOutput(m_audioOutput);
    m_player->setSource(QUrl("qrc:/new/resources/resources/mixkit-trap-hamza-267.mp3"));
    m_audioOutput->setVolume(0);
    m_player->play();

}

mainwindow::~mainwindow()
{
    if (m_rez != nullptr) {
        delete m_rez;
        m_rez = nullptr;
    };

    if (m_mapa != nullptr) {
        qDebug() << "obrisana mapa";
        delete m_mapa;
        m_mapa = nullptr;
    };

    if (m_player != nullptr) {
        qDebug() << "obrisan player";
        delete m_player;
        m_player = nullptr;
    };

    if (m_audioOutput != nullptr) {
        qDebug() << "obrisan audioOutput";
        delete m_audioOutput;
        m_audioOutput = nullptr;
    };

    delete m_ui;
}

void mainwindow::zapocniIgricuClicked()
{
    if (m_mapa != nullptr) {
        qDebug() << "obrisana mapa";
        delete m_mapa;
        m_mapa = nullptr;
    }
    m_ui->stackedWidget->setCurrentIndex(1);
}

void mainwindow::kreni()
{
    QString ime = m_ui->lineEdit->text();
    if (ime == "" || postojiIgrac(ime)) {
        m_ui->lineEdit->clear();
        QMessageBox::warning(this, "Uneto ime postoji!", "Molimo unesite drugo ime!");
    } else {
        m_ui->lineEdit->clear();
        Igrac noviIgrac(ime);
        m_rez->dodajIgraca(noviIgrac);
        m_rez->sacuvajIgraceJson("../bgchallenge/resources/savefile.json");
        if (m_mapa != nullptr) {
            qDebug() << "obrisana mapa";
            delete m_mapa;
            m_mapa = nullptr;
        }
        m_mapa = new MapaUI(noviIgrac, m_rez, this);
        m_mapa->raise();
        m_mapa->show();
        connect(m_mapa, &MapaUI::cestitajIgracu, this, &mainwindow::cestitajIgracu);
        m_ui->stackedWidget->setCurrentIndex(0);
    }
}

bool mainwindow::postojiIgrac(const QString& ime) { return m_rez->postojiIgrac(ime); }

void mainwindow::nastaviIgricuClicked()
{
    bool ok;
    QString ime = QInputDialog::getText(this, "Nastavi igru", "Molimo unesite ime:", QLineEdit::Normal, QString(), &ok);

    if (ok && !ime.isEmpty()) {

        if (postojiIgrac(ime)) {
            Igrac stariIgrac = m_rez->nadjiIgraca(ime);
            qDebug() << stariIgrac.getIme() << stariIgrac.getVreme();
            if (m_mapa != nullptr) {
                qDebug() << "obrisana mapa";
                delete m_mapa;
                m_mapa = nullptr;
            };
            m_mapa = new MapaUI(stariIgrac, m_rez, this);
            m_mapa->raise();
            m_mapa->show();
            connect(m_mapa, &MapaUI::cestitajIgracu, this, &mainwindow::cestitajIgracu);
            m_ui->stackedWidget->setCurrentIndex(0);
            showLoadGame();

        } else {
            QMessageBox::warning(this, "Uneli ste nepostojece ime", "Molimo zapocnite novu igru ili unesite ispravno ime!");
        }
    } else {
        QMessageBox::warning(this, "Upozorenje", "Niste uneli ime!");
    }
}

void mainwindow::pregledRezultataClicked()
{
    m_ui->stackedWidget->setCurrentIndex(3);
    setUpTabWidges();
}

void mainwindow::podesavanjaClicked() { m_ui->stackedWidget->setCurrentIndex(4); }

void mainwindow::napustiIgricuClicked()
{
    QMessageBox msgBox;
    msgBox.setText("Da li zaista želite da napustite igricu?");

    QAbstractButton* daButton = msgBox.addButton("Da", QMessageBox::YesRole);
    QAbstractButton* neButton = msgBox.addButton("Ne", QMessageBox::NoRole);

    // Ukloni standardne dugmadi za zatvaranje i minimizaciju
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

    QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
                               "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
                               "padding: 10px; border-radius: 21px; font: bold 14px;}"
                               "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
                               "#bcbcbc; }"
                               "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
                               "#a0a0a0; }";

    QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";

    msgBox.setStyleSheet(msgBoxStylesheet);

    daButton->setStyleSheet(buttonStylesheet);
    neButton->setStyleSheet(buttonStylesheet);

    msgBox.exec();

    if (msgBox.clickedButton() == daButton) {
        qApp->quit();
    }
}

void mainwindow::natragNaGlavniMeniClicked()
{
    m_ui->stackedWidget->setCurrentIndex(0);
    showLoadGame();
}

QString mainwindow::formatSeconds(int seconds)
{
    QTime time = QTime(0, 0).addSecs(seconds);

    return time.toString("h:mm:ss");
}

void mainwindow::setUpTabWidges()
{
    m_ui->twRezultati->setColumnCount(2);
    m_ui->twRezultati->setColumnWidth(0, 299.5);
    m_ui->twRezultati->setColumnWidth(1, 299.5);
    m_ui->twRezultati->setHorizontalHeaderLabels(QStringList() << "Igrac"
                                                               << "Vreme");

    for (int i = 0; i < m_ui->twRezultati->rowCount(); i++) {
        m_ui->twRezultati->removeRow(i);
    }

    QVector<Igrac> igraci = m_rez->getIgraci();
    std::sort(igraci.begin(), igraci.end(), [](const Igrac& obj1, const Igrac& obj2) { return obj1.getVreme() < obj2.getVreme(); });
    qDebug() << igraci.size();
    if (igraci.size() > 0) {
        int row = 0;

        for (const Igrac& igrac : igraci) {

            if (igrac.getPoslednjiOtkljucaniChallenge() == 16) {
                // int row = ui->twRezultati->rowCount();

                m_ui->twRezultati->insertRow(row);

                QTableWidgetItem* newItemPlayer = new QTableWidgetItem(igrac.getIme());
                newItemPlayer->setFlags(newItemPlayer->flags() ^ Qt::ItemIsEditable);
                m_ui->twRezultati->setItem(row, 0, newItemPlayer);

                bool ok;
                QString score = formatSeconds(igrac.getVreme());

                QTableWidgetItem* newItemScore = new QTableWidgetItem(score);
                newItemScore->setFlags(newItemScore->flags() ^ Qt::ItemIsEditable);
                m_ui->twRezultati->setItem(row, 1, newItemScore);
                row++;
            }
        }

    } else {
        qDebug() << "savefile prazan";
    }
}

void mainwindow::setUpBackground()
{
    QPixmap bkgnd(":/new/resources/resources/cropped_map.png");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    QPalette palette;

    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);
}

void mainwindow::showLoadGame()
{

    QVector<Igrac> igraci = m_rez->getIgraci();

    if (igraci.size() > 0) {
        m_ui->pbNastaviIgru->setEnabled(true);
        m_ui->pbNastaviIgru->setVisible(true);
    } else {
        m_ui->pbNastaviIgru->setEnabled(false);
        m_ui->pbNastaviIgru->setVisible(false);
    }
}

bool mainwindow::eventFilter(QObject* obj, QEvent* event)
{
    if (event->type() == QEvent::MouseButtonPress) {
        // Emitujemo signal kada se desi klik
        emit clicked();
        return false;
    }
    return QObject::eventFilter(obj, event);
}

void mainwindow::klikMelodija()
{
    // Provera da li je došlo do klika na bilo kojem mestu na ekranu
    if (QApplication::activeWindow() != nullptr) {
        // Reprodukcija zvuka
        QMediaPlayer* player = new QMediaPlayer();
        QAudioOutput* audioOutput = new QAudioOutput();
        player->setAudioOutput(audioOutput);
        // connect(player, &QMediaPlayer::positionChanged, this,
        // &MediaExample::positionChanged);
        player->setSource(QUrl("qrc:/new/resources/resources/mixkit-arcade-game-jump-coin-216.wav"));
        audioOutput->setVolume(10);
        if (m_ui->RB_ukljucen->isChecked())
            player->play();
    }
}

void mainwindow::jacinaPozadinseMelodije(int vrednost) { m_audioOutput->setVolume(static_cast<qreal>(vrednost)); }

void mainwindow::cestitajIgracu()
{
    //    mapa->close();
    pregledRezultataClicked();

    QMessageBox msgBox;
    msgBox.setText("Čestitamo na kompletiranju svih izazova! Kliknite ok da "
                   "biste pregledali svoj rezultat.");

    QAbstractButton* okBtn = msgBox.addButton("Ok", QMessageBox::YesRole);

    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

    QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
                               "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
                               "padding: 10px; border-radius: 21px; font: bold 14px;}"
                               "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
                               "#bcbcbc; }"
                               "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
                               "#a0a0a0; }";

    QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";

    msgBox.setStyleSheet(msgBoxStylesheet);

    okBtn->setStyleSheet(buttonStylesheet);

    msgBox.exec();

    if (msgBox.clickedButton() == okBtn) { }
}

void mainwindow::prikaziUputstvo()
{

    QMessageBox msgBox;
    msgBox.setText("Dobrodošli u BgChallenge! Cilj igrice je da prođeš kroz sve izazove i "
                   "otkriješ što više o Beogradu. Kad dođeš na lokaciju, dobićeš jedan "
                   "izazov. Svaki sadrži nešto karakteristično vezano za tu lokaciju, bilo "
                   "to "
                   "u vidu šifre koju treba da otkriješ ili samog zadatka. Nakon što ga "
                   "rešiš, prikazaće ti se tekst o toj lokaciji - prilika da saznaš nešto "
                   "novo i zanimljivo o poznatim objektima Beograda. Sledeća lokacija na "
                   "tvojoj turi je misterija: moraćeš da rešiš asocijaciju i da pogodiš gde "
                   "te vodimo dalje! Imaj na umu: odgovor je uglavnom neka turistička "
                   "lokacija. Uživaj u igrici!");
    msgBox.setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);
    msgBox.exec();
}
