#include "../include/rezultat.h"

#include <QDebug>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>

void Rezultat::dodajIgraca(const Igrac& noviIgrac)
{
    if (!postojiIgrac(noviIgrac.getIme()))
        m_igraci.append(noviIgrac);
    else
        qDebug() << "Igrac vec postoji!";
}

QVector<Igrac> Rezultat::getIgraci() const { return m_igraci; }

void Rezultat::sacuvajIgraceJson(const QString& filename) const
{
    QJsonArray playerArray;
    for (const Igrac& igrac : m_igraci) {
        playerArray.append(igrac.toJson());
    }

    QJsonObject jsonObject;
    jsonObject["players"] = playerArray;

    QJsonDocument jsonDocument(jsonObject);

    QFile file(filename);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        file.write(jsonDocument.toJson());
        file.close();
    } else {
        qDebug() << "File not opened" << file.errorString();
    }
}

void Rezultat::ucitajIgraceJson(const QString& filename)
{
    m_igraci.clear();

    QFile file(filename);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QByteArray jsonData = file.readAll();
        file.close();

        QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonData);
        QJsonArray playerArray = jsonDocument["players"].toArray();

        for (const QJsonValue& playerValue : playerArray) {
            QJsonObject playerObject = playerValue.toObject();
            Igrac igrac = Igrac::fromJson(playerObject);
            m_igraci.append(igrac);
        }
    }
}

void Rezultat::azurirajIgraca(const QString& ime, const Igrac& azuriranIgrac)
{
    for (Igrac& igrac : m_igraci) {
        if (igrac.getIme() == ime) {
            igrac = azuriranIgrac;
            break;
        }
    }
}

void Rezultat::azurirajIgracaJson(const QString& filename, const QString& ime, const Igrac& azuriranIgrac)
{
    ucitajIgraceJson(filename);
    azurirajIgraca(ime, azuriranIgrac);
    sacuvajIgraceJson(filename);
}

bool Rezultat::postojiIgrac(const QString& ime)
{
    for (Igrac& igrac : m_igraci) {
        if (igrac.getIme() == ime) {
            return true;
        }
    }

    return false;
}

Igrac Rezultat::nadjiIgraca(const QString& ime)
{
    for (Igrac& igrac : m_igraci) {
        if (igrac.getIme() == ime) {
            return igrac;
        }
    }

    Igrac nepostojeci("");
    return nepostojeci;
}
