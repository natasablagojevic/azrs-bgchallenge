#include "../include/kviz.h"
#include "../forms/ui_kvizUI.h"
#include <QDebug>
#include <QDialogButtonBox>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPalette>
#include <QPixmap>

Kviz::Kviz(Timer* vreme, int pocetnoVreme, QWidget* parent)
    : ChallengeTest(2, 1, vreme, pocetnoVreme, parent)
    , m_ui(new Ui::kviz)
{
    m_ui->setupUi(this);
    m_ui->stackedWidget->setCurrentIndex(0);
    setImagePath(":/new/pozadine/resources/staro_sajmiste.jpg");

    connect(m_vreme, &Timer::azurirajVreme, this, [=](int vreme) {
        m_pocetnoVreme++;
        QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
        m_ui->lcdNumber->display(time.toString("h:mm:ss"));
        update();
    });
    connect(m_ui->pbNazad, &QPushButton::clicked, this, &Kviz::zatvori);
    connect(m_ui->pbPitanje1Odgovor1, &QPushButton::clicked, this, &Kviz::onPitanje1OdgovorClicked);
    connect(m_ui->pbPitanje1Odgovor2, &QPushButton::clicked, this, &Kviz::onPitanje1OdgovorClicked);
    connect(m_ui->pbPitanje1Odgovor3, &QPushButton::clicked, this, &Kviz::onPitanje1OdgovorClicked);
    connect(m_ui->pbPitanje1Odgovor4, &QPushButton::clicked, this, &Kviz::onPitanje1OdgovorClicked);

    connect(m_ui->pbPitanje2Odgovor1_2, &QPushButton::clicked, this, &Kviz::onPitanje2OdgovorClicked);
    connect(m_ui->pbPitanje2Odgovor2_2, &QPushButton::clicked, this, &Kviz::onPitanje2OdgovorClicked);
    connect(m_ui->pbPitanje2Odgovor3_2, &QPushButton::clicked, this, &Kviz::onPitanje2OdgovorClicked);
    connect(m_ui->pbPitanje2Odgovor4_2, &QPushButton::clicked, this, &Kviz::onPitanje2OdgovorClicked);

    connect(m_ui->pbPitanje3Odgovor1, &QPushButton::clicked, this, &Kviz::onPitanje3OdgovorClicked);
    connect(m_ui->pbPitanje3Odgovor2, &QPushButton::clicked, this, &Kviz::onPitanje3OdgovorClicked);
    connect(m_ui->pbPitanje3Odgovor3, &QPushButton::clicked, this, &Kviz::onPitanje3OdgovorClicked);
    connect(m_ui->pbPitanje3Odgovor4, &QPushButton::clicked, this, &Kviz::onPitanje3OdgovorClicked);

    connect(m_ui->pbPitanje4Odgovor1, &QPushButton::clicked, this, &Kviz::onPitanje4OdgovorClicked);
    connect(m_ui->pbPitanje4Odgovor2, &QPushButton::clicked, this, &Kviz::onPitanje4OdgovorClicked);
    connect(m_ui->pbPitanje4Odgovor3, &QPushButton::clicked, this, &Kviz::onPitanje4OdgovorClicked);
    connect(m_ui->pbPitanje4Odgovor4, &QPushButton::clicked, this, &Kviz::onPitanje4OdgovorClicked);

    tekstovi();
}

void Kviz::setUpBackground(const QString& path)
{
    QPixmap bkgnd(path);
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    QPalette palette;

    palette.setBrush(QPalette::Window, QBrush(bkgnd));
    this->setPalette(palette);
}

void Kviz::onPitanje1OdgovorClicked() { m_ui->stackedWidget->setCurrentIndex(1); }

void Kviz::onPitanje2OdgovorClicked() { m_ui->stackedWidget->setCurrentIndex(2); }

void Kviz::onPitanje3OdgovorClicked() { m_ui->stackedWidget->setCurrentIndex(3); }

void Kviz::onPitanje4OdgovorClicked() { predjiDalje(); }

void Kviz::predjiDalje()
{

    QString targetText = "1941";
    m_isCorrect = false;

    while (!m_isCorrect) {
        QInputDialog inputDialog;
        inputDialog.setInputMode(QInputDialog::TextInput);
        inputDialog.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
        inputDialog.setLabelText("Na osnovu odgovora sa kviza napišite konačan odgovor:\n");
        inputDialog.setCancelButtonText("Igraj ponovo");
        inputDialog.setWindowTitle(" ");

        QLineEdit* lineEdit = inputDialog.findChild<QLineEdit*>();
        if (lineEdit) {
            lineEdit->setStyleSheet("QLineEdit { background-color: #f0f0f0; border: 1px solid #dcdcdc; "
                                    "color: #333; padding: 10px; border-radius: 5px; font: bold 14px;}");
        }

        QDialogButtonBox* buttonBox = inputDialog.findChild<QDialogButtonBox*>();
        if (buttonBox) {
            QPushButton* okButton = buttonBox->button(QDialogButtonBox::Ok);
            QPushButton* cancelButton = buttonBox->button(QDialogButtonBox::Cancel);

            okButton->setStyleSheet("min-width: 100px; min-height: 20px; margin-right: 10px; "
                                    "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
                                    "padding: 10px; border-radius: 21px; font: bold 14px;");
            cancelButton->setStyleSheet("min-width: 100px; min-height: 20px; margin-right: 10px; "
                                        "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
                                        "padding: 10px; border-radius: 21px; font: bold 14px;");
        }

        int result = inputDialog.exec();
        if (result == QDialog::Accepted) {
            QString text = inputDialog.textValue();
            if (text == targetText) {
                m_isCorrect = true;

                foreach (QObject* child, this->children()) {
                    if (child->isWidgetType()) {
                        QWidget* childWidget = qobject_cast<QWidget*>(child);
                        if (childWidget) {
                            qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

                            childWidget->hide();
                        }
                    }
                }

                if (this->layout() != nullptr) {
                    QLayoutItem* item;
                    while ((item = this->layout()->takeAt(0)) != nullptr) {
                        qDebug() << "brisem" << item->widget()->objectName() << item->widget()->parent()->objectName();

                        item->widget()->setVisible(false);
                        delete item;
                    }
                    delete this->layout();
                }

                prikaziEdukativniTekst();
            }
        } else {
            m_ponovi = true;
            m_ui->stackedWidget->setCurrentIndex(0);
            break;
        }
    }
}

Kviz::~Kviz() { delete m_ui; }

void Kviz::tekstovi()
{

    m_edukativniTekst = R"(
Prvi beogradski sajam imao je dobar početak, sa bogatom ponudom koja ga je učinila popularnim mestom za izlazak Beograđana.U vremenu u kome je sagrađeno, postojala je još samo jedna naseobina na toj obali, takozvano Novo naselje, koje je u suštini bilo divlje (Bežanija je još uvek bila isuviše daleko po ondašnjim standardima da bi se tretirala kao Beograd; čak su i Dedinje i Senjak, ma koliko elitni, bili u suštini periferija).Njegovom izgradnjom, Beograd je po prvi put iskoračio na drugu obalu reke Save i time otvorio put budućim graditeljima Novog Beograda.

Prvi televizijski program emitovan na Balkanu bio je emitovan upravo sa beogradskog sajma, 1938. godine. Čehoslovačka „Škoda“ je konstruisala na sajmu toranj visok 74 m, koji je korišćen za obuku padobranaca iz svih delova zemlje.

No, potom je Nemačka administracija odabrala ovu lokaciju za logor i sve se promenilo.

Decembra 1941. godine formiran je Jevrejski Logor Zemun (Judenlager Semlin) koji je teritorijelno pripadao Nezavisnoj državi Hrvatskoj. Nemačka je dobila odobrenje da na toj lokaciji osnuje logor, a postojao je uslov da ne bude srpskih stražara, ali da se snabdeva iz  Beograda. Logor je formirao Gestapo radi likvidacije jevrejskog i romskog stanovništva na području Beograda i Srbije. Sajam je pretvoren u paviljon za mučenje i vešanje, mrtvačnicu, bolnicu, smeštaj logoraša.U logor je dovedeno 6400 Jevreja i oko 600 Roma.
Mnogi zatočenici su stradali zime 1941/1942. godinu zbog hladnoće, gladi i bolesti, a kada je zima prošla, preživele je čekala gasna komora. Logor je postojao 5 meseci, i za to vreme ubijeno je više od 6000 Jevreja.

Posle Drugog svetskog rata, Sajmište nije moglo da povrati svoj stari sjaj. Počela je izgradnja Novog Beograda na levoj obali reke Save, a u očuvane paviljone smeštene su omladinske radne brigade. )";

    m_asocijacija = "Bajaga i Eplay pevaju zajedno jednu pesmu."; // zamisao je da se ovo
                                                                  // pusti u pozadini, tekst
                                                                  // u labeli moze da stoji
                                                                  // tipa "Muzicka
                                                                  // asocijacija.."

    m_hint = "Ja mogu ono što niko ne može, ne mogu ono što svako može...";

    m_resenjeAsocijacije.append("hotel jugoslavija");
    m_resenjeAsocijacije.append("hotel jugoslavije");
    m_naslovEdukativnogTeksta = "O Starom Sajmištu";
}
