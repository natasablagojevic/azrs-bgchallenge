#include "../include/matf.h"
#include "../forms/ui_matf.h"

#include <QLabel>
#include <QPushButton>
#include <QTableWidget>
#include <QVBoxLayout>

MATF::MATF(Timer* timer, int pocetnoVreme, QWidget* parent)
    : ChallengeTest(16, 4, timer, pocetnoVreme, parent)
    , m_ui(new Ui::MATF)
{
    // NE DIRATI, NECE DA UCITA UI
    if (this->layout() != nullptr) {
        QLayoutItem* item;
        while ((item = this->layout()->takeAt(0)) != nullptr) {
            qDebug() << "brisem" << item->widget()->objectName() << item->widget()->parent()->objectName();

            delete item->widget();
            delete item;
        }
        delete this->layout();
    }

    m_ui->setupUi(this);

    tekstovi();

    this->setWindowTitle("BgChallenge");
    this->setMinimumWidth(1280);
    this->setMinimumHeight(720);
    this->setFixedSize(this->width(), this->height());

    this->setImagePath(":/new/matf/resources/matf_bg.jpg");
    this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

    connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
        m_pocetnoVreme++;
        QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
        m_ui->lcdNumber->display(time.toString("h:mm:ss"));
        update();
    });

    m_ui->tableWidget->setRowCount(3);
    m_ui->tableWidget->setColumnCount(3);

    // QTimer::singleShot(5000, [this]() {
    //     ui->label->setVisible(false);
    // });

    m_ui->tableWidget->setStyleSheet("QTableWidget {background-color: #f0f0f0;} QTableWidget::item { "
                                     "text-align: center; background-color: #f0f0f0; color: #333; } "
                                     "QTableWidget::item:selected{ text-align: center; background-color: "
                                     "#d0d0d0; }");
    m_ui->tableWidget->setItem(0, 0, new QTableWidgetItem("8"));
    m_ui->tableWidget->setItem(1, 1, new QTableWidgetItem("5"));
    m_ui->tableWidget->setItem(1, 2, new QTableWidgetItem("9"));
    m_ui->tableWidget->setItem(2, 2, new QTableWidgetItem("2"));
    m_ui->tableWidget->item(0, 0)->setTextAlignment(Qt::AlignCenter);
    m_ui->tableWidget->item(1, 1)->setTextAlignment(Qt::AlignCenter);
    m_ui->tableWidget->item(1, 2)->setTextAlignment(Qt::AlignCenter);
    m_ui->tableWidget->item(2, 2)->setTextAlignment(Qt::AlignCenter);
    // Set the items as non-editable and non-selectable
    m_ui->tableWidget->item(0, 0)->setFlags(m_ui->tableWidget->item(0, 0)->flags() ^ Qt::ItemIsEnabled);
    m_ui->tableWidget->item(1, 1)->setFlags(m_ui->tableWidget->item(1, 1)->flags() ^ Qt::ItemIsEnabled);
    m_ui->tableWidget->item(1, 2)->setFlags(m_ui->tableWidget->item(1, 2)->flags() ^ Qt::ItemIsEnabled);
    m_ui->tableWidget->item(2, 2)->setFlags(m_ui->tableWidget->item(2, 2)->flags() ^ Qt::ItemIsEnabled);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (!m_ui->tableWidget->item(i, j)) {
                QTableWidgetItem* item = new QTableWidgetItem();
                item->setTextAlignment(Qt::AlignCenter);
                item->setFlags(item->flags() | Qt::ItemIsEditable);
                m_ui->tableWidget->setItem(i, j, item);
            }
        }
    }

    m_ui->tableWidget->setShowGrid(true);

    //    connect(ui->pbSetupUi, &QPushButton::clicked, this,
    //    &Tabela::onPbSetupUiClicked);
    connect(m_ui->pbProveri, &QPushButton::clicked, this, &MATF::onPbProveriClicked);
    connect(m_ui->pbHint, &QPushButton::clicked, this, &MATF::onPbHintClicked);
    connect(m_ui->pbNazad, &QPushButton::clicked, this, &MATF::zatvori);
}

MATF::~MATF() { delete m_ui; }

void MATF::onPbHintClicked()
{
    QDialog dijalog;
    dijalog.setFixedHeight(300);
    dijalog.setFixedWidth(400);
    dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
                          "rgb(200,200,200); margin: 10px;}");
    QLabel* slika = new QLabel(&dijalog);
    slika->setFixedSize(200, 200);
    slika->setAlignment(Qt::AlignCenter);
    QPixmap kvadrat("://resources/magicni_kvadrat/kvadrat.png");
    slika->setPixmap(kvadrat.scaled(slika->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    slika->setAlignment(Qt::AlignCenter);
    QLabel* text = new QLabel("Zbir nad vrstama, kolonama i dijagonalama treba da bude jednak 15.", &dijalog);
    text->setAlignment(Qt::AlignCenter);
    text->setWordWrap(true);
    text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
    QPushButton* ok = new QPushButton("OK", &dijalog);
    ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
                      "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
                      "padding: 10px; border-radius: 21px; font: bold 14px;}"
                      "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
                      "#bcbcbc; }"
                      "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
                      "#a0a0a0; }");
    QVBoxLayout* layout = new QVBoxLayout(&dijalog);

    layout->addWidget(text);
    layout->addWidget(slika, 0, Qt::AlignCenter);
    layout->addWidget(ok);
    connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
    dijalog.exec();
}

void MATF::onPbProveriClicked()
{
    QVector<QVector<int>> matrica(3, QVector<int>(3));

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            QTableWidgetItem* item = m_ui->tableWidget->item(i, j);
            if (item != nullptr) {
                bool ok;
                matrica[i][j] = item->text().toInt(&ok);

                if (ok) {
                    qDebug() << "Sve ok!" << matrica[i][j];
                } else {
                    qDebug() << "parsing failed!";
                }
            }
        }
    }

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++)
            qDebug() << matrica[i][j] << ' ';
        qDebug() << "\n";
    }

    if (checkMatrix(matrica)) {

        QObjectList children = this->children();
        for (QObject* child : children) {
            // qDebug() << child->objectName();
            if (QWidget* widget = qobject_cast<QWidget*>(child)) {
                // qDebug() << widget->objectName() << widget->parent()->objectName();
                widget->setVisible(false);
            }
        }
        QLayout* postojeciLayout = this->layout();
        if (postojeciLayout) {
            delete postojeciLayout;
        }

        prikaziEdukativniTekst();

    } else {
        QMessageBox::warning(this, "Upozorenje",
            "Brojeve koje ste uneli ne pedstavljauju elemente magicnog "
            "kvadrata.\nPazljivo pogledajte kvadrat i nadjite gresku.");
    }
}

bool MATF::checkSum(const int& a, const int& b, const int& c) { return a + b + c == 15; }

bool MATF::checkMatrix(const QVector<QVector<int>>& m)
{
    return checkSum(m[0][0], m[0][1], m[0][2]) && checkSum(m[1][0], m[1][1], m[1][2]) && checkSum(m[2][0], m[2][1], m[2][2]) && checkSum(m[0][0], m[1][0], m[2][0])
        && checkSum(m[0][1], m[1][1], m[2][1]) && checkSum(m[0][2], m[1][2], m[2][2]) && checkSum(m[0][0], m[1][1], m[2][2]) && checkSum(m[0][2], m[1][1], m[2][0]);
}

void MATF::tekstovi()
{
    m_naslovEdukativnogTeksta = "O Matematičkom fakultetu";
    m_edukativniTekst = R"(
Kolosalna zgrada na vrhu dorćolske padine, zvana skraćenicom ustanove koja se tu nalazila -“PMF”, velika palata nekadašnjeg Prirodno-matematičkog fakulteta, danas sigurno ne izgleda baš tako moćno kao u doba kada je nastala, krajem četrdesetih i početkom pedesetih godina 20. veka.
Bilo je to doba puno zanosa, želje za napretkom, koji je ova betonska kraljica izuzetno simbolizovala. Ali, istovremeno, bila je to epoha i opasnih političkih turbulencija. I to na svetskom nivou.
Valjda je baš zato ubrzo nastala legenda o skrivenoj poruci koju je ovo moćno zdanje moderne arhitekture nosilo. Doduše, “poruku” je jedino bilo moguće videti iz aviona ili nekakvih špijunskih balona, preteče savremenih satelita, ali upravo takve sprave su i koristili u preletima preko tadašnjeg Beograda oni koji je najpre trebalo da prime tu poruku.

Ali, krenimo redom, jer već je i sama lokacija izabrana za izgradnju novog “hrama nauke” u socijalističkom društvu koje je i samo tek krenulo da se stvara, posle 1945. godine, na obodu Studentskog parka, nosila u sebi mnoge turbulentne i mračne asocijacije.

Da bi krenula izgradnja PMF, negde tamo 1951. počela je priprema za rušenje jedne naoko simpatične, prizemne zgrade sa lučnim prozorima i ulazom.
Bila je stara, ali nekako “sterilna“. Možda bi lepše i romantičnije izgledala da se u njoj našlo neko društvo žena, hor ili vrtić. Možda joj je takvu namenu i trebalo odrediti kako bi se u nju unela vedrina. Ili… mogla je da postane muzej, ali u tom slučaju muzej nečeg mučnog.

Ta stara zgrada nije tako sablasna bez razloga. Nije tu prolazio baš ko nije morao, a koga je put i naveo, pružio bi malo korak. Jer, unutra je bila “ćorka“. Tako se zvala mala pritvorenička soba. A veća, po kojoj je i cela zgrada dobila ime, zvala se “glavnjača”. Bilo je tu i drugih soba popularno nazvanih “ženski salon” i “gospodska soba”.

To nije bio, dakle, zatvor, već pritvor i mesto gde je prema građanima postupala policija u svojim istražnim postupcima. I normalno, nije bilo prijatno ni proći pored Glavnjače kako je glasio ozloglašeni nadimak niskog zdanja Uprave Grada Beograda.
Tu su završavali i oni koji su bili gonjeni zbog svoje političke orijentacije. Na primer, komunisti, ali i nacionalisti-separatisti. Bilo je to pravo mučilište i to skoro čitav vek.

Tokom okupacije ovo, začudo, nije bio zatvor. Međutim, politički zatvorenici vraćaju se u nju posle oslobođenja Beograda. Ali, sad je priča obrnuta, Glavnjačom upravljaju možda baš oni koji su u njoj krvarili u vreme Kraljevine. To je postalo sedište OZNE.

I tako je bilo sve do 1953. kada je sagrađen Centralni zatvor.";)";
    // nema asocijaciju
}

bool MATF::getCheckSum(const int& a, const int& b, const int& c) { return checkSum(a, b, c); }
