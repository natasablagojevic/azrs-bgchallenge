#include "../include/memorija.h"

#include <QDebug>
#include <QPainter>

#include <QBrush>
#include <QFile>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPalette>
#include <QPixmap>
#include <QPushButton>
#include <QString>
#include <QTextBrowser>
#include <QUiLoader>

#include <QDialog>

Memorija::Memorija(Timer* vreme, int pocetnoVreme, QWidget* parent)
    : ChallengeTest(7, 2, vreme, pocetnoVreme, parent)
{

    this->setWindowTitle("BgChallenge");
    this->setMinimumWidth(1280);
    this->setMinimumHeight(720);
    this->setFixedSize(this->width(), this->height());

    this->setImagePath(":/new/pozadine/resources/hram.jpg");
    this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

    QFile file(":/new/resources/forms/memorija.ui");

    if (!file.open(QFile::ReadOnly)) {
        qDebug() << "Error opening UI file memorija.ui:" << file.errorString();
        return;
    }

    QUiLoader loader;
    m_ui = loader.load(&file, this);
    m_ui->setFixedSize(this->width(), this->height());
    file.close();

    QLCDNumber* lcdNumber = m_ui->findChild<QLCDNumber*>("lcdNumber");

    connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
        m_pocetnoVreme++;
        QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
        lcdNumber->display(time.toString("h:mm:ss"));
        update();
    });

    connect(m_ui->findChild<QPushButton*>("pbNazad"), &QPushButton::clicked, this, &Memorija::zatvori);
    connect(m_ui->findChild<QPushButton*>("pbPomoc"), &QPushButton::clicked, this, &Memorija::pomoc);

    m_brojac = new int(0);

    m_sifra = "MOZAIK";

    m_timerNetacno = new QTimer(this);
    m_timerNetacno->setSingleShot(true);
    connect(m_timerNetacno, &QTimer::timeout, this, &Memorija::handleTimeoutNetacno);

    m_timerTacno = new QTimer(this);
    m_timerTacno->setSingleShot(true);
    connect(m_timerTacno, &QTimer::timeout, this, &Memorija::handleTimeoutTacno);

    // Deklaracija dugmadi
    m_btnCard1 = m_ui->findChild<QPushButton*>("btnCard1");
    m_btnCard2 = m_ui->findChild<QPushButton*>("btnCard2");
    m_btnCard4 = m_ui->findChild<QPushButton*>("btnCard4");
    m_btnCard6 = m_ui->findChild<QPushButton*>("btnCard6");
    m_btnCard7 = m_ui->findChild<QPushButton*>("btnCard7");
    m_btnCard8 = m_ui->findChild<QPushButton*>("btnCard8");
    m_btnCard9 = m_ui->findChild<QPushButton*>("btnCard9");
    m_btnCard10 = m_ui->findChild<QPushButton*>("btnCard10");
    m_btnCard11 = m_ui->findChild<QPushButton*>("btnCard11");
    m_btnCard12 = m_ui->findChild<QPushButton*>("btnCard12");
    m_btnCard13 = m_ui->findChild<QPushButton*>("btnCard13");
    m_btnCard14 = m_ui->findChild<QPushButton*>("btnCard14");
    m_btnCard15 = m_ui->findChild<QPushButton*>("btnCard15");
    m_btnCard16 = m_ui->findChild<QPushButton*>("btnCard16");
    m_btnCard17 = m_ui->findChild<QPushButton*>("btnCard17");
    m_btnCard18 = m_ui->findChild<QPushButton*>("btnCard18");
    m_btnCard19 = m_ui->findChild<QPushButton*>("btnCard19");
    m_btnCard20 = m_ui->findChild<QPushButton*>("btnCard20");

    m_leResenjeIzazova = m_ui->findChild<QLineEdit*>("leResenjeIzazova");
    m_tbUputstvo = m_ui->findChild<QTextBrowser*>("tbUputstvo");

    m_buttonList = QList<QPushButton*>(); // Ne treba new QList
    m_buttonPar = QList<QPushButton*>();

    m_buttonList.append(m_btnCard1);
    m_buttonList.append(m_btnCard2);
    m_buttonList.append(m_btnCard4);
    m_buttonList.append(m_btnCard6);
    m_buttonList.append(m_btnCard7);
    m_buttonList.append(m_btnCard8);
    m_buttonList.append(m_btnCard9);
    m_buttonList.append(m_btnCard10);
    m_buttonList.append(m_btnCard11);
    m_buttonList.append(m_btnCard12);
    m_buttonList.append(m_btnCard13);
    m_buttonList.append(m_btnCard14);
    m_buttonList.append(m_btnCard15);
    m_buttonList.append(m_btnCard16);
    m_buttonList.append(m_btnCard17);
    m_buttonList.append(m_btnCard18);
    m_buttonList.append(m_btnCard19);
    m_buttonList.append(m_btnCard20);

    tekstovi();

    for (QPushButton* btn : m_buttonList) {

        connect(btn, &QPushButton::clicked, this, &Memorija::onBtnClicked);
        QString imageNameBack = ":/new/memorija/resources/matflogo.png";
        QPixmap pixmap1(imageNameBack);
        pixmap1 = pixmap1.scaled(btn->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);

        btn->setAttribute(Qt::WA_TranslucentBackground);
        btn->setWindowOpacity(1.0);

        btn->setStyleSheet(QString("    color: rgba(0, 0, 0, 0); "
                                   "    border-style: outset;"
                                   "    border-width: 2px;"
                                   "    border-radius: 10px;"
                                   "    border-color: beige;"
                                   "    font: bold 14px;"
                                   "    padding: 6px;"
                                   "    border-image: url("
            + imageNameBack + ");"));
    }

    // buttonList.clear();

    connect(m_leResenjeIzazova, &QLineEdit::returnPressed, this, [=]() {
        if (m_leResenjeIzazova->text().toUpper() == "MOZAIK") {
            QObjectList children = this->children();
            for (QObject* child : children) {
                if (QWidget* widget = qobject_cast<QWidget*>(child)) {
                    widget->setVisible(false);
                }
            }
            QLayout* postojeciLayout = this->layout();
            if (postojeciLayout) {
                delete postojeciLayout;
            }
            prikaziEdukativniTekst();
        }
    });
}

Memorija::~Memorija()
{
    delete m_timerNetacno;
    delete m_timerTacno;
    delete m_brojac;

    if (m_mEffect0 != nullptr) {

        delete m_mEffect0;
        m_mEffect0 = nullptr;
        qDebug() << "mEffect0 obrisan i stavlje na nullptr";
    }

    if (m_mEffect1 != nullptr) {
        delete m_mEffect1;
        m_mEffect1 = nullptr;
        qDebug() << "mEffect1 obrisan i stavlje na nullptr";
    }

    if (m_animation1 != nullptr) {
        delete m_animation1;
        m_animation1 = nullptr;
        qDebug() << "animation1 obrisan i stavlje na nullptr";
    }

    if (m_animation2 != nullptr) {
        delete m_animation2;
        m_animation2 = nullptr;
        qDebug() << "animation2 obrisan i stavlje na nullptr";
    }

    if (m_opacityAnimation1 != nullptr) {
        delete m_opacityAnimation1;
        m_opacityAnimation1 = nullptr;
        qDebug() << "opacityAnimation1 obrisan i stavlje na nullptr";
    }

    if (m_opacityAnimation2 != nullptr) {
        delete m_opacityAnimation2;
        m_opacityAnimation2 = nullptr;
        qDebug() << "opacityAnimation2 obrisan i stavlje na nullptr";
    }
    if (m_group != nullptr) {
        delete m_group;
        m_group = nullptr;
        qDebug() << "group obrisan i stavlje na nullptr";
    }

    delete m_ui;
}

void Memorija::onBtnClicked()
{

    QPushButton* clickedButton = qobject_cast<QPushButton*>(sender());

    if (clickedButton) {

        m_buttonPar.append(clickedButton);

        qDebug() << "velicina liste " << m_buttonPar.size();

        if (m_buttonPar.size() == 2) {

            qDebug() << "kliknuo si dve kartice, sacekaj - Onesposobi";

            onesposobi();
        }

        if (!m_animationInProgress) {

            // Postavite flag da označite početak animacije
            m_animationInProgress = true;

            QPoint pocetnaPozicija = clickedButton->pos();

            // Dodaj jump efekat
            QPropertyAnimation* animation1 = new QPropertyAnimation(clickedButton, "pos");
            animation1->setDuration(200);
            animation1->setStartValue(pocetnaPozicija);
            animation1->setEndValue(pocetnaPozicija + QPoint(0, -10)); // Pomaknite se gore
            animation1->setEasingCurve(QEasingCurve::OutBounce); // Izravnanje sa bounce efektom
            animation1->start();

            connect(animation1, &QPropertyAnimation::finished, this, [=]() {
                // Postavite flag da označite završetak animacije
                m_animationInProgress = false;

                QPropertyAnimation* reverseAnimation1 = new QPropertyAnimation(clickedButton, "pos");
                reverseAnimation1->setDuration(200);
                reverseAnimation1->setStartValue(pocetnaPozicija + QPoint(0, -10)); // Povratak na početnu poziciju
                reverseAnimation1->setEndValue(pocetnaPozicija);
                reverseAnimation1->setEasingCurve(QEasingCurve::OutBounce); // Izravnanje sa bounce efektom
                reverseAnimation1->start();
            });

            QString imageName1 = ":/new/memorija/resources/" + clickedButton->text() + ".png";

            QPixmap pixmap1(imageName1);
            pixmap1 = pixmap1.scaled(clickedButton->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);

            clickedButton->setStyleSheet(QString("    color: rgba(0, 0, 0, 0); "
                                                 "    border-style: outset;"
                                                 "    border-width: 2px;"
                                                 "    border-radius: 10px;"
                                                 "    border-color: beige;"
                                                 "    font: bold 14px;"
                                                 "    padding: 6px;"
                                                 "    border-image: url("
                + imageName1 + ");"));
        }

        // Ako lista sadrži dva člana
        if (m_buttonPar.size() == 2) {

            // Treba proveriti da li se tekstovi poklapaju
            if ((m_buttonPar[0]->objectName() != m_buttonPar[1]->objectName()) && !m_buttonPar[0]->text().compare(m_buttonPar[1]->text())) {
                qDebug() << "Našao si par";
                // Sakrij dugmadi
                // buttonPar[0]->hide();
                // buttonPar[1]->hide();

                m_timerTacno->start(250);

            } else {
                qDebug() << "Nisi našao par";

                m_timerNetacno->start(700);
            }

            // U svakom slučaju, vrati listu na praznu listu
            // buttonPar.clear();
        }
    }
}

void Memorija::handleTimeoutNetacno()
{

    // Ovde pišite kod koji će se izvršiti nakon isteka vremena

    QString imageNameBack = ":/new/memorija/resources/matflogo.png";
    qDebug() << "okrenute su slike, osposobi";
    osposobi();
    QPixmap pixmap1(imageNameBack);
    pixmap1 = pixmap1.scaled(m_buttonPar[0]->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);

    m_buttonPar[0]->setStyleSheet(QString("    color: rgba(0, 0, 0, 0); "
                                        "    border-style: outset;"
                                        "    border-width: 2px;"
                                        "    border-radius: 10px;"
                                        "    border-color: beige;"
                                        "    font: bold 14px;"
                                        "    padding: 6px;"
                                        "    border-image: url("
        + imageNameBack + ");"));

    m_buttonPar[1]->setStyleSheet(QString("    color: rgba(0, 0, 0, 0); "
                                        "    border-style: outset;"
                                        "    border-width: 2px;"
                                        "    border-radius: 10px;"
                                        "    border-color: beige;"
                                        "    font: bold 14px;"
                                        "    padding: 6px;"
                                        "    border-image: url("
        + imageNameBack + ");"));

    m_buttonPar.clear();
}

void Memorija::handleTimeoutTacno()
{

    *m_brojac += 1;

    if (*m_brojac == 9) {
        m_leResenjeIzazova->setText("MOZAIK");
    }

    m_mEffect0 = new QGraphicsOpacityEffect(m_buttonPar[0]);
    m_mEffect0->setOpacity(1.0);
    m_buttonPar[0]->setGraphicsEffect(m_mEffect0);

    m_mEffect1 = new QGraphicsOpacityEffect(m_buttonPar[1]);
    m_mEffect1->setOpacity(1.0);
    m_buttonPar[1]->setGraphicsEffect(m_mEffect1);

    // Pravljenje animacije za prvo dugme (pomeranje na gore i fade out)
    m_animation1 = new QPropertyAnimation(m_buttonPar[0], "geometry");
    m_animation1->setDuration(500); // Podešavanje trajanja animacije
    m_animation1->setStartValue(m_buttonPar[0]->geometry());
    m_animation1->setEndValue(m_buttonPar[0]->geometry().adjusted(0, -50, 0, -50)); // Pomeranje na gore

    /* QPropertyAnimation *opacityAnimation1 = new
    QPropertyAnimation(buttonPar[0], "opacity");
    opacityAnimation1->setDuration(500); // Podešavanje trajanja animacije
    opacityAnimation1->setStartValue(1.0);
    opacityAnimation1->setEndValue(0.0); // Izbledi */

    m_opacityAnimation1 = new QPropertyAnimation(m_mEffect0, "opacity");
    m_opacityAnimation1->setDuration(500);
    m_opacityAnimation1->setStartValue(1.0);
    m_opacityAnimation1->setEndValue(0.0);

    // Pravljenje animacije za drugo dugme (pomeranje na gore i fade out)
    m_animation2 = new QPropertyAnimation(m_buttonPar[1], "geometry");
    m_animation2->setDuration(500); // Podešavanje trajanja animacije
    m_animation2->setStartValue(m_buttonPar[1]->geometry());
    m_animation2->setEndValue(m_buttonPar[1]->geometry().adjusted(0, -50, 0, -50)); // Pomeranje na gore

    /* QPropertyAnimation *opacityAnimation2 = new
    QPropertyAnimation(buttonPar[1], "opacity");
    opacityAnimation2->setDuration(500); // Podešavanje trajanja animacije
    opacityAnimation2->setStartValue(1.0);
    opacityAnimation2->setEndValue(0.0); // Izbledi */

    m_opacityAnimation2 = new QPropertyAnimation(m_mEffect1, "opacity");
    m_opacityAnimation2->setDuration(500);
    m_opacityAnimation2->setStartValue(1.0);
    m_opacityAnimation2->setEndValue(0.0);

    // Pravljenje grupe animacija
    m_group = new QParallelAnimationGroup();

    // Dodavanje animacija u grupu
    m_group->addAnimation(m_animation1);
    m_group->addAnimation(m_opacityAnimation1);
    m_group->addAnimation(m_animation2);
    m_group->addAnimation(m_opacityAnimation2);

    connect(m_group, &QParallelAnimationGroup::finished, this, [=]() {
        // Uklanjanje dugmadi iz stvarnog interfejsa
        m_buttonPar[0]->hide();
        m_buttonPar[1]->hide();
        qDebug() << "nestale su slike, osposobi";
        osposobi();
        // Obezbediti da su sva dugmad uklonjena
        m_buttonPar.clear();
    });

    m_group->start();
}

void Memorija::tekstovi()
{

    m_edukativniTekst = R"(
Inicijativa za podizanje hrama Svetog Save na Vračaru pokrenuta je prilikom obeležavanja tristo godina od spaljivanja moštiju Svetog Save, prvog srpskog arhiepiskopa. Međutim, veliko je pitanje - gde su spaljene mošti. Da, poznato je da su spaljene na Vračaru, ali ono što se naziva Vračarom, razlikovalo se tada i sada.
Lokaciju na današnjem Vračaru još 1895. izabralo je Društvo za podizanje Hrama Svetog Save uvereno kako je tadašnji Vračar isto ono brdo na kome je 1595. godine Sinan paša spalio njegove mošti. Međutim, ispostavlja se da se u 16. veku mesto koje se zvalo Vračar nalazilo 1,3 kilometra dalje. I da je hram na pogrešnom mestu. Gligorije Vozarević, izdavač i knjižar, ceo život je posvetio traženju tačne lokacije, Nakon mnogo godina proučavanja, bio je siguran da je našao mesto spaljivanja moštiju prvog srpskog arhiepiskopa. Otkupivši je od prethodnog vlasnika, obeležio je jednu parcelu i označio je drvenim krstom ofarbanim u crveno. Čak i nakon što je Društvo za podizanje hrama svetog Save na Vračaru odabralo današnju lokaciju za izgradnju hrama, Vozarovićev plac sa krstom je nastavio da privlači pažnju, a čitav taj kraj je dobio ime po prvobitnom simbolu koji ga je godinama krasio – Crveni krst.

U narodu su kolale priče da su svečeve mošti spaljene na najvišoj uzvisini Tašmajdana, brdašcu koje su Beograđani od početka 18. veka nazivali „Čupina umka“.  Ipak, uprkos bogatom istorijskom nasleđu Tašmajdana, odlučeno je da se hram zida na Istočnom Vračaru.

Ideja za izgradnju ovog hrama postojala je jos 1895. godine, ali je izdgradnja zapoceta tek 1935. godine. Prvi i Drugi balkanski rat, kao i Drugi svetski rat, zaustavili su napredak u izgradnji.

Ukrašavanje mozaikom na površini od 15000 kvadratih metara čini ovaj hram posebno zanimljivim. Sadrži više od 50 miliona komadića. Na ovom mozaiku je radilo radilo 300 umetnika mozaičara, a ako uključimo i one koji su ga lepili, na ovom mozaiku je radilo više od 600 ljudi. Smatra se najvećim mozaikom u hrišćanskim svetinjama.

U kripti se nalazi crkva Sv. Lazara. Oslikana je freskama koje prikazuje scene iz života kneza Lazara u neovizantijskom stilu i po pravoslavnom ikonografskom programu. Sveti Sava je bio veliki misionar ili, kako bismo to danas rekli, diplomata, pa se nekako desilo da na vratima na ulasku u Hram, koja su grandiozna, budu napisane molitve na 24 jezika, tako da 'bilo koji stanovnik zemaljske kugle' koji dođe dođe pred Hram može na sebi veoma bliskom jeziku da pročita tri molitve - Oče naš, Bogorodice Devo i Care nebeski. )";

    m_asocijacija = "Idemo kod Tita!";
    m_hint = "Sledeca lokacija se nalazi negde na otkljucanom delu mape. "
             "Najjuznije. Nije Kuća cveća, ali ona se nalazi u okviru ovoga.";

    m_resenjeAsocijacije.append("muzej jugoslavije");
    m_resenjeAsocijacije.append("muzej jugoslavija");
    m_resenjeAsocijacije.append("muzej istorije jugoslavije");
    m_resenjeAsocijacije.append("jugoslovenski muzej");
    m_naslovEdukativnogTeksta = "O Hramu Svetog Save";
}

void Memorija::pomoc()
{
    QDialog dijalog;
    dijalog.setFixedHeight(300);
    dijalog.setFixedWidth(400);
    dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
                          "rgb(200,200,200); margin: 10px;}");

    QLabel* text = new QLabel("U srcu Beograda, Hram Svetog Save stoji kao simbol vere i kulturnog "
                              "nasleđa. Dok se uzdiže visoko iznad grada, podseća nas na bogatu "
                              "istoriju "
                              "i duboke korene naše civilizacije. Podseća nas na to da istorija ne "
                              "treba "
                              "da nas pusti da zaboravljamo kako smo došli do sadašnjosti. Nakon što "
                              "spojiš sve kartice, dobićeš šifru za trenutnu lokaciju.",
        &dijalog);
    text->setAlignment(Qt::AlignCenter);
    text->setWordWrap(true);
    text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
    QPushButton* ok = new QPushButton("OK", &dijalog);
    ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
                      "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
                      "padding: 10px; border-radius: 21px; font: bold 14px;}"
                      "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
                      "#bcbcbc; }"
                      "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
                      "#a0a0a0; }");
    QVBoxLayout* layout = new QVBoxLayout(&dijalog);

    layout->addWidget(text);

    layout->addWidget(ok);
    connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
    dijalog.exec();
}

void Memorija::onesposobi()
{

    for (QPushButton* btn : m_buttonList) {

        btn->setEnabled(false);
    }
}

void Memorija::osposobi()
{

    for (QPushButton* btn : m_buttonList) {

        btn->setEnabled(true);
    }
}
