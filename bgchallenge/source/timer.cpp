#include "../include/timer.h"

Timer::Timer(QObject* parent)
    : QTimer(parent)
{
    m_gameTime = 0;
    m_startTime = QDateTime::currentDateTime();
    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &Timer::azurirajGameTime);
    m_timer->start(1000); // vreme se azurira svake sekunde
}

Timer::~Timer() { delete m_timer; }

void Timer::start() { m_timer->start(1000); }

void Timer::stop()
{
    m_timer->stop();
    m_endTime = QDateTime::currentDateTime();
}

void Timer::azurirajGameTime()
{
    m_gameTime = m_startTime.secsTo(QDateTime::currentDateTime());
    emit azurirajVreme(m_gameTime);
}

int Timer::getGameTime() const { return m_gameTime; }
