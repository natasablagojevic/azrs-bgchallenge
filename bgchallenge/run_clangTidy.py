import os
import subprocess

def run_clang_tidy(source_dir, include_dir):
    # Pronalaženje .cpp i .h fajlova u odgovarajućim direktorijumima
    cpp_files = [os.path.join(root, filename)
                 for root, _, files in os.walk(source_dir)
                 for filename in files if filename.endswith('.cpp')]
    header_files = [os.path.join(root, filename)
                    for root, _, files in os.walk(include_dir)
                    for filename in files if filename.endswith('.h')]

    # Spajanje fajlova u jednu listu
    files_to_check = cpp_files + header_files

    # Pokretanje clang-tidy i primena automatskih popravki (-fix)
    for file in files_to_check:
        print(f"Running clang-tidy for file: {file}")
        subprocess.run(["clang-tidy", file, "--config-file=.clang-tidy", "--fix", "--header-filter=\".*\"", "--fix-errors", "--", "--std=c++17"])

if __name__ == "__main__":
    source_directory = "./source/"
    include_directory = "./include/"

    run_clang_tidy(source_directory, include_directory)

# clang-tidy source/matf.cpp --config-file=.clang-tidy --fix --header-filter=".*" --fix-errors  -- --std=c++20 > log-matf.txt
