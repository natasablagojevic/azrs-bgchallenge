#ifndef __REZULTAT_H__
#define __REZULTAT_H__

#include <QVector>
#include <QJsonObject>

#include "igrac.h"

class Rezultat
{
public:
    void dodajIgraca(const Igrac& noviIgrac);
    QVector<Igrac> getIgraci() const;
    void sacuvajIgraceJson(const QString &filename) const;
    void ucitajIgraceJson(const QString& filename);
    void azurirajIgraca(const QString& ime, const Igrac& azuriranIgrac);
    void azurirajIgracaJson(const QString& filename, const QString& ime, const Igrac& azuriranIgrac);
    bool postojiIgrac(const QString& ime);
    Igrac nadjiIgraca(const QString &ime);


private:
    QVector<Igrac> m_igraci;

};

#endif // __REZULTAT_H__
