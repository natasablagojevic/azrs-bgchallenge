// zeleznickastanica.h

#ifndef ZELEZNICKASTANICA_H
#define ZELEZNICKASTANICA_H

#include <QObject>
#include <QWidget>
#include "timer.h"
#include <QPushButton>
#include "Challenge_test.h"

class ZeleznickaStanica : public ChallengeTest
{
    Q_OBJECT
public:
    ZeleznickaStanica(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
    ~ZeleznickaStanica();

public slots:
    void pomoc();


private slots:
    void levoLjubljanaClicked();
    void desnoLjubljanaClicked();

    void levoMokraGoraClicked();
    void desnoMokraGoraClicked();

    void levoTitoClicked();
    void desnoTitoClicked();

    void levoZagrebClicked();
    void desnoZagrebClicked();

    void btnProveriClicked();

    void tekstovi();


private:


    QList<QString> m_odgovori;
    QPushButton* m_tito;
    QPushButton* m_mokraGora;
    QPushButton* m_zagreb;
    QPushButton* m_ljubljana;

    int* m_brojacTito;
    int* m_brojacMokraGora;
    int* m_brojacZagreb;
    int* m_brojacLjubljana;

    QWidget *m_ui=nullptr;


};

#endif // ZELEZNICKASTANICA_H
