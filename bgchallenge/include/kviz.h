#ifndef KVIZ_H
#define KVIZ_H

#include <QStackedWidget>
#include "Challenge_test.h"
#include "timer.h"


namespace Ui {
class kviz;
}

class Kviz : public ChallengeTest
{
    Q_OBJECT

public:
    explicit Kviz(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
    ~Kviz();
    void setUpBackground(const QString& path);

    void tekstovi();
    void predjiDalje();
    bool m_ponovi;
    bool m_isCorrect;

private:
    friend class KvizTest;
    Ui::kviz *m_ui;


private slots:
    friend class KvizTest;
    void onPitanje1OdgovorClicked();
    void onPitanje2OdgovorClicked();
    void onPitanje3OdgovorClicked();
    void onPitanje4OdgovorClicked();

};

#endif // KVIZ_H
