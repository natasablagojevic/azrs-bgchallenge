#ifndef CHALLENGEKALEMEGDAN_H
#define CHALLENGEKALEMEGDAN_H
#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QVector>
#include <QStringList>
#include <QPaintEvent>
#include "Challenge_test.h"
#include "timer.h"

class ChallengeKalemegdan : public ChallengeTest
{
    Q_OBJECT
public:
    ChallengeKalemegdan(Timer* timer, int pocetnoVreme, QWidget *parent = nullptr);
    virtual void zavrsiChallenge() override;
    void tekstovi();
    ~ChallengeKalemegdan();

    void setTrenutnaPozicija(int poz);
    void setPozicijaPocetnogSlova(int poz);

    int getTrenutnaPozicija();
    int getPozicijaPocetnogSlova();

    QStringList getTrenutnaRec();
    void setTrenutnaRec(const QStringList& rec);

    QVector<QLabel*>& getPolja();

    QWidget *m_ui;


public slots:
    void pogodi();
    void izbrisi();
    virtual void zapocniChallenge() override;
signals:
    void pogodjenaRec();


protected:

private:

    QStringList m_rec;
    QVector<QLabel*> m_polja;
    int m_trenutnaPozicija;
    int m_pozicijaPocetnogSlova;
    QStringList generisiRec();
};

#endif // CHALLENGEKALEMEGDAN_H
