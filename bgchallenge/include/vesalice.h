#ifndef VESALICE_H
#define VESALICE_H
#include "Challenge_test.h"
#include "timer.h"

#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QPainterPath>
#include <QString>
#include <QPainterPath>
#include <QtMath>
#include <QDebug>

namespace Ui {
class vesalice;
}

class Vesalice :  public ChallengeTest
{
    Q_OBJECT

public:
    explicit Vesalice(Timer* vreme,int pocetnoVreme, QWidget* parent = nullptr);
    ~Vesalice();
    Ui::vesalice *m_ui;
    QString getRec();
    void setRec(QString nova);
    int getBrojGreske();


    int getBrojSlova();

protected:
    void paintEvent(QPaintEvent *event);
    void drawHair(QPainter &painter);
    void drawEars(QPainter &painter);
    void drawTear(QPainter &painter, int x, int y);
    void drawGallows(QPainter &painter);
    //void nacrtajDeoCicaglise(int brojgreske);
    void drawHangman(QPainter &painter,int brojgreske);
    void drawHead(QPainter &painter);
    void drawLeftEye(QPainter &painter);
    void drawRightEye(QPainter &painter);
    void drawMouth(QPainter &painter);
    //void drawHair(QPainter &painter);
    void drawBody(QPainter &painter);
    void drawLeftArm(QPainter &painter);
    void drawRightArm(QPainter &painter);
    void drawLeftLeg(QPainter &painter);
    void drawRightLeg(QPainter &painter);
    //void paintEvent(QPaintEvent *event) override;

public slots:
    void tekstovi();
    void proveri2();
    void slovoA();
    void slovoB();
    void slovoV();
    void slovoG();
    void slovoD();
    void slovoDj();
    void slovoE();
    void slovoZz();
    void slovoZ();
    void slovoI();
    void slovoJ();
    void slovoK();
    void slovoL();
    void slovoLj();
    void slovoM();
    void slovoN();
    void slovoNj();
    void slovoO();
    void slovoP();
    void slovoR();
    void slovoS();
    void slovoT();
    void slovoCc();
    void slovoU();
    void slovoF();
    void slovoH();
    void slovoC();
    void slovoCcc();
    void slovoDz();
    void slovoS2();
    void proveri();

private:
    QString m_rec;
    int m_brojgreske;
    QTimer m_timer;
    int m_brojSlova;
    bool m_igraZavrsena;
};

#endif // VESALICE_H
