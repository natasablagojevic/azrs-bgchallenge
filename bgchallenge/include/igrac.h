#ifndef __IGRAC_H__
#define __IGRAC_H__

#include <QString>
#include <QList>
#include <QJsonObject>
class Igrac
{
public:
    Igrac(const QString &ime);

    Igrac();

    Igrac(const QString &ime, int vreme, int nivo, int izazov);

    QString getIme() const;
    int getVreme() const;
    int getNivo() const;
    int getPoslednjiOtkljucaniChallenge() const;
    void setPoslednjiOtkljucaniChallenge(int redniBr);
    void setNivo(int nivo);
    void setVreme(int vreme);
    void sacuvaj();


    QJsonObject toJson() const;
    static Igrac fromJson(const QJsonObject& json);

private:
    QString m_ime;
    int m_vreme;
    int m_nivo;
    int m_poslednjiChallenge;

};

#endif // __IGRAC_H__
