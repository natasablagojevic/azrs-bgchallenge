#ifndef CHALLENGETEST_H
#define CHALLENGETEST_H

#include<QString>
#include <QWidget>
#include <QStackedWidget>
#include <QPaintEvent>
#include "timer.h"

class ChallengeTest : public QWidget
{
    Q_OBJECT
protected:
    int m_id;
    int m_nivo;
    QString m_asocijacija;
    QStringList m_resenjeAsocijacije;
    QString m_hint;
    QString m_edukativniTekst;
    QString m_imagePath;
    QString m_naslovEdukativnogTeksta;
    Timer *m_vreme = nullptr;
    int m_pocetnoVreme;
    void paintEvent(QPaintEvent *event) override;


public:
    ChallengeTest(int id, int nivo, Timer* vreme, int pocetnoVreme, QWidget *parent = nullptr);

    virtual void zapocniChallenge();
    virtual void zavrsiChallenge();

    virtual ~ChallengeTest();

protected slots:
    void setImagePath(const QString &newImagePath);
    void zatvori();
    void prikaziEdukativniTekst();

signals:
    void resenIzazov(int id);
    void closed();

};

#endif // CHALLENGETEST_H
