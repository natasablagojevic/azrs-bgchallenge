#ifndef PERMUTACIJE_H
#define PERMUTACIJE_H

#include <QObject>
#include <QWidget>
#include "timer.h"
#include <QTextBrowser>
#include <QLabel>
#include <QPushButton>
#include "Challenge_test.h"

class Permutacije : public ChallengeTest
{
    Q_OBJECT

public:
    Permutacije(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
    ~Permutacije();

public slots:
   
    void tekstovi();
    void pomoc();

private slots:
    void kliknuoTrougao();
    void kliknuoZvezdu();
    void kliknuoKrug();
    void kliknuoReset();
    void poklopljenaSlova();




private:


    QString m_kombinacija;
    QString m_resenje;
    QString m_prethodniKlik;

    QLabel* m_lblPocetna;
    QLabel* m_lblStambol;
    QPushButton* m_btnTrougao;
    QPushButton* m_btnZvezda;
    QPushButton* m_btnKrug;
    QPushButton* m_btnReset;

    QTextBrowser* m_tbUputstvo;

    QWidget *m_ui;




};

#endif // PERMUTACIJE_H
