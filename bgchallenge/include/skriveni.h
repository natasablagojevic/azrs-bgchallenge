#ifndef SKRIVENI_H
#define SKRIVENI_H

#include <QObject>
#include <QWidget>
#include "timer.h"
#include <QTextBrowser>
#include <QLabel>
#include <QPushButton>
#include "Challenge_test.h"

class Skriveni : public ChallengeTest
{
    Q_OBJECT
public:
    Skriveni(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
    ~Skriveni();

public slots:

    void tekstovi();
    void pomoc();

    void btnVucicClicked();
    void btnMetaClicked();
    void btnMegafonClicked();
    void btnTransparentClicked();
    void btnSuzavacClicked();
    void btnFerariClicked();
    void btnNasaoVucicaClicked();
    void btnNasaoMetuClicked();
    void btnNasaoZastavuClicked();
    void btnNasaoTransparentClicked();
    void btnNasaoSuzavacClicked();
    void btnNasaoMegafonClicked();




private:

    bool m_nasaoVucica = false;
    bool m_nasaoZastavu = false;
    bool m_nasaoSuzavac = false;
    bool m_nasaoMegafon = false;
    bool m_nasaoMetu = false;
    bool m_nasaoTransparent = false;

    QTextBrowser* m_tbOpis;

    QPushButton* m_btnNasaoMegafon;
    QPushButton* m_btnNasaoMetu;
    QPushButton* m_btnNasaoSuzavac;
    QPushButton* m_btnNasaoTransparent;
    QPushButton* m_btnNasaoVucica;
    QPushButton* m_btnNasaoZastavu;

    QPushButton* m_btnMegafon;
    QPushButton* m_btnMeta;
    QPushButton* m_btnSuzavac;
    QPushButton* m_btnTransparent;
    QPushButton* m_btnVucic;
    QPushButton* m_btnFerari;

    int* m_brojac;

    QWidget *m_ui=nullptr;


};

#endif // SKRIVENI_H

