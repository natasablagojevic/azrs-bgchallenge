#ifndef PUZLA_H
#define PUZLA_H

#include <QWidget>
#include "time.h"
#include "Challenge_test.h"

namespace Ui {
class Puzla;
}

class Puzla : public ChallengeTest
{
    Q_OBJECT

public:
    explicit Puzla(Timer* vreme, int pocetnoVreme, QWidget *parent = nullptr);
    ~Puzla();
    void tekstovi();
    Ui::Puzla *m_ui;

public slots:
    void onPbSlika1Clicked();
    void onPbSlika2Cilcked();
    void onPbSlika3Clicked();
    void obPbSlika4Clicked();
    void onPbSlika5Clicked();
    void onPbSlika6Clicked();
    void onPbV1Clicked();
    void onPbMissingPieceClicked();


//private:
};

#endif // PUZLA_H
