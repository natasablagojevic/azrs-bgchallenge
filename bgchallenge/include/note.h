#ifndef NOTE_H
#define NOTE_H
#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QVector>
#include <QStringList>
#include <QPaintEvent>
#include "Challenge_test.h"
#include "timer.h"
#include <QMediaPlayer>
#include <QAudioOutput>

class Note : public ChallengeTest
{
    Q_OBJECT
public:
    Note(Timer* timer, int pocetnoVreme, QWidget *parent = nullptr);
    //virtual void zapocniChallenge() override;
    //virtual void zavrsiChallenge() override;
    ~Note();

    QString getIspravnaKombinacija() const { return m_ispravnaKombinacija; }
    QString getKombinacija() const { return m_kombinacija; }
    QList<QPushButton*> getNoteIspravnaKombinacija() const { return m_noteIspravnaKombinacija; }
    int* getBrojac() const { return m_brojac; }
    void tekstovi();


public slots:
    //virtual void zapocniChallenge() override;
    void pustiNotu(QString url);

    void kliknuoNotu(QPushButton* clickedButton,QString* kombinacija, QString url);
    void obrni(QPushButton* btnDo,QPushButton* btnRe,QPushButton* btnMi,QPushButton* btnFa,QPushButton* btnSol,QPushButton* btnLa,QPushButton* btnSi,QPushButton* btnDo2);
    void pomoc();





private slots:

    void reprodukcijaZvuka(QMediaPlayer *player);

signals:
         //void pogodjenaRec();

private:
    QWidget *m_ui;

    QMediaPlayer *m_nota;
    QAudioOutput *m_notaAO;

    QPushButton* m_btnDo;
    QPushButton* m_btnRe;
    QPushButton* m_btnMi;
    QPushButton* m_btnFa;
    QPushButton* m_btnSol;
    QPushButton* m_btnLa;
    QPushButton* m_btnSi;
    QPushButton* m_btnDo2;

    QString m_ispravnaKombinacija;
    QString m_kombinacija;
    QList<QPushButton*> m_noteKombinacija;
    QList<QPushButton*> m_noteIspravnaKombinacija;
    int* m_brojac;



};

#endif // NOTE_H

