#ifndef SVECI_H
#define SVECI_H
#include "Challenge_test.h"
#include "timer.h"

#include <QWidget>
#include <QPushButton>
#include<QLabel>
#include <QPaintEvent>

namespace Ui {
class Sveci;
}

class Sveci : public ChallengeTest
{
    Q_OBJECT

public:
    explicit Sveci(Timer* vreme,int pocetnoVreme, QWidget* parent = nullptr);
    ~Sveci();
    Ui::Sveci *m_ui;


public slots:
    void tekstovi();
    void proveri();
    void proveri2();
    void proveri3();
    void proveri4();
    void proveri5();
    void proveri6();
    void f();
    void r();
    void e();
    void s();
    void k();
    void a();


private:
    QPushButton *m_button1;
    QPushButton *m_button2;
    QPushButton *m_button3;
    QPushButton *m_button4;
    QPushButton *m_button5;
    QPushButton *m_button6;

    QTimer m_timer;

    QLabel *m_labela;



};

#endif // SVECI_H
