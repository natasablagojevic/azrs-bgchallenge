#ifndef MEMORIJA_H
#define MEMORIJA_H

#include <QObject>
#include <QWidget>
#include "timer.h"
#include <QTextBrowser>
#include <QString>
#include <QList>
#include <QTimer>
#include <QPushButton>
#include <QLineEdit>
#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>
#include <QParallelAnimationGroup>
#include "Challenge_test.h"

class Memorija : public ChallengeTest
{
    Q_OBJECT
public:
    Memorija(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
    ~Memorija();
    void tekstovi();
    void onesposobi();
    void osposobi();
    void onBtnClicked();
    void handleTimeoutNetacno();
    void handleTimeoutTacno();
     QList<QPushButton*> getButtonPar() const { return m_buttonPar; }
public slots:

    void pomoc();






private:

    QList<QPushButton*> m_buttonList;
    QList<QPushButton*> m_buttonPar;
    bool m_animationInProgress = false;
    QTimer* m_timerNetacno;
    QTimer* m_timerTacno;
    int* m_brojac;
    QString m_sifra;

    QPushButton* m_btnCard1;
    QPushButton* m_btnCard2;
    QPushButton* m_btnCard3;
    QPushButton* m_btnCard4;
    QPushButton* m_btnCard5;
    QPushButton* m_btnCard6;
    QPushButton* m_btnCard7;
    QPushButton* m_btnCard8;
    QPushButton* m_btnCard9;
    QPushButton* m_btnCard10;
    QPushButton* m_btnCard11;
    QPushButton* m_btnCard12;
    QPushButton* m_btnCard13;
    QPushButton* m_btnCard14;
    QPushButton* m_btnCard15;
    QPushButton* m_btnCard16;
    QPushButton* m_btnCard17;
    QPushButton* m_btnCard18;
    QPushButton* m_btnCard19;
    QPushButton* m_btnCard20;

    QLineEdit* m_leResenjeIzazova;
    QTextBrowser* m_tbUputstvo;

    QGraphicsOpacityEffect* m_mEffect0=nullptr;
    QGraphicsOpacityEffect* m_mEffect1=nullptr;
    QPropertyAnimation *m_animation1=nullptr;
    QPropertyAnimation* m_opacityAnimation1=nullptr;
    QPropertyAnimation *m_animation2=nullptr;
    QPropertyAnimation* m_opacityAnimation2=nullptr;
    QParallelAnimationGroup *m_group=nullptr;

    QWidget *m_ui=nullptr;




};

#endif // MEMORIJA_H
