#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include<QLineEdit>
#include <QMouseEvent>
#include <QAudioOutput>
#include <QMediaPlayer>
#include <QSoundEffect>
#include "mapaUI.h"
#include "rezultat.h"




QT_BEGIN_NAMESPACE
namespace Ui { class mainwindow; }
QT_END_NAMESPACE

class mainwindow : public QWidget
{
    Q_OBJECT

public:
    mainwindow(QWidget *parent = nullptr);
    Rezultat *m_rez;
    ~mainwindow();

public slots:
    void zapocniIgricuClicked();
    void pregledRezultataClicked();
    void podesavanjaClicked();
    void napustiIgricuClicked();

    void natragNaGlavniMeniClicked();

    void nastaviIgricuClicked();
    void kreni();

    void klikMelodija();
    void jacinaPozadinseMelodije (int vrednost);

    void cestitajIgracu();
    void prikaziUputstvo();


protected:
//    void resizeEvent(QResizeEvent *event) override;

    void kreniIgricu();
    bool eventFilter(QObject *obj, QEvent *event) override ;

signals:
    void clicked();


private:
    Ui::mainwindow *m_ui;
   // ChallengeMorzeovaAzbukaUI *u = nullptr;
    MapaUI *m_mapa = nullptr;
    QMediaPlayer *m_player;
    QAudioOutput *m_audioOutput;
    void setUpTabWidges();
    void setUpBackground();
    void showLoadGame();
    QString formatSeconds(int seconds);
    bool postojiIgrac(const QString& ime);
};
#endif // MAINWINDOW_H
