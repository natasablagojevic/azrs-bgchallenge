#ifndef PRONADJIULJEZA_H
#define PRONADJIULJEZA_H

#include <QDialog>
#include "Challenge_test.h"
#include "timer.h"

namespace Ui {
class PronadjiUljeza;
}

class PronadjiUljeza : public ChallengeTest
{
    Q_OBJECT

public:  
    explicit PronadjiUljeza(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
    ~PronadjiUljeza();
    bool m_znakpitanja;
    bool m_zlatnibokal;
    bool m_trisesira;
    bool m_malivrabac;
    bool m_sesirmoj;

private slots:
    friend class PronadjiUljezaTest;
    void znakpitanjaClicked();
    void zlatnibokalClicked();
    void trisesiraClicked();
    void malivrabacClicked();
    void sesirmojClicked();
    void limunzutClicked();

public:
    void tekstovi();
    void info();
    void buttonMask(QPushButton *button, const QPixmap &image);

private:
    friend class PronadjiUljezaTest;
    Ui::PronadjiUljeza *m_ui;

};

#endif // PRONADJIULJEZA_H
