#ifndef HANOJSKAKULA_H
#define HANOJSKAKULA_H

#include <QDialog>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "../forms/ui_hanojskakula.h"
#include <QStack>
#include "timer.h"
#include <QPainter>
#include "Challenge_test.h"



namespace Ui {
class Hanojskakula;
}
class Hanojskakula : public ChallengeTest
{
    Q_OBJECT

public:
    explicit Hanojskakula(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
    ~Hanojskakula();
    void zapocniChallenge() override;
    void zavrsiChallenge() override;
    void postaviPocetnoStanje();
    bool eventFilter(QObject *watched, QEvent *event) override;
    void obradiDropDiska(QGraphicsProxyWidget &proxyIzvorniStub,QGraphicsProxyWidget &proxyCiljniStub, QPointF inicijalnaPozicijaDiska);
    void postaviDiskNaStub(QGraphicsPixmapItem *disk, QStack<unsigned char> &stek, QLabel *ciljniStub,int yOffset);
    QStack<unsigned char> &vratiOdgovarajuciStek(QGraphicsProxyWidget *proxyCiljniStub);
    bool dozvoljenPotez(QGraphicsPixmapItem *draggedDisk, QStack<unsigned char> stub);
    void smanjiStek(QStack<unsigned char> &stub);
    void promeniDozvolu(QStack<unsigned char> stub, bool dozvola);
    void prikaziPorukuPobede();
    void tekstovi();
    bool m_pomocExecuted;

private slots:
    friend class HanojskakulaTest;
    void onRetryClicked();
    void pomoc();

private:
    friend class HanojskakulaTest;
    Ui::Hanojskakula *m_ui;
    QGraphicsView *m_viewHanojskeKule;
    QGraphicsScene *m_sceneHanojskeKule;
    QGraphicsPixmapItem *m_izabranDisk = nullptr;
    QGraphicsPixmapItem *m_disk1Item;
    QGraphicsPixmapItem *m_disk2Item;
    QGraphicsPixmapItem *m_disk3Item;
    QGraphicsProxyWidget *m_proxyStub1;
    QGraphicsProxyWidget *m_proxyStub2;
    QGraphicsProxyWidget *m_proxyStub3;
    QStack<unsigned char> m_stub1;
    QStack<unsigned char> m_stub2;
    QStack<unsigned char> m_stub3;
    QPointF m_inicijalnaPozicijaDiska = QPointF();
    QGraphicsProxyWidget *m_proxyInicijalniStub = nullptr;

};

#endif // HANOJSKAKULA_H
