#ifndef CHALLENGEMORZEOVAAZBUKAUI_H
#define CHALLENGEMORZEOVAAZBUKAUI_H
#include <QWidget>
#include <QPaintEvent>
#include <QLCDNumber>
#include <QString>
#include "timer.h"
#include "Challenge_test.h"
#include <QPushButton>

class ChallengeMorzeovaAzbukaUI : public ChallengeTest
{
    Q_OBJECT

public:

    ChallengeMorzeovaAzbukaUI(Timer* vreme,int pocetnoVreme, QWidget* parent = nullptr);
    ~ChallengeMorzeovaAzbukaUI();
    void tekstovi();

    QWidget *m_ui =nullptr;

    QString getRec();
    QString getKod();

    QTimer& getTimer();

    int getCurrentIndex();
    QString getImagePath();


public slots:


    void pogodi(const QString &r);
    void spelujRec();
    void timerExpired();


private:

    const QString m_rec = "staklo";
    const QString m_kod = ". . .#-#. -#- . -#. - . .#- - -";
    bool m_upaljenoSvetlo;
    QTimer m_timer;
    int m_currentIndex = 0;


};


#endif // CHALLENGEMORZEOVAAZBUKAUI_H
