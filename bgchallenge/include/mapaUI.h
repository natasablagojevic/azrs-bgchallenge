#ifndef MAPAUI_H
#define MAPAUI_H



#include <QWidget>
#include <QString>
#include <QPaintEvent>
#include <QLCDNumber>
#include <QPushButton>
#include <QVector>
#include "mapa.h"
#include "igrac.h"
#include "rezultat.h"




class MapaUI : public QWidget{
    Q_OBJECT

public:
    MapaUI(Igrac igrac,Rezultat *rez, QWidget* parent = nullptr);
    ~MapaUI();


public slots:
    void updateBackground(const QString& newPath);
    void nazadNaGlavniMeni();
    void azurirajVreme(qint64 vreme);

    void otkljucajNaredniIzazov(qint64 id);
    void ucitajMapu(int poslednjiIzazov, int nivo);

    void prikaziPrviChallenge();
    void prikaziDrugiChallenge();
    void prikaziTreciChallenge();
    void prikaziCetvrtiChallenge();

    void prikaziPetiChallenge();
    void prikaziSestiChallenge();
    void prikaziSedmiChallenge();
    void prikaziOsmiChallenge();

    void prikaziDevetiChallenge();
    void prikaziDesetiChallenge();
    void prikaziJedanaestiChallenge();
    void prikaziDvanaestiChallenge();

    void prikaziTrinaestiChallenge();
    void prikaziCetrnaestiChallenge();
    void prikaziPetnaestiChallenge();
    void prikaziSesnaestiChallenge();

    void zavrsenaIgrica();
signals:
    void cestitajIgracu();
private:

    Mapa *m_mapa;
    QString m_bgImagePath;
    QLCDNumber *m_vreme = nullptr;
    QVector <QPushButton*> m_izazovi;



protected:
    void paintEvent(QPaintEvent* event) override;

};



#endif // MAPAUI_H
