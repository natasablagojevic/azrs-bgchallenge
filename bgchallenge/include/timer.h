#ifndef TIMER_H
#define TIMER_H

#include <QTimer>
#include <QDateTime>

class Timer : public QTimer
{
    Q_OBJECT
public:
    explicit Timer(QObject* parent = nullptr);
    ~Timer();
    void start();
    void stop();
    int getGameTime() const;
private slots:
    void azurirajGameTime();
signals:
    void azurirajVreme(int vreme);
private:
    QTimer *m_timer;
    QDateTime m_startTime;
    QDateTime m_endTime;
    int m_gameTime;

};

#endif // TIMER_H
