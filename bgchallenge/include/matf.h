#ifndef MATF_H
#define MATF_H

#include <QDialog>
#include<QPushButton>
#include<QMessageBox>
#include<QVector>
#include<QTimer>
#include<QPixmap>
#include<QTableWidgetItem>
#include<QCoreApplication>
#include <QWidget>
#include "../include/Challenge_test.h"
#include "../include/timer.h"

namespace Ui {
class MATF;
}

class MATF : public ChallengeTest
{
    Q_OBJECT

public:
    explicit MATF(Timer* timer, int pocetnoVreme, QWidget *parent = nullptr);
    ~MATF();
    void tekstovi();
    Ui::MATF *m_ui;
//    QVector<QVector<int>> *matrica;

    bool getCheckSum(const int &a, const int &b, const int &c);
//    bool getCheckMatrix(const QVector<QVector<int>> &m);

public slots:
    void onPbHintClicked();
    void onPbProveriClicked();

private:

    bool checkSum(const int &a, const int &b, const int &c);
    bool checkMatrix(const QVector<QVector<int>> &m);
};

#endif // MATF_H
