#ifndef LOSTINMIGRATION_H
#define LOSTINMIGRATION_H

#include "Challenge_test.h"
#include <QObject>
#include <QWidget>

#include <QObject>
#include <QWidget>
#include "timer.h"
#include <QTextBrowser>
#include <QLabel>
#include <QSequentialAnimationGroup>
#include <QPushButton>
#include <QLineEdit>

class LostInMigration : public ChallengeTest
{
    Q_OBJECT
public:
    LostInMigration(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
    ~LostInMigration();
    void tekstovi();
    QPushButton* getBtnLevo() const { return m_btnLevo; }
    QPushButton* getBtnDesno() const { return m_btnDesno; }
    QPushButton* getBtnGore() const { return m_btnGore; }
    QPushButton* getBtnDole() const { return m_btnDole; }
    QWidget* getUi() const { return m_ui; }

    QLineEdit* getLeResenjeIzazova() const { return m_leResenjeIzazova; }
    QLabel* getLblUnesiResenje() const { return m_lblUnesiResenje; }

    void startFadeDownAnimations();
public slots:
   


private slots:
    void kliknuoDugmeLevo(int* brojac, QString* kombinacija, QString ispravnaKombinacija);
    void kliknuoDugmeGore(int* brojac, QString* kombinacija, QString ispravnaKombinacija);
    void kliknuoDugmeDesno(int* brojac, QString* kombinacija, QString ispravnaKombinacija);
    void kliknuoDugmeDole(int* brojac, QString* kombinacija, QString ispravnaKombinacija);


    void hideFrame(QLabel* label, QString slovo);
    void fadeDownLabel(QLabel* label, QSequentialAnimationGroup* group, int startDelay, QString slovo);


    void pomoc();


signals:
 


private:




    QString m_kombinacija;
    int m_brojac;
    QString m_ispravnaKombinacija;
    QString m_resenjeIzazova;

    QWidget *m_ui=nullptr;

    QPushButton* m_btnLevo;
    QPushButton* m_btnDesno;
    QPushButton* m_btnGore;
    QPushButton* m_btnDole;

    QLineEdit* m_leResenjeIzazova;
    QLabel* m_lblUnesiResenje;

    QLabel* m_lblDesno1;
    QLabel* m_lblDesno2;
    QLabel* m_lblDole1;
    QLabel* m_lblDesno3;
    QLabel* m_lblGore1;
    QLabel* m_lblLevo1;
    QLabel* m_lblGore2;
    QLabel* m_lblLevo2;


};
#endif // LOSTINMIGRATION_H
