#!/usr/bin/bash

# Putanje do foldera
SOURCE_DIR="./source"
INCLUDE_DIR="./include"

# Ključevi za clang-format
CLANG_FORMAT_KEYS="-style=file -i"

# Formatiranje .cpp i .h fajlova u source folderu
find "$SOURCE_DIR" -type f \( -name "*.cpp" -o -name "*.h" \) -exec clang-format $CLANG_FORMAT_KEYS {} +

# Formatiranje .cpp i .h fajlova u include folderu
# find "$INCLUDE_DIR" -type f \( -name "*.cpp" -o -name "*.h" \) -exec clang-format $CLANG_FORMAT_KEYS {} +
